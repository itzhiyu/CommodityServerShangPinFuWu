SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_level` int(11) DEFAULT NULL COMMENT '级别',
  `category_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '目录名称',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  `is_leaf` bit(1) DEFAULT NULL COMMENT '是否为叶子节点',
  `merchant_id` bigint(20) DEFAULT NULL COMMENT '商家ID',
  `order_num` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '排序编号',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父节点',
  PRIMARY KEY (`id`),
  KEY `FK2y94svpmqttx80mshyny85wqr` (`parent_id`),
  CONSTRAINT `FK2y94svpmqttx80mshyny85wqr` FOREIGN KEY (`parent_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='商品类别（公共库）表';

DROP TABLE IF EXISTS `commodity`;
CREATE TABLE IF NOT EXISTS `commodity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `barcode` varchar(100) DEFAULT NULL COMMENT '商品条形码',
  `category_id` varchar(20) DEFAULT NULL COMMENT '商品分类标识',
  `create_date` datetime DEFAULT NULL  COMMENT '创建时间',
  `description` varchar(255) DEFAULT NULL  COMMENT '描述',
  `merchant_id` bigint(20) DEFAULT NULL  COMMENT '商家ID',
  `name` varchar(50) DEFAULT NULL COMMENT '商品名称',
  `pinyin` varchar(255) DEFAULT NULL COMMENT '商品拼音',
  `pinyin_shorthand` varchar(50) DEFAULT NULL COMMENT '商品拼音首字母缩写',
  `specification` varchar(100) DEFAULT NULL COMMENT '商品规格',
  `type` varchar(1) DEFAULT NULL COMMENT '商品类型 1:编码商品 2:称重商品, 3:快录商品',
  `unit` varchar(100) DEFAULT NULL COMMENT '商品单位',
  `update_date` datetime DEFAULT NULL COMMENT '商品修改时间',
  `warning_number` double DEFAULT NULL COMMENT '商品预警数',
  `split_rule_id` bigint(20) DEFAULT NULL COMMENT '商品拆分规则外键',
  `buying_price` decimal(19,2) DEFAULT NULL COMMENT '商品进货价',
  `country` varchar(5) DEFAULT NULL COMMENT '商品所属国家缩写',
  `is_deleted` tinyint(1) DEFAULT '0'COMMENT '商品是否删除',
  `is_detachable` tinyint(1) DEFAULT NULL COMMENT '商品是否拆分',
  `selling_price` decimal(19,2) DEFAULT NULL COMMENT '商品销售价',
  `status` varchar(2) DEFAULT NULL COMMENT '商品状态 1 下架 2 上架 ',
  `inventory_number` double DEFAULT NULL COMMENT '商品商品库存数',
  PRIMARY KEY (`id`),
  KEY `FK24j4pxneuo8actrdeylcgtekt` (`split_rule_id`),
  CONSTRAINT `FK24j4pxneuo8actrdeylcgtekt` FOREIGN KEY (`split_rule_id`) REFERENCES `split_commodity_rule` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT '商品表（商家库）';

DROP TABLE IF EXISTS `plucode`;
CREATE TABLE IF NOT EXISTS `plucode` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `array_string` text COLLATE utf8mb4_unicode_ci COMMENT '二维数组序列化字符串',
  `shop_id` bigint(20) DEFAULT NULL COMMENT '店铺ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT '商品PLU二维数组表（商家库）';

DROP TABLE IF EXISTS `public_category`;
CREATE TABLE IF NOT EXISTS `public_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_level` int(1) DEFAULT NULL COMMENT '级别',
  `category_name` varchar(50) DEFAULT NULL COMMENT '目录名称',
  `is_leaf` bit(1) DEFAULT NULL  COMMENT '是否为叶子节点',
  `order_num` varchar(50) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL DEFAULT NULL COMMENT '排序编号',
  PRIMARY KEY (`id`),
  KEY `FK5t6o2xwvpukyvl6escxhihi2d` (`parent_id`),
  CONSTRAINT `FK5t6o2xwvpukyvl6escxhihi2d` FOREIGN KEY (`parent_id`) REFERENCES `public_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT '商品分类表（公共库）';

DROP TABLE IF EXISTS `public_commodity`;
CREATE TABLE IF NOT EXISTS `public_commodity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `barcode` varchar(50) DEFAULT NULL COMMENT '商品条形码',
  `category_id` varchar(50) DEFAULT NULL COMMENT '商品分类标识',
  `create_time` datetime DEFAULT NULL  COMMENT '创建时间',
  `description` varchar(255) DEFAULT NULL  COMMENT '描述',
  `is_detachable` tinyint(1) DEFAULT '0' COMMENT '商品是否拆分',
  `name` varchar(50) DEFAULT NULL  COMMENT '商品名称',
  `specification` varchar(100) DEFAULT NULL COMMENT '商品规格',
  `unit` varchar(50) DEFAULT NULL COMMENT '商品单位',
  `split_rule_id` bigint(20) DEFAULT NULL COMMENT '商品拆分规则外键',
  `brand_name` varchar(150) DEFAULT NULL COMMENT '品牌名称',
  `selling_price` decimal(19,2) DEFAULT NULL COMMENT '商品销售价',
  `company_id` bigint(20) DEFAULT NULL COMMENT '企业Id',
  `country` varchar(50) DEFAULT NULL COMMENT '商品所属国家',
  `is_gs1` bit(1) DEFAULT NULL COMMENT '商品是否为GS1编码',
  `pinyin` varchar(255) DEFAULT NULL  COMMENT '商品拼音',
  `pinyin_shorthand` varchar(50) DEFAULT NULL  COMMENT '商品拼音首字母缩写',
  `create_date` datetime DEFAULT NULL  COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `FKi1fwkp74s3r5ymd9y54u4e6c0` (`split_rule_id`),
  CONSTRAINT `FKi1fwkp74s3r5ymd9y54u4e6c0` FOREIGN KEY (`split_rule_id`) REFERENCES `split_commodity_rule` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT '商品表（公共库）';

DROP TABLE IF EXISTS `public_resource`;
CREATE TABLE IF NOT EXISTS `public_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `heigth` float NOT NULL COMMENT '图片资源高度',
  `is_banner` int(11) NOT NULL DEFAULT '0' COMMENT '是否是图片',
  `res_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片资源路径',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '资源类型',
  `width` float NOT NULL COMMENT '图片资源宽度',
  `public_commodity_id` bigint(20) DEFAULT NULL COMMENT '商品id（公共库）',
  PRIMARY KEY (`id`),
  KEY `FK3fiv4hkuxia5v47q1f19qgcxj` (`public_commodity_id`),
  CONSTRAINT `FK3fiv4hkuxia5v47q1f19qgcxj` FOREIGN KEY (`public_commodity_id`) REFERENCES `public_commodity` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT '商品公共库资源表';

DROP TABLE IF EXISTS `resource`;
CREATE TABLE IF NOT EXISTS `resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `heigth` float NOT NULL COMMENT '图片资源高度',
  `res_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片资源路径',
  `width` float NOT NULL COMMENT '图片资源宽度',
  `is_banner` int(11) NOT NULL DEFAULT '0' COMMENT '是否是图片',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '资源类型',
  `commodity_id` bigint(20) DEFAULT NULL COMMENT '商品id（商家库）',
  PRIMARY KEY (`id`),
  KEY `FKcorauqeuy9obc8c9gkysswmhj` (`commodity_id`),
  CONSTRAINT `FKcorauqeuy9obc8c9gkysswmhj` FOREIGN KEY (`commodity_id`) REFERENCES `commodity` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT '商品（商家库）资源表';

DROP TABLE IF EXISTS `public_split_commodity_rule`;
CREATE TABLE IF NOT EXISTS `public_split_commodity_rule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `commodity_id` bigint(20) DEFAULT NULL COMMENT '拆分后商品id（公共库）',
  `split_number` double DEFAULT NULL COMMENT '商品拆分数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT '商品公共库拆分规则表';

DROP TABLE IF EXISTS `split_commodity_rule`;
CREATE TABLE IF NOT EXISTS `split_commodity_rule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `commodity_id` bigint(20) DEFAULT NULL COMMENT '拆分后商品id（商家库）',
  `split_number` double DEFAULT NULL COMMENT '商品拆分数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT = 0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT '商品（商家库）拆分规则表';