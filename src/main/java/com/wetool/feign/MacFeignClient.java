package com.wetool.feign;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("mac-server")
public interface MacFeignClient {

	 @RequestMapping(value = "/mac/getByMerchantId/{merchantId}", method = RequestMethod.GET)
	    public ResponseEntity<List<String>> getByMerchantId(@PathVariable("merchantId") Long merchantId);
}
