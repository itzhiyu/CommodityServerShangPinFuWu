package com.wetool.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wetool.entity.Commodity;
import com.wetool.entity.PublicCommodity;

/**
 * 信息同步
 */
@FeignClient("task-server")
public interface TaskFeignClient {

	/**
	 * 商品信息校验
	 * @param PublicCommodity 公共库商品
	 * @return
	 */
	@RequestMapping(value = "/sync", method = RequestMethod.POST)
	public PublicCommodity sync(@RequestBody Commodity commodity);

}
