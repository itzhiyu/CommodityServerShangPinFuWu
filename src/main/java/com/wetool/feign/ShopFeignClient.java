package com.wetool.feign;

import java.security.Principal;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("shop-server")
public interface ShopFeignClient {

	@RequestMapping(value = "/getMerchantId", method = RequestMethod.GET)
	public ResponseEntity<?> getMerchantIdByPrincipal(Principal principal);
}
