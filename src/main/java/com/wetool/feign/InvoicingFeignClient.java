package com.wetool.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.wetool.entity.Commodity;
import com.wetool.model.CommoditySave;


@FeignClient("invoicing-server")
public interface InvoicingFeignClient {

	/**
	 * 商家库添加商品同步到店铺商品中
	 * @param commodity
	 * @return
	 */
	@RequestMapping(value = "/commodity/create/{merchantId}", method = RequestMethod.POST)
	public Commodity createShopCommodity(@PathVariable("merchantId") Long merchantId,@RequestBody Commodity commodity);
	
	/**
	 * 商家库修改商品同步到店铺商品中
	 * @param commodity
	 * @return
	 */
	@RequestMapping(value = "/commodity/update", method = RequestMethod.PATCH)
	public String updateShopCommodity(@RequestBody CommoditySave commodity);
}
