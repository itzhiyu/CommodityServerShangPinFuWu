package com.wetool.feign;

import com.wetool.model.Base64Image;
import com.wetool.model.Result;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient("basic-server")
public interface UploadFeignClient {

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, value = "/findBasePath")
    ResponseEntity<String> getBasePath();

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value = "/delOssObject")
    ResponseEntity<?> delOssObject(@RequestBody String keys);

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value = "/api/v1/uploadImageBySimpleMethod")
    ResponseEntity<Result> uploadImageBySimpleMethod(@RequestBody Base64Image base64Image);


	/* 示例：支持实体对象参数 */
    // @RequestLine("POST /metrics-service/rest/addMetric")
    // MyEntity sendEntity(@RequestBody MyEntity toSend);
}