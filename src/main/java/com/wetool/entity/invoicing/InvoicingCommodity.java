package com.wetool.entity.invoicing;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import com.wetool.entity.Commodity;
import lombok.Data;

/**
 * 描述：商品POJO
 * @author lixin
 */
@Data
@Entity
@DynamicInsert(value=true)
@DynamicUpdate(value=true)
@Table(name = "invoicing_commodity")
public class InvoicingCommodity implements Serializable {

	private static final long serialVersionUID = 8750477900539271695L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column()
	private Long shopId;
	
	@ManyToOne(cascade = { CascadeType.MERGE ,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name = "commodity_id")
	private Commodity commodity;
	
	@Column()
	protected BigDecimal sellingPrice; // 销售价格

	@Column()
	protected BigDecimal buyingPrice; // 进货价格
	
	@Column()
	private Double netSales = 0D; // 网络预售数
	
	@Column()
	private Boolean isDeleted = false;
	
	@Column()
	private Date createDate;//创建时间
	
	@Column()
	private Date updateDate;//修改时间

	@Column()
	private Long supplierId; // 默认供应商

	@Column(columnDefinition = "decimal(11,3)")
	private Double inventoryWarningQuantity = 0D; // 商品预警数

	@Column(columnDefinition = "decimal(11,3)")
	private Double inventoryTotalQuantity = 0D; // 库存总量

	@Column(columnDefinition = "decimal(11,3)")
	private Double inventoryNumber = 0D; //店铺商品库存数
	
	@Column()
	private Double inventoryCost = 0D; //库存成本
	
	@Transient
	private String supplierName;//供应商名称
}
