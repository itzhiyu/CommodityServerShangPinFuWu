package com.wetool.entity.invoicing;

import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.wetool.model.enums.OutOfStorage;

import lombok.Data;

/**
 * 视图实体
 * @author lixin
 *
 */
@Data
public class InvoicingRecord {
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date createDate;
	
	private String date;
	
	private Long type;
	
	private Double number;
	
	private OutOfStorage outOfStorage;
	
	private Double price;
	
	private String typeName;
	
	public InvoicingRecord(){}

	
	public InvoicingRecord(Date createDate, Long type, Double number,OutOfStorage outOfStorage,BigDecimal price) {
		super();
		this.createDate = createDate;
		this.type = type;
		this.number = (number == null)? 0D: number;
		this.outOfStorage = outOfStorage;
		this.price = (price == null)?new BigDecimal(0).doubleValue():price.doubleValue();
	}
}
