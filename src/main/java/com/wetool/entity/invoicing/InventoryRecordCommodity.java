package com.wetool.entity.invoicing;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.wetool.utils.Arith;

import lombok.Data;

/**
 * 描述：入库多商品POJO
 * @author lixin
 */
@Data
@Entity
@Table()
public class InventoryRecordCommodity implements Serializable {
	private static final long serialVersionUID = -1132319928812751217L;

	/** 主键 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/** 商品单价 */
	@Column(columnDefinition = "decimal(9,2)")
	private BigDecimal price;

	/** 商品数量 */
	@Column(columnDefinition = "decimal(9,2)")
	private Double commodityNumber;

	/** 出入库记录主表ID */
	@Column(name = "inventory_record_id")
	private Long inventoryRecordId;//对应入库单外键
	
    /** 出入库总金额 */
	@Column(columnDefinition = "decimal(9,2)")
    private BigDecimal totalPrice;

    /** 商品条码 */
	@Column()
    private String barcode;
    
    /** 商品名称 */
	@Column()
    private String name;
 
    /** 商品规格*/
    @Column()
	private String specification; 

    /**商品单位*/
    @Column()
	private String unit;
    
    /** 出入库商品ID */
    @Column()
    private Long commodityId;

	/** 结存 */
	@Column(columnDefinition = "decimal(9,2)")
	private Double balance;
	
	@Column()
	private String remark;//备注信息

	public InventoryRecordCommodity() {}
	
	public InventoryRecordCommodity( BigDecimal price, Double commodityNumber, String barcode, 
			String name, String specification, String unit, Long commodityId,
			Double balance) {
		super();
		this.price = price;
		this.commodityNumber = commodityNumber;
		Double pri =  Arith.mul(price.doubleValue(), commodityNumber);
		this.totalPrice = new BigDecimal(pri).setScale(2, BigDecimal.ROUND_HALF_UP);
		this.barcode = barcode;
		this.name = name;
		this.specification = specification;
		this.unit = unit;
		this.commodityId = commodityId;
		this.balance = balance;
	}
	
	public InventoryRecordCommodity( BigDecimal price, Double commodityNumber, String barcode, 
			String name, String specification, String unit, Long commodityId,
			Double balance, BigDecimal totalPrice) {
		super();
		this.price = price;
		this.commodityNumber = commodityNumber;
		Double pri =  Arith.mul(price.doubleValue(), commodityNumber);
		this.totalPrice = new BigDecimal(pri).setScale(2, BigDecimal.ROUND_HALF_UP);
		this.barcode = barcode;
		this.name = name;
		this.specification = specification;
		this.unit = unit;
		this.commodityId = commodityId;
		this.balance = balance;
		this.totalPrice = totalPrice;
	}
}
