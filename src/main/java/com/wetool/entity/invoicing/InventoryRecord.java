package com.wetool.entity.invoicing;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import com.wetool.model.enums.OutOfStorage;
import com.wetool.model.enums.Status;
import lombok.Data;

/**
 * 描述：入库信息POJO
 * @author lixin
 */
@Entity
@Data
@Table()
public class InventoryRecord implements Serializable {
	private static final long serialVersionUID = 5143691833018554795L;

	/** 主键 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/** 订单号 */
	@Column(nullable = false)
	private String orderNumber;

	/**初始单号*/
	@Column()
	private String initialOrderNumber;
	
	/** 入库类型 4: 采购入库  */
	@Column()
	private Long type;

	/** 多商品 */
	@OneToMany(targetEntity = InventoryRecordCommodity.class,cascade = { CascadeType.MERGE,CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name = "inventory_record_id")
	private List<InventoryRecordCommodity> commoditys;

	/** 供应商 */
	@Column()
	private Long supplierId;

	/** 店铺ID */
	@Column()
	private Long shopId;

	/** 进货日期 */
	@Column()
	private Date createDate = new Date(); // 创建时间
	
	/** 修改日期 */
	@Column()
	private Date updateDate = new Date();

	/**入库状态status : 0：生成订单 1:提交订单*/
	@Column()
	@Enumerated(EnumType.ORDINAL)
	private Status status;

	/** 调拨出库库商店 */
	@Column()
	private Long outShop;

	/** 出入库 1 为出库 2为入库 */
	@Column() 
	@Enumerated(EnumType.ORDINAL)
	private OutOfStorage outOfStorage;
	
	/** 订单总价 */
	@Column()
	private BigDecimal totalPrice;
	
	@Column()
	private String remark;//备注信息

	@Column
	private Boolean isDeleted = false; // 删除标识
	
	@Transient
	private String supplierName;

	
	public InventoryRecord() {}
	
	public InventoryRecord( String orderNumber, String initialOrderNumber, Long type,
			List<InventoryRecordCommodity> commoditys, Long supplierId, Long shopId, 
			Status status, Long outShop, OutOfStorage outOfStorage, BigDecimal totalPrice) {
		super();
		this.orderNumber = orderNumber;
		this.initialOrderNumber = initialOrderNumber;
		this.type = type;
		this.commoditys = commoditys;
		this.supplierId = supplierId;
		this.shopId = shopId;
		this.status = status;
		this.outShop = outShop;
		this.outOfStorage = outOfStorage;
		this.totalPrice = totalPrice;
	}
	
	
	
}
