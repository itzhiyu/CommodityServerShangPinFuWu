package com.wetool.entity.invoicing;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

/**
 * 描述：供应商POJO
 * @author lixin
 */
@Entity
@Data
@Table()
public class Supplier implements Serializable {
	private static final long serialVersionUID = -6109271897503138969L;

	/** id（供应商主键） */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/** 店铺id */
	@Column()
	private Long shopId;

	/** 供应商名称 */
	@Column()
	private String supplierName;

	/** 供应商联系人 */
	@Column( )
	private String supplierContacts;

	/** 供应商电话 */
	@Column()
	private String supplierTel;

	/** 供应商地址 */
	@Column()
	private String supplierAddress;

	@Column
	private Boolean isDeleted = false; // 删除标识
	
	@Column()
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="CET") 
	private Date createDate = new Date();//创建时间 

	/** 供应商备注 */
	@Column(name = "sup_remark", length = 100)
	private String remark;
	
	@Transient
	private Long count;//供应商对应商品数量
}