package com.wetool.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;

/**
 * plu码实体
 * @author li
 *
 */
@Data
@Entity
public class PLUCode {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column()
	private Long shopId; // 店铺ID
	
	@Column(columnDefinition="TEXT", length = 65535)
	private String arrayString;//二维数组序列化字符串
}
