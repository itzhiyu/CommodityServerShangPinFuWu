package com.wetool.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;

/**
 * 商品类别
 * 
 * @author zhangjie
 */
@Entity
@Data
public class Category implements java.io.Serializable {
    private static final long serialVersionUID = -1679572259923023796L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id; // 主键

    @Column()
    private Long merchantId; //商家Id
    
    @Column(name = "category_name")
    private String categoryName; // 目录名称

    @Column(name = "category_level")
    private Integer categoryLevel; // 级别

    @Column(name = "order_num")
    private String orderNum; // 排序编号

    @Column(name = "is_leaf")
    private boolean isLeaf; // 是否为叶子节点
    
    @Column
	private boolean isDeleted = false; // 逻辑删除标记 0-未删除 1-已删除
    
    /** 父节点 */
    @ManyToOne(cascade = { CascadeType.MERGE })
    @JoinColumn(name = "parent_id")
    @JsonBackReference
    private Category parent;
    
    /** 子节点 */
    @OneToMany(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY, mappedBy = "parent")
    private List<Category> children = new ArrayList<Category>();

}