package com.wetool.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wetool.serialize.FullPathDeserializer;
import com.wetool.serialize.FullPathSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 图片资源文件
 * 
 * @author zhangjie
 */
@Entity
@Data
@ApiModel(value = "新增图片资源对象", description = "")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","operations","roles","menus"})
public class PublicResource implements Serializable {

	private static final long serialVersionUID = -5949216842396501491L;

	/** id（流水号主键） */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(value = "资源id(新增商品图片为NULL值，修改商品图片填入ID)", required = false, example =  "54" ,position = 0)
	private Long id;

	/**
	 * 是否为banner图片 0-否 1-是 为0时，即点击banner图进入的专题页面图片
	 */
	@Column(nullable = false, length = 1)
	@ApiModelProperty(hidden=true)
	private Integer isBanner;
	
	/**
	 * 资源类型 0-图片 1-视频
	 */
	@ApiModelProperty(value = "资源类型 0-图片 1-视频", required = false, example =  "0" ,position = 0)
	@Column(nullable = false, length = 1)
	private Integer type;

	/**
	 * 资源路径
	 */
	@ApiModelProperty(value = "资源路径", required = false, example =  "http://wetool.oss-cn-beijing.aliyuncs.com/ms.test.pro/ba85cb80-09e8-4a35-95f0-f1de79069877_w640_h394.jpeg" ,position = 0)
	@Column(name = "res_url", nullable = false, length = 255)
	private String resUrl;

	@JsonDeserialize(using = FullPathDeserializer.class)
	public void setResUrl(String resUrl) {
		this.resUrl = resUrl;
	}

	
	@JsonSerialize(using = FullPathSerializer.class)
	public String getResUrl() {
		return this.resUrl;
	}

	/**
	 * 图片宽度
	 */
	@Column(nullable = false)
	@ApiModelProperty(hidden=true)
	private Float width;

	/**
	 * 图片高度
	 */
	@Column(nullable = false)
	@ApiModelProperty(hidden=true)
	private Float heigth;

}