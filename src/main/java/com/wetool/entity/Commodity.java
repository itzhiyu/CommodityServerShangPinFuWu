package com.wetool.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import com.wetool.entity.base.CommodityBase;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 商品实体对象（商家库）
 * 
 * @author zhangjie
 */
@Data
@Entity
@EqualsAndHashCode(callSuper=true)
@Table(name = "commodity")
public class Commodity extends CommodityBase implements Serializable {
	private static final long serialVersionUID = -198546246502271343L;
	
	@Column()
	private Long merchantId; // 商家ID
	
	@OneToMany(cascade = { CascadeType.MERGE ,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name = "commodity_id")
	private List<Resource> resources;// 资源列表

	@Column()
	private String description; // 描述

	@Column()
	private String country; // 商品所属国家简写

	@OneToOne(cascade = { CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.MERGE})
	@JoinColumn(name = "split_rule_id")
	private SplitCommodityRule splitRule; // 拆分规则

	@Column()
	private Double warningNumber; // 商品预警数
	
	@Column(columnDefinition = "decimal(11,3)")
	private Double inventoryNumber = 0D; //商家商品库存数
	
	@Transient
	private Resource resource;// 展示资源列表
}