package com.wetool.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 * 商品拆分规则
 */

@Entity
@Data
@Table
public class SplitCommodityRule {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	//@ApiModelProperty(hidden=true)
	private Long id;

	@Column(name = "commodity_id")
	//@ApiModelProperty(value = "拆分后商品编号", required = false, example =  "1084" ,position = 0)
	private Long commodityId;// 拆分后商品编号

	@Column()
	//@ApiModelProperty(value = "拆分数量", required = false, example =  "0.5" ,position = 0)
	private Double splitNumber;// 拆分数量
}