package com.wetool.entity.base;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

/**
 * 商品库存公共属性
 * @author lixin
 *
 */
@Data
@MappedSuperclass
public class CommodityBase {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected Long id;

	@Column()
	protected String name; // 商品名称

	@Column()
	protected Long categoryId; // 商品分类标识

	@Column()
	protected String barcode; // 商品条形码
	
	@Column()
	private String selfCoding;//自编码

	@Column()
	protected String pinyin; // 商品名称拼音

	@Column()
	protected String pinyinShorthand; // 商品名称拼音首字母简写

	@Column()
	protected Long type; // 商品类型 0:正规商品 1:自编码; 3:称重商品
	
	@Column()
	protected String specification; // 商品规格

	@Column()
	protected String unit; // 商品单位

	@Column()
	protected Integer status = 1; //商品状态 1：下架 2：上架 (入库单添加商品时默认下架)

	@Column()
	protected Boolean isDetachable; // 是否可拆分

	@Column
	protected BigDecimal sellingPrice; // 销售价格

	@Column
	protected BigDecimal buyingPrice; // 进货价格

	@Column()
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd hh:mm:ss") 
	protected Date createDate = new Timestamp(System.currentTimeMillis()); // 创建时间

	@Column()
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd hh:mm:ss") 
	protected Date updateDate = new Timestamp(System.currentTimeMillis()); // 修改时间
	
	@Column
	protected Boolean isDeleted = false; // 逻辑删除标记 0-未删除 1-已删除
}
