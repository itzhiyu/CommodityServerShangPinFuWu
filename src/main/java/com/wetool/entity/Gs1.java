package com.wetool.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

/**
 * gs1信息模型
 * @author lixin
 *
 */
@Entity
@Data
public class Gs1 {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column()
    private String barCode; // 查询条码

    @Column()
    private String commodityName;// 商品名称

    @Column()
    private String brandName; // 品牌名称

    @Column()
    private String enterpriseName; // 企业名称

    @Column()
    private String enterpriseAddress; // 企业地址

    @Column()
    private String commoditySpecification; // 商品规格

    @Column()
    private String linkUrl; // 查询地址

    @Column()
    private String imgUrl; // 商品图片地址

    @Column()
    private Integer type; // 商品进出口类型

}
