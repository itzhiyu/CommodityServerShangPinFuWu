package com.wetool.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

/**
 * 商品实体对象（公共库）
 * 
 * @author zhangjie
 */
@Entity
@Setter
@Getter
public class PublicCommodity implements java.io.Serializable {
    private static final long serialVersionUID = 4744891820482977460L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column()
    private String name; // 商品名称

    @Column()
    private Long categoryId; // 商品分类标识

    @Column()
    private String barcode; // 商品条形码
    
    @Column()
	private String pinyin; // 商品名称拼音

	@Column()
	private String pinyinShorthand; // 商品名称拼音首字母简写

    @Column()
    private String specification; // 商品规格

    @Column()
    private String unit; // 商品单位

    @Column()
    private BigDecimal sellingPrice;// 商品价格

    @Column()
    private Long companyId;// 企业Id

    @Column()
    private String brandName; // 品牌名称

    @Column()
    private Boolean isDetachable; // 是否可拆分

    @Column()
    private String description; // 描述

    @Column()
    private Boolean isGs1; // 是否为正规条码商品

    @Column()
    private String country; // 商品所属国家

    @Column()
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss", timezone="CET") 
    private Timestamp createTime; // 创建时间

    @OneToMany(cascade = { CascadeType.ALL })
    @JoinColumn(name = "public_commodity_id")
    private List<PublicResource> resources;// 资源列表

    @OneToOne(cascade = { CascadeType.ALL })
    @JoinColumn(name = "split_rule_id")
    private SplitCommodityRule splitRule; // 拆分规则
}