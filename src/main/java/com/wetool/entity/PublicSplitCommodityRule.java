package com.wetool.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Data;

/**
 * 公共库商品拆分规则
 */

@Entity
@Data
public class PublicSplitCommodityRule implements Serializable{
	private static final long serialVersionUID = -2551171381046863194L;

	@Id
	private Long id;
	
	@Column()
	private Long commodityId;// 商品拆分后对应商品编号
	
	@Column()
	private Double splitNumber;// 拆分数量
	
	@Column(name = "_mycat_op_time")
	private Timestamp mycatOpTime;
}
