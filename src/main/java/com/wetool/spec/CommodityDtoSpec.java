package com.wetool.spec;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import com.wetool.model.SearchParam;
import com.wetool.model.dto.CommodityDto;

public class CommodityDtoSpec implements Specification<CommodityDto>{

	
	private final Long merchantId;
	private final SearchParam params;
	private final  Long[] categoryIds;

	public CommodityDtoSpec(Long merchantId, SearchParam params, Long[] categoryIds) {
		this.merchantId = merchantId;
		this.params = params;
		this.categoryIds = categoryIds;
	}

	@Override
	public Predicate toPredicate(Root<CommodityDto> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

		List<Predicate> predicates = new ArrayList<>();
		
		// 商家查询
		predicates.add(cb.equal(root.get("merchantId"), merchantId));
		
		//逻辑删除商品不展示
		predicates.add(cb.equal(root.get("isDeleted"), false));
		
		// 判断商品条码或名称
		if (StringUtils.isNotBlank(params.getKey())) {
			Predicate pred = cb.or(cb.like(cb.lower(root.get("name")), "%" + params.getKey() + "%"));
			pred = cb.or(pred, cb.like(cb.lower(root.get("barcode")), "%" + params.getKey() + "%"));
			pred = cb.or(pred, cb.like(cb.lower(root.get("pinyin")), "%" + params.getKey() + "%"));
			pred = cb.or(pred, cb.like(cb.lower(root.get("pinyinShorthand")), "%" + params.getKey() + "%"));
			predicates.add(pred);
		}
		if (params.getType() != null) {
			predicates.add(cb.equal(root.get("type"), params.getType()));
		}
		if (params.getType() == null && params.getUpdateTime() == null) {
			predicates.add(cb.notEqual(root.get("type"), 3L));
		}
		// 商品类型
		if (params.getCategoryId() != null) {
			Predicate pred = null;
			for(int l = 0 ; l < categoryIds.length ; l++) {
				if (l==0) {
					pred = cb.or(cb.equal(root.get("categoryId"),categoryIds[l]));//拼接sql null不能开头
				}
				pred = cb.or(pred,cb.equal(root.get("categoryId"),categoryIds[l]));
			}
			predicates.add(pred);
		}
		
//		Join<Commodity, InvoicingCommodity> join = root.join("invoicingCommodity",JoinType.LEFT);
//		if (params.getShopId() != null) {
//			predicates.add(cb.or(cb.equal(join.get("shopId"), params.getShopId()),cb.isNull(join.get("shopId"))));
//		}
		
		// 创建时间比对
		// 日期格式大于
//		if (params.getFrom() != null) {
//			predicates.add(cb.greaterThan(root.get("createDate"), params.getFrom()));
//		}
//		if (params.getTo() != null) {
//			predicates.add(cb.lessThan(root.get("createDate"), params.getTo()));
//		}
		
		return andTogether(predicates, cb);
	}

	private Predicate andTogether(List<Predicate> predicates, CriteriaBuilder cb) {
		return cb.and(predicates.toArray(new Predicate[0]));
	}

}
