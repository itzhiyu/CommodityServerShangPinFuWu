package com.wetool.spec.invoicing;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import com.wetool.entity.invoicing.InvoicingCommodity;
import com.wetool.model.invoicing.SearchParam;

public class CommoditySpec implements Specification<InvoicingCommodity> {

	private final SearchParam params;
	private Long[] categoryIds;
	private List<Long> ids;

	public CommoditySpec(SearchParam params,List<Long> ids){
		this.params = params;
		this.ids = ids;
	}
	
	public CommoditySpec(SearchParam params,Long[] categoryIds){
		this.params = params;
		this.categoryIds = categoryIds;
	}
	
	public CommoditySpec(SearchParam params) {
		this.params = params;
	}

	@Override
	public Predicate toPredicate(Root<InvoicingCommodity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		List<Predicate> predicates = new ArrayList<>();
		//商品是否被删除
		predicates.add(cb.and(cb.equal(root.get("isDeleted"), false)));
		if (params.getFlag() == null || params.getFlag() == false) {
			predicates.add(cb.notEqual(root.get("type"), 3L));
		}
		// 判断商品条码或名称
		if (StringUtils.isNotEmpty(params.getKey())) {
			Predicate pred = cb.or(cb.like(cb.lower(root.get("name")), "%" + params.getKey() + "%"));
			pred = cb.or(pred, cb.like(cb.lower(root.get("barcode")), "%" + params.getKey() + "%"));
			predicates.add(pred);
		}
		/* 多件商品ID 查询 */
		if (ids != null && ids.size() > 0) {
			Predicate pred = null;
			for (int l = 0; l < ids.size(); l++) {
				if (l == 0) {// null不能开头
					pred = cb.or(cb.equal(root.get("id"), ids.get(l)));// 拼接sql										
				}else{
					pred = cb.or(pred, cb.equal(root.get("id"), ids.get(l)));
				}
			}
			predicates.add(pred);
		}
		// 商品分类类型
		if (params.getCategoryId() != null && params.getCategoryId() != 1) {
			Predicate pred = null;
			for (int l = 0; l < categoryIds.length; l++) {
				if (l == 0) {
					pred = cb.or(cb.equal(root.get("categoryId"), categoryIds[l]));// 拼接sql
																					// null不能开头
				}
				pred = cb.or(pred, cb.equal(root.get("categoryId"), categoryIds[l]));
			}
			predicates.add(pred);
		}
		// 商品类型查询
		if (params.getTypeId() != null) {
			predicates.add(cb.equal(root.get("type"), params.getTypeId()));
		}
		// 店铺查询
		if (params.getShopId() != null && params.getShopId() > 0) {
			predicates.add(cb.equal(root.get("shopId"), params.getShopId()));
		}
		// 拆分商品查询
		if (params.getSplitCommodityId() != null) {
			predicates.add(cb.equal(root.get("splitRule").get("commodityId"), params.getSplitCommodityId()));
		}
		return andTogether(predicates, cb);
	}

	private Predicate andTogether(List<Predicate> predicates, CriteriaBuilder cb) {
		return cb.and(predicates.toArray(new Predicate[0]));
	}

}
