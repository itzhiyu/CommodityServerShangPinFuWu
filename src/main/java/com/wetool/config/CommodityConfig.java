package com.wetool.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@ComponentScan
public class CommodityConfig extends WebMvcConfigurerAdapter {

    @Bean
    public MessageSource messageSource() {
	ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
	messageSource.setBasename("message.messages");
	messageSource.setDefaultEncoding("UTF-8");
	return messageSource;
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
	LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
	localValidatorFactoryBean.setValidationMessageSource(messageSource());
	return localValidatorFactoryBean;
    }

    @Override
    public org.springframework.validation.Validator getValidator() {
	return validator();
    }
}
