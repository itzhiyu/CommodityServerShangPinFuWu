package com.wetool.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.BasicErrorController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import com.google.common.base.Predicate;
import io.swagger.annotations.ApiOperation;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Value("${zuul.address}")
	private String zuulAddress;	// zuul路由服务IP:端口
	
	@Value("${zuul.api-url}")
	private String zuulApiUrl;	// 当前服务zuul路由路径

	@Bean
    public Docket createRestApi() {
	    ParameterBuilder pb = new ParameterBuilder();
	    pb.name("Authorization").description("token").modelRef(new ModelRef("string")).parameterType("header").required(true).defaultValue("Bearer xxxx").build();
	    List<Parameter> params = Arrays.asList(pb.build());
	    
        Predicate<RequestHandler> predicate = new Predicate<RequestHandler>() {
            @Override
            public boolean apply(RequestHandler input) {
                Class<?> declaringClass = input.declaringClass();
                if (declaringClass == BasicErrorController.class)// 排除
                    return false;
                if(declaringClass.isAnnotationPresent(RestController.class)) // 被注解的类
                    return true;
                if(input.isAnnotatedWith(ApiOperation.class)) // 被注解的方法
                    return true;
                return false;
            }
        };
        
        return new Docket(DocumentationType.SWAGGER_2)
        		.host(zuulAddress)
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .globalOperationParameters(params)
                .pathMapping(zuulApiUrl)
                .select()
                .apis(predicate)
                .build();
    }

	private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Wetool RESTful APIs")
                .description("商品管理接口")
                .termsOfServiceUrl("https://www.wetool.com/")
                .version("1.0")
                .build();
    }
}