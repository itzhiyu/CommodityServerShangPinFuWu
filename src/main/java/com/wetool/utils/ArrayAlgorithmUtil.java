package com.wetool.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wetool.service.CommodityService;

@Component
public class ArrayAlgorithmUtil {
	
	@Autowired
	private CommodityService commodityService;
	
	/**
	 * 为空的最小二维数组下表组合PLU码
	 * @return
	 */
	public  String getMin(Long[][] array,Long merchantId){
		String arrayString = null; // 返回最小编号
		for (int i = 0; i < 1000; i++) {
            for (int j = 0; j < 10; j++) {
            	if (i== 0 && j == 0) {
            		continue;//设定下标0000 过滤
            	}
            	/*找到为空最小下表拼接成4位PLU码*/
            	arrayString = String.format("%04d", i*10 +j);
            	Long count = commodityService.countByBarcodeAndMerchantIdAndIsDeleted(arrayString,merchantId,false);
            	if (count == 0) {
            		return arrayString;
            	}
            }
        }
		return arrayString;
	}
}
