package com.wetool.utils;

import java.util.Comparator;
import java.util.Date;

import com.wetool.entity.invoicing.InvoicingRecord;

/**
 * List时间排序
 */
public class ComparatorDate implements Comparator<Object> {

	public int compare(Object obj1, Object obj2) {
		InvoicingRecord t1 = (InvoicingRecord) obj1;
		InvoicingRecord t2 = (InvoicingRecord) obj2;
		// return t1.getTradetime().compareTo(t2.getTradetime()); //

		Date d1, d2;
		d1 = t1.getCreateDate();
		d2 = t2.getCreateDate();
		if (d1.before(d2)) {
			return 1;
		} else {
			return -1;
		}
	}
}
