/* 
 * Copyright (c) 2016, W.T. Co., Ltd.   All rights reserved.
 */
package com.wetool.utils;

/**
 * 描述：进销存，库存常量
 * 
 * <pre>HISTORY
 * ****************************************************************************
 *  ID   DATE           PERSON          REASON
 *  1    2016年7月12日      laicp         Create
 * ****************************************************************************
 * </pre>
 * @author laicp
 * @since 1.0
 */
public interface WarehouseContants {
    
    /**
     * 入库单
     */
    byte WARE_IN = 2;
    /**
     * 出库单
     */
    byte WARE_OUT = 1;
    
    /**
    * 库存调整操作类型：采购入库
    */
    public static final byte OPERATE_TYPE_PURCHASE_INSTOCK = 5;
    /**
     * 库存调整操作类型：采购退货
     */
    public static final byte OPERATE_TYPE_PURCHASE_T_OUTSTOCK = 0;
    /**
     * 库存调整操作类型：采购换货
     */
    public static final byte OPERATE_TYPE_PURCHASE_H_OUTSTOCKK = 1;
    /**
     * 库存调整操作类型：破损出库
     */
    public static final byte OPERATE_TYPE_DAMAGED_OUTSTOCK = 2;
    /**
     * 库存调整操作类型：其他出库
     */
    public static final byte OPERATE_TYPE_OTHER_OUTSTOCK = 3;
    
    /**
     * 入库前缀
     */
    String FLAG_IN = "RK";
    
    /**
     * 出库前缀
     */
    String FLAG_OUT = "CK";
    
    /**
     * memcache 缓存KEY
     */
    String cache_ruku_key = "cache_rk_seq";
    
    /**
     * memcache 缓存KEY
     */
    String cache_chuku_key = "cache_ck_seq";
}
