package com.wetool.utils;

import org.pentaho.di.core.KettleEnvironment; 
import org.pentaho.di.job.Job; 
import org.pentaho.di.job.JobMeta; 


public class Transformation {  
  
    public static void runJob(String[] params, String jobPath) {  
        try {  
            KettleEnvironment.init(); 
            JobMeta jobMeta = new JobMeta(jobPath, null);
            Job job = new Job(null, jobMeta);  
            job.setVariable("db-host", params[0]);  
            job.setVariable("report", params[1]);  
            job.setVariable("password", params[2]);  
            job.setVariable("merchantId", params[3]);  
            job.setVariable("shopId", params[4]);  
            job.setVariable("filePath", params[5]);  
            job.start();  
            job.waitUntilFinished();  
            if (job.getErrors() > 0) {  
                throw new Exception("There are errors during job exception!(执行job发生异常)");  
            }  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
    } 
    
} 
