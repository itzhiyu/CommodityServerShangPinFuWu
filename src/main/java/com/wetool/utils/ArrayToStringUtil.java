package com.wetool.utils;

import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


/**
 * 数组反序列化和序列化 工具类
 *
 * @author lixin
 */
public class ArrayToStringUtil {

    /**
     * 二维数组序列化
     *
     * @param array
     * @return
     * @throws Exception
     */
    public static String encode(Long[][] array) throws Exception {
        String result = null;
        /* 创建字节输入流 */
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
		/* 创建对象输入流 */
        ObjectOutputStream oos = new ObjectOutputStream(bos);
		/*数组写入流中*/
        oos.writeObject(array);
        oos.flush();
        oos.close();
        byte[] b = bos.toByteArray();//读取流中的数组转成byte数组
        result = Base64.encodeBase64String(b);//byte数组转成字符串
        return result;
    }

    /**
     * 二维数组反序列化
     *
     * @param s
     * @return
     * @throws Exception
     */
    public static Long[][] decode(String s) throws Exception {
        Long[][] result = null;
//		Base64 base64 = new Base64();
		/* 将字符串反编译成byte数组 */
        byte[] b = Base64.decodeBase64(s);
        ByteArrayInputStream bis = new ByteArrayInputStream(b);
        ObjectInputStream ois = new ObjectInputStream(bis);
        result = (Long[][]) ois.readObject();//从流中写回数组
        ois.close();//关闭流
        return result;
    }

}
