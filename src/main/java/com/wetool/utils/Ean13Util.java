package com.wetool.utils;

/**
 * 商品条形码验证是否符gs1规则
 * @author lixin
 */
public class Ean13Util {

	public static Boolean getVerificationEan13(String barcord) {
		boolean flag = false;
		// 传入字符串是否为空
		if (barcord == null) {
			return flag;
		}
		// 截取最后一位做判断
		String last = barcord.substring(barcord.length() - 1, barcord.length());

		// 截取前12位
		barcord = barcord.substring(0, barcord.length() - 1);
		if (barcord.length() != 12) {
			return flag;
		}
		Integer lastNumber = Integer.parseInt(last);
		if (lastNumber != null) {
			// 商品条形码前12位算法验证最后一位
			int changeNumber = change(barcord);
			if (lastNumber == changeNumber) {
				flag = true;
			}
		}
		return flag;
	}

	public static int change(String barcord) {
		/*
		 * 第一步：取出该数的奇数位的和: 第二步：取出该数的偶数位的和：
		 */
		int c1 = 0;// 奇数位的和
		int c2 = 0;// 偶数位的和
		// i=0,2,4,6,8,10 奇数位的值
		// i+1 1,3,5,7,9,11
		// 取奇数、偶数
		for (int i = 0; i < barcord.length(); i += 2) {
			// 奇数位值
			char c = barcord.charAt(i);
			// 奇数位的和
			c1 = c1 + c - 48;
			// 偶数位的值
			char c3 = barcord.charAt(i + 1);
			// 偶数位的和
			c2 = c2 + c3 - 48;
		}
		// 计算奇数位值的和
		int cc = c1 + c2 * 3; // 110
		// 去结果的个位数
		cc %= 10;
		// 用十减去这个个位数
		cc = 10 - cc;
		cc = cc % 10;
		return cc;
	}
}
