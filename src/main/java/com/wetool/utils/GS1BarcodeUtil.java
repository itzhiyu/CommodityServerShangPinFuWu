package com.wetool.utils;

/**
 * 商品条码匹配归属地或作用正则
 * @author lixin
 */
import java.lang.reflect.Field;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GS1BarcodeUtil {
	public static final String GS1_BARCODE = "^950\\d{10}$"; // GS1总部
	public static final String GS1_ELECTRONIC_BARCODE = "^951\\d{10}$";// GS1总部(产品电子代码)
	public static final String GS1_SHORTENED_BARCODE = "^96[0-9]\\d{10}$";// GS1总部(缩短码)
	public static final String DISCOUNT_BARCODE = "^(05[0-9]|99[0-9])\\d{10}$"; // 优惠卷
	public static final String CIRCULATION_BARCODE = "^98[123]\\d{10}$"; // 普通流通券
	public static final String BOOK_BARCODE = "^97[89]\\d{10}$";// 图书
	public static final String BILLS_RECEIVABLE_BARCODE = "^980\\d{10}$";// 应收票据
	public static final String SERIALS_BARCODE = "^977\\d{10}$"; // 连刊出版物
	public static final String SHOP_BARCODE = "^(02[0-9]|04[0-9]|2[0-9][0-9])\\d{10}$";// 店内码
	public static final String CN_BARCODE = "^69[0-9]\\d{10}$"; // 中国
	public static final String US_BARCODE = "^(0[01][0-9]|03[0-9]|0[6-9]\\d|1[012]\\d|13[0-9])\\d{10}$"; // 美国
	public static final String GB_BARCODE = "^50[0-9]\\d{10}$"; // 英国
	public static final String FR_BARCODE = "^3[0-7][0-9]\\d{10}$"; // 法国
	public static final String DE_BARCODE = "^(4[0-3][0-9]|440)\\d{10}$"; // 德国
	public static final String JP_BARCODE = "^(45[0-9]|49[0-9])\\d{10}$"; // 日本
	public static final String RU_BARCODE = "^46[0-9]\\d{10}$"; // 俄罗斯
	public static final String HK_BARCODE = "^489\\d{10}$"; // 中国香港特别行政区
	public static final String MO_BARCODE = "^958\\d{10}$"; // 中国澳门特别行政区
	public static final String TW_BARCODE = "^471\\d{10}$"; // 台湾
	public static final String GR_BARCODE = "^52[01]\\d{10}$"; // 希腊
	public static final String BE_LU_BARCODE = "^54[0-9]\\d{10}$"; // 比利时和卢森堡
	public static final String BG_BARCODE = "^380\\d{10}$"; // 保加利亚
	public static final String SI_BARCODE = "^383\\d{10}$"; // 斯洛文尼亚
	public static final String HR_BARCODE = "^385\\d{10}$";// 克罗地亚
	public static final String BA_BARCODE = "^387\\d{10}$"; // 波黑
	public static final String ME_BARCODE = "^389\\d{10}$"; // 黑山共和国
	public static final String KGZ_BARCODE = "^470\\d{10}$"; // 吉尔吉斯斯坦
	public static final String EE_BARCODE = "^474\\d{10}$"; // 爱沙尼亚
	public static final String LV_BARCODE = "^475\\d{10}$"; // 拉脱维亚
	public static final String AZ_BARCODE = "^476\\d{10}$"; // 阿塞拜疆
	public static final String LT_BARCODE = "^477\\d{10}$"; // 立陶宛
	public static final String UZ_BARCODE = "^478\\d{10}$"; // 乌兹别克斯坦
	public static final String LK_BARCODE = "^479\\d{10}$"; // 斯里兰卡
	public static final String PH_BARCODE = "^480\\d{10}$"; // 菲律宾
	public static final String BY_BARCODE = "^481\\d{10}$"; // 白俄罗斯
	public static final String UA_BARCODE = "^482\\d{10}$"; // 立陶宛
	public static final String MD_BARCODE = "^484\\d{10}$"; // 摩尔多瓦
	public static final String AM_BARCODE = "^485\\d{10}$"; // 亚美尼亚
	public static final String GE_BARCODE = "^486\\d{10}$"; // 格鲁吉亚
	public static final String KZ_BARCODE = "^487\\d{10}$"; // 哈萨克斯坦
	public static final String TJ_BARCODE = "^488\\d{10}$"; // 塔吉克斯坦
	public static final String LB_BARCODE = "^528\\d{10}$"; // 黎巴嫩
	public static final String CY_BARCODE = "^529\\d{10}$"; // 塞浦路斯
	public static final String AL_BARCODE = "^530\\d{10}$"; // 阿尔巴尼亚
	public static final String MK_BARCODE = "^531\\d{10}$"; // 马其顿
	public static final String MT_BARCODE = "^535\\d{10}$"; // 马耳他
	public static final String IE_BARCODE = "^539\\d{10}$"; // 爱尔兰
	public static final String PT_BARCODE = "^560\\d{10}$"; // 葡萄牙
	public static final String IS_BARCODE = "^569\\d{10}$"; // 冰岛
	public static final String DK_BARCODE = "^57[0-9]\\d{10}$"; // 丹麦
	public static final String PL_BARCODE = "^590\\d{10}$"; // 波兰
	public static final String RO_BARCODE = "^594\\d{10}$"; // 罗马尼亚
	public static final String HU_BARCODE = "^599\\d{10}$"; // 匈牙利
	public static final String ZA_BARCODE = "^60[01]\\d{10}$"; // 南非
	public static final String GH_BARCODE = "^603\\d{10}$"; // 加纳
	public static final String SN_BARCODE = "^604\\d{10}$"; // 塞内加尔
	public static final String BH_BARCODE = "^608\\d{10}$"; // 巴林
	public static final String MU_BARCODE = "^609\\d{10}$"; // 毛里求斯
	public static final String MA_BARCODE = "^611\\d{10}$"; // 摩洛哥
	public static final String DZ_BARCODE = "^613\\d{10}$"; // 阿尔及利亚
	public static final String NG_BARCODE = "^615\\d{10}$"; // 尼日利亚
	public static final String KE_BARCODE = "^616\\d{10}$"; // 肯尼亚
	public static final String CI_BARCODE = "^618\\d{10}$"; // 象牙海岸
	public static final String TN_BARCODE = "^619\\d{10}$"; // 突尼斯
	public static final String SY_BARCODE = "^621\\d{10}$"; // 叙利亚
	public static final String EG_BARCODE = "^608\\d{10}$"; // 埃及
	public static final String LY_BARCODE = "^624\\d{10}$"; // 利比亚
	public static final String JO_BARCODE = "^625\\d{10}$"; // 约旦
	public static final String IR_BARCODE = "^626\\d{10}$"; // 伊朗
	public static final String KW_BARCODE = "^627\\d{10}$"; // 科威特
	public static final String SA_BARCODE = "^628\\d{10}$"; // 沙特阿拉伯
	public static final String AE_BARCODE = "^629\\d{10}$"; // 阿拉伯联合酋长国
	public static final String FI_BARCODE = "^64[0-9]\\d{10}$"; // 芬兰
	public static final String NO_BARCODE = "^70[0-9]\\d{10}$"; // 挪威
	public static final String IL_BARCODE = "^729\\d{10}$"; // 以色列
	public static final String SE_BARCODE = "^73[0-9]\\d{10}$"; // 瑞典
	public static final String GT_BARCODE = "^740\\d{10}$"; // 危地马拉
	public static final String SV_BARCODE = "^741\\d{10}$"; // 萨尔瓦多
	public static final String HN_BARCODE = "^742\\d{10}$"; // 洪都拉斯
	public static final String NI_BARCODE = "^743\\d{10}$"; // 尼加拉瓜
	public static final String CR_BARCODE = "^744\\d{10}$"; // 哥斯达黎加
	public static final String PA_BARCODE = "^745\\d{10}$"; // 巴拿马
	public static final String DO_BARCODE = "^746\\d{10}$"; // 多米尼加
	public static final String MX_BARCODE = "^750\\d{10}$"; // 墨西哥
	public static final String CA_BARCODE = "^75[45]\\d{10}$"; // 加拿大
	public static final String VE_BARCODE = "^759\\d{10}$"; // 委内瑞拉
	public static final String CH_BARCODE = "^76[0-9]\\d{10}$"; // 瑞士
	public static final String CO_BARCODE = "^77[01]\\d{10}$"; // 哥伦比亚
	public static final String UY_BARCODE = "^773\\d{10}$"; // 乌拉圭
	public static final String PE_BARCODE = "^775\\d{10}$"; // 秘鲁
	public static final String BO_BARCODE = "^777\\d{10}$"; // 玻利维亚
	public static final String AR_BARCODE = "^77[89]\\d{10}$"; // 阿根廷
	public static final String CL_BARCODE = "^780\\d{10}$"; // 智利
	public static final String PY_BARCODE = "^784\\d{10}$"; // 巴拉圭
	public static final String EC_BARCODE = "^786\\d{10}$"; // 厄瓜多尔
	public static final String BR_BARCODE = "^7[89][09]\\d{10}$"; // 巴西
	public static final String IT_BARCODE = "^(8[0-3][0-9])\\d{10}$"; // 意大利
	public static final String ES_BARCODE = "^84[0-9]\\d{10}$"; // 西班牙
	public static final String CU_BARCODE = "^850\\d{10}$"; // 古巴
	public static final String SK_BARCODE = "^858\\d{10}$"; // 斯洛伐克
	public static final String CS_BARCODE = "^859\\d{10}$"; // 捷克
	public static final String YU_BARCODE = "^860\\d{10}$"; // 南斯拉夫
	public static final String MN_BARCODE = "^865\\d{10}$"; // 蒙古
	public static final String KP_BARCODE = "^867\\d{10}$"; // 朝鲜
	public static final String TR_BARCODE = "^86[89]\\d{10}$"; // 土耳其
	public static final String NL_BARCODE = "^87[0-9]\\d{10}$"; // 荷兰
	public static final String KR_BARCODE = "^880\\d{10}$"; // 韩国
	public static final String KH_BARCODE = "^884\\d{10}$"; // 柬埔寨
	public static final String TH_BARCODE = "^885\\d{10}$"; // 泰国
	public static final String SG_BARCODE = "^888\\d{10}$"; // 新加坡
	public static final String IN_BARCODE = "^890\\d{10}$"; // 印度
	public static final String VN_BARCODE = "^893\\d{10}$"; // 越南
	public static final String PK_BARCODE = "^896\\d{10}$"; // 巴基斯坦
	public static final String ID_BARCODE = "^899\\d{10}$"; // 印度尼西亚
	public static final String AT_BARCODE = "^9[01][0-9]\\d{10}$"; // 奥地利
	public static final String AU_BARCODE = "^93[0-9]\\d{10}$"; // 澳大利亚
	public static final String NZ_BARCODE = "^94[0-9]\\d{10}$"; // 新西兰
	public static final String MY_BARCODE = "^955\\d{10}$"; // 马来西亚

	/**
	 * 商品条码正则匹配归属地或作用
	 * @param barcode 商品条码
	 * @return
	 * @throws IllegalArgumentException
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 */
	public static String getCountryCode(String barcode)
			throws IllegalArgumentException, ClassNotFoundException, IllegalAccessException {
		Class<?> clazz = Class.forName("com.wetool.utils.GS1BarcodeUtil");
		Field[] fields = clazz.getFields();
		String name = null;
		if (barcode == null || "".equals(barcode)) {
			return null;	
		}
		for (int i = 0; i < fields.length; i++) {
			String regular = (String) fields[i].get(clazz);
			// 编译正则表达式
			Pattern pattern = Pattern.compile(regular);
			// 忽略大小写的写法
			// Pattern pat = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(barcode);
			boolean rs = matcher.matches();
			if (rs) {
				name = fields[i].getName().substring(0, fields[i].getName().lastIndexOf("_"));	//问题B 枚举型改造
			}
		}
		if (name == null) {
			name = "OTHER";
		}
		return name;
	}
}
