/* 
 * Copyright (c) 2017, S.F. Express Inc. All rights reserved.
 */
package com.wetool;

import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * 描述：全局控制器配置
 * @author zhangjie
 * @since 1.0
 */
@ControllerAdvice
public class GlobalControllerAdvice {

}