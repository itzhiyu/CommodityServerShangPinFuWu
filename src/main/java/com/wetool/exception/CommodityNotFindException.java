package com.wetool.exception;

/**
 * 描述：查询商品不存在异常
 * 查询商品商品信息不存在
 * @author lixin
 */
public class CommodityNotFindException extends Exception {
	private static final long serialVersionUID = -7407009130742453607L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public CommodityNotFindException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public CommodityNotFindException() {
		super();
	}
}
