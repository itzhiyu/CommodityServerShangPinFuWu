package com.wetool.exception;

/**
 * 描述：修改商品信息失败异常
 * 修改商品同步到店铺数商品失败
 * @author lixin
 *
 */
public class CommodityUpdateException extends Exception {
	private static final long serialVersionUID = -1515503962446429812L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public CommodityUpdateException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public CommodityUpdateException() {
		super();
	}
}
