package com.wetool.exception;

/**
 * 描述：商家未登录异常
 * @author lixin
 *
 */
public class MerchantNotLoginException extends Exception {

	private static final long serialVersionUID = 6616374042270976751L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public MerchantNotLoginException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public MerchantNotLoginException() {
		super();
	}
}
