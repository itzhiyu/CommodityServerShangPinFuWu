package com.wetool.exception;

/**
 * 描述：商品分类名称重复
 * @author lixin
 */
public class CategoryNameRepetitionException extends Exception {
	private static final long serialVersionUID = 1357306644939651811L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public CategoryNameRepetitionException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public CategoryNameRepetitionException() {
		super();
	}
}
