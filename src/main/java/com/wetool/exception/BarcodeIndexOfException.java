package com.wetool.exception;

/**
 * 称重商品条码长度越界
 * @author lixin
 */
public class BarcodeIndexOfException extends Exception {
	private static final long serialVersionUID = 3225260211635570146L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public BarcodeIndexOfException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public BarcodeIndexOfException() {
		super();
	}
}
