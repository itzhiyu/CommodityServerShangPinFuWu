/* 
 * Copyright (c) 2017, S.F. Express Inc. All rights reserved.
 */
package com.wetool.exception;

/**
 * 描述：数据关联异常
 * 当前商品目录下如果已存在商品数据，不允许删除或转移操作
 * 
 * @author zhangjie
 * @since 1.0
 */
public class DataRelationException extends Exception {
	private static final long serialVersionUID = 1L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}
	public DataRelationException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}
	public DataRelationException() {
		super();
	}
}
