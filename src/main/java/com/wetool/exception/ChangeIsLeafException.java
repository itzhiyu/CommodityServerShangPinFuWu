/* 
 * Copyright (c) 2017, S.F. Express Inc. All rights reserved.
 */
package com.wetool.exception;

/**
 * 描述： 改变节点类型异常
 * 不允许改变节点isLeaf属性
 * 
 * @author zhangjie
 * @since 1.0
 */
public class ChangeIsLeafException extends Exception {
	private static final long serialVersionUID = 1L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}
	public ChangeIsLeafException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}
	public ChangeIsLeafException() {
		super();
	}

}
