package com.wetool.exception;

/**
 * 描述：商品分类找不到异常
 * @author lixin
 */
public class CategoryNotFindException extends Exception {

	private static final long serialVersionUID = -4481735307461172646L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public CategoryNotFindException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public CategoryNotFindException() {
		super();
	}
}
