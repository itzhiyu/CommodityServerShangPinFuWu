/* 
 * Copyright (c) 2017, S.F. Express Inc. All rights reserved.
 */
package com.wetool.exception;

/**
 * 描述： 目录节点关联异常
 * 当前商品目录下如果已存在子目录，不允许删除或转移操作
 * 
 * @author zhangjie
 * @since 1.0
 */
public class NodeRelationException extends Exception {
	private static final long serialVersionUID = 1L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}
	public NodeRelationException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}
	public NodeRelationException() {
		super();
	}

}
