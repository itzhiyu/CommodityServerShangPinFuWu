package com.wetool.exception;

/**
 * 描述:PLU保存DB失败
 * @author lixin
 */
public class PLUSaveException extends Exception{
	private static final long serialVersionUID = 6594829575300103361L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public PLUSaveException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public PLUSaveException() {
		super();
	}
}
