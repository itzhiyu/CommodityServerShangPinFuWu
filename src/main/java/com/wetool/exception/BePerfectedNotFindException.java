package com.wetool.exception;

/**
 * 描述:快录商品信息不存在异常
 * @author lixin
 */
public class BePerfectedNotFindException extends Exception {
	private static final long serialVersionUID = -7618173883245970323L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public BePerfectedNotFindException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public BePerfectedNotFindException() {
		super();
	}
}
