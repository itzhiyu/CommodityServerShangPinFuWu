package com.wetool.exception;

/**
 * 商品添加失败异常
 * @author lixin
 */
public class CommodityAddException extends Exception {
	private static final long serialVersionUID = -7575641749519865261L;
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public CommodityAddException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public CommodityAddException() {
		super();
	}
}
