package com.wetool.exception;

import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

/**
 * 描述： 参数校验异常
 * @author zhangjie
 * @since 1.0
 */
public class ParamCheckException extends BindException {
	private static final long serialVersionUID = -38758609179015099L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public ParamCheckException(BindingResult bindingResult) {
		super(bindingResult);
	}

}
