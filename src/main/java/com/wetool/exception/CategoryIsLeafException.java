package com.wetool.exception;

/**
 * 商品分类目录添加商品异常
 * @author lixin
 */
public class CategoryIsLeafException extends Exception {
	private static final long serialVersionUID = -5160164627984157201L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public CategoryIsLeafException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public CategoryIsLeafException() {
		super();
	}
}
