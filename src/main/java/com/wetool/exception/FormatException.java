package com.wetool.exception;

/**
 * 描述：格式异常
 * @author lixin
 *
 */
public class FormatException extends Exception {

	private static final long serialVersionUID = 6616374042270976751L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public FormatException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public FormatException() {
		super();
	}
}
