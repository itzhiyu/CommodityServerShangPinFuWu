package com.wetool.exception.invoicing;

/**
 * 描述：商品下有拆分商品异常
 * @author lixin
 */
public class SplitCommodityException extends Exception {
	private static final long serialVersionUID = -647775331900041409L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public SplitCommodityException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public SplitCommodityException() {
		super();
	}

}
