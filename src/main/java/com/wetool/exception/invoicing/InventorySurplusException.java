package com.wetool.exception.invoicing;

/**
 * 描述：商品库仍有库存异常
 * @author lixin
 */
public class InventorySurplusException extends Exception {
	private static final long serialVersionUID = -7025950818014158634L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public InventorySurplusException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public InventorySurplusException() {
		super();
	}
}
