package com.wetool.exception.invoicing;

/**
 * 描述：入库信息未找到异常
 * @author lixin
 */
public class StorageNotFindException extends Exception {
	private static final long serialVersionUID = 4468698536359706998L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public StorageNotFindException(String message) {
		super(message);
	}

	public StorageNotFindException() {
		super();
	}

}
