package com.wetool.exception.invoicing;

/**
 * 描述：添加供应商异常
 * @author lixin
 */
public class AddSupplierException extends Exception {
	private static final long serialVersionUID = -7135086041690505355L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public AddSupplierException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public AddSupplierException() {
		super();
	}
}
