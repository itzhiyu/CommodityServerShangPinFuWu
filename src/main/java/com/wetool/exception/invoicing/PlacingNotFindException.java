package com.wetool.exception.invoicing;

/**
 * 描述：出库信息没有异常
 * @author lixin
 */
public class PlacingNotFindException extends Exception {
	private static final long serialVersionUID = -417289064481247964L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public PlacingNotFindException(String message) {
		super(message);
	}

	public PlacingNotFindException() {
		super();
	}
}
