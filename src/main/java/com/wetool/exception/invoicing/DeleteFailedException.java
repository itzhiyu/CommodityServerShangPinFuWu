package com.wetool.exception.invoicing;

/**
 * 描述:删除失败异常
 * @author lixin
 *
 */
public class DeleteFailedException extends Exception{
	private static final long serialVersionUID = -9106523845207701958L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public DeleteFailedException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public DeleteFailedException() {
		super();
	}
}
