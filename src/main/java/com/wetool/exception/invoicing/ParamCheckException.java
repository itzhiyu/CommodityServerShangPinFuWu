package com.wetool.exception.invoicing;

import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

/**
 * 描述： 参数校验异常
 * @author lixin
 */
public class ParamCheckException extends BindException {

	private static final long serialVersionUID = -5799530499939619218L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public ParamCheckException(BindingResult bindingResult) {
		super(bindingResult);
	}

}
