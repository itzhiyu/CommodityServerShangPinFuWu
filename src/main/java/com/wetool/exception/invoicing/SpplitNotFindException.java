package com.wetool.exception.invoicing;

/**
 * 描述：未找到供应商信息
 * 查询为空或逻辑删除时抛出
 * @author lixin
 */
public class SpplitNotFindException extends Exception {
	private static final long serialVersionUID = -5848441061416775991L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public SpplitNotFindException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public SpplitNotFindException() {
		super();
	}
}
