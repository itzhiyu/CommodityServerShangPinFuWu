package com.wetool.exception.invoicing;

/**
 * 描述：添加出库异常
 * @author lixin
 */
public class AddPlacingException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public AddPlacingException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public AddPlacingException() {
		super();
	}

}
