package com.wetool.exception.invoicing;

/**
 * 描述：库存不足异常
 * @author lixin
 */
public class InsufficientStockException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public InsufficientStockException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public InsufficientStockException() {
		super();
	}
}
