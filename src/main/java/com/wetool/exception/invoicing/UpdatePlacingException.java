package com.wetool.exception.invoicing;

/**
 * 描述：出库修改异常
 * @author lixin
 */
public class UpdatePlacingException extends Exception {
	private static final long serialVersionUID = 3998221037879818865L;
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public UpdatePlacingException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public UpdatePlacingException() {
		super();
	}
}
