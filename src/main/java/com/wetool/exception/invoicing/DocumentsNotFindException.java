package com.wetool.exception.invoicing;

/**
 * 描述：无出入库信息
 * 查询根据商品查询出入库信息时商品无出入库信息抛出
 * @author lixin
 *
 */
public class DocumentsNotFindException extends Exception {
	private static final long serialVersionUID = 8878481321113477171L;
	
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public DocumentsNotFindException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public DocumentsNotFindException() {
		super();
	}
}
