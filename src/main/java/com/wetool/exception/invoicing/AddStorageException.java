package com.wetool.exception.invoicing;

/**
 * 描述：添加入库信息异常
 * @author lixin
 */
public class AddStorageException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public AddStorageException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public AddStorageException() {
		super();
	}

}
