package com.wetool.exception.invoicing;

/**
 * 描述：店铺未登录
 * @author lixin
 */
public class ShopNotLoginException extends Exception {
	private static final long serialVersionUID = 4577949324376530860L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public ShopNotLoginException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public ShopNotLoginException() {
		super();
	}
}
