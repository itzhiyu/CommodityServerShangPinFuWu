package com.wetool.exception.invoicing;

/**
 * 描述：修改入库信息异常
 * @author lixin
 */
public class UpdateStorageException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public UpdateStorageException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public UpdateStorageException() {
		super();
	}

}
