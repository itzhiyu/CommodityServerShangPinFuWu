package com.wetool.exception.invoicing;

/**
 * 描述：入库单据提交异常
 * 入库数据已提交
 * @author lixin
 */
public class BillException extends Exception {
	private static final long serialVersionUID = 3613467930312261971L;
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public BillException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public BillException() {
		super();
	}
}
