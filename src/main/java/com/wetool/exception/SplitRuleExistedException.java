package com.wetool.exception;

/**
 * 描述:拆分规则存在异常
 * 商品存在拆分商品
 * @author lixin
 */
public class SplitRuleExistedException extends Exception{
	private static final long serialVersionUID = -2160031495170962639L;
	
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public SplitRuleExistedException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public SplitRuleExistedException() {
		super();
	}
}
