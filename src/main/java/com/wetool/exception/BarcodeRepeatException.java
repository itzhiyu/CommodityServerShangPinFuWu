package com.wetool.exception;

/**
 * 描述:快录商品条形码重复异常
 * 跨录商品条形码不能重复
 * @author li
 */
public class BarcodeRepeatException extends Exception {
	private static final long serialVersionUID = 3886698451982557434L;

	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public BarcodeRepeatException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public BarcodeRepeatException() {
		super();
	}
}
