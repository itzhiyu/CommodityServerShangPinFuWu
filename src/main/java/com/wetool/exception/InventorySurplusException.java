package com.wetool.exception;

/**
 * 商品仍有库存
 * @author li
 *
 */
public class InventorySurplusException extends Exception {
	private static final long serialVersionUID = -7411252162792011118L;
	
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public InventorySurplusException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public InventorySurplusException() {
		super();
	}
}
