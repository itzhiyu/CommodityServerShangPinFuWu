package com.wetool;

import java.io.Serializable;

public class Constant implements Serializable{
	private static final long serialVersionUID = 7732529959423181169L;
	public static final String BASH_PATH = "http://wetool.oss-cn-beijing.aliyuncs.com/";
	
    /* 正则表达式规则定义 */
    public static final String REG_MOBILE = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[0-9])|(18[0-9]))\\d{8}$"; // 手机号码格式
    
    /**入库单类型编号*/
    public static final  byte WARE_IN = 2;
    
    /**出库单类型编号*/
    public static final  byte WARE_OUT = 1;
    
    /**线上销售出库*/
    public static final int ONLINE_SALES_PLACING = 7;
    
    /**店铺销售出库*/
    public static final int SALES_PLACING  = 8;
    
    /**商品退换货入库*/
    public static final int REFUND_STORAGE = 5;
    
    /**商品拆分入库*/
    public static final Long SPLIT_STORAGE = 2L;

    /**商品拆分入库*/
    public static final Long SPLIT_PLACING = 13L;
    
    /**入库前缀*/
    public static final String FLAG_IN = "RK";
    
    /** 出库前缀 */
    public static final  String FLAG_OUT = "CK";
    
    /** 出入库单据提交 */
    public static final int BILL_SUBMIT = 1;
    
    /** 出入库单据保存 */
    public static final int BILL_SAVE = 0;
    
    /**memcache 缓存KEY*/
    public static final  String cache_ruku_key = "cache_rk_seq";
    
    /** memcache 缓存KEY*/
    public static final String cache_chuku_key = "cache_ck_seq";

}