package com.wetool.jpa;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.wetool.entity.Commodity;
import com.wetool.entity.SplitCommodityRule;

/**
 * 商品分类仓库（商家库）
 * @author lixin
 *
 */
@Repository
public interface SplitCommodityRuleRepo extends JpaRepository<SplitCommodityRule, Long> {

	@Query("select s from Commodity c inner join c.splitRule s where s.id = c.splitRule.id and c.isDeleted = false and s.commodityId = :id ")
	List<SplitCommodityRule> findByCommodityId(@Param("id") Long commodityId);

	@Query("select c from Commodity c inner join c.splitRule s where s.id = c.splitRule.id and c.isDeleted = false and s.commodityId = :id ")
	List<Commodity> findBySplit(@Param("id") Long commodityId);

}
