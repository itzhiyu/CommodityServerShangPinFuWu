package com.wetool.jpa;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.wetool.entity.Commodity;
import com.wetool.entity.PublicCommodity;
import com.wetool.entity.SplitCommodityRule;
import com.wetool.model.BePerfectedCommodity;

/**
 * 商品信息仓库（商家库）
 * @author lixin
 *
 */
@Repository
public interface CommodityRepo extends JpaRepository<Commodity, Long>,
	JpaSpecificationExecutor<Commodity> {

    Commodity findByMerchantIdAndId(Long merchantId, Long id);

    List<PublicCommodity> findByIdBetween(int start, int limit);
    
    List<Commodity> getByMerchantIdAndSplitRule(Long merchantId,SplitCommodityRule scr);
    /**
	 * 根据商品名称和价格智能匹配商品 美团专用
	 * @return
	 * List<Commodity>
	 */
    @Query("select t from Commodity t where shopId = :shopId and t.sellingPrice= :price and t.name like :name")
    List<Commodity> findCommodityByNameAndPrice(Long shopId,String commodityName,BigDecimal price);

	Long countByMerchantIdAndType(Long merchantId, Long type);

	Long countByMerchantIdAndBarcodeAndType(Long merchantId, String barcode, Long type);

	@Query("select new com.wetool.model.BePerfectedCommodity(t.id,t.merchantId,t.barcode,t.name,t.sellingPrice,t.createDate) "
			+ "from Commodity t where t.merchantId = :merchantId and t.type = :type and t.isDeleted = false")
	Page<BePerfectedCommodity> findByMerchantIdAndType(@Param("merchantId")Long merchantId,@Param("type") Long type, Pageable p);
	
	@Query("select new com.wetool.model.BePerfectedCommodity(t.id,t.merchantId,t.barcode,t.name,t.sellingPrice,t.createDate) "
			+ "from Commodity t where t.merchantId = :merchantId and t.type = :type and t.barcode = :key and t.isDeleted = false")
	Page<BePerfectedCommodity> findByMerchantIdAndType(@Param("merchantId")Long merchantId,@Param("type") Long type,@Param("key")String key, Pageable p);

	Long countByMerchantIdAndBarcode(Long merchantId, String barcode);

	Long countByBarcodeAndMerchantIdAndIsDeleted(String arrayString, Long shopId, boolean b);

	Page<Commodity> findByMerchantId(Long merchantId,Pageable p);

	Long countByCategoryIdAndIsDeletedAndMerchantIdIsNotNullAndTypeNot(Long categoryId, boolean b, Long i);

	@Query("select t from Commodity t where t.splitRule.commodityId = :id")
	List<Commodity> findByCommodity(@Param("id")Long id);

	Commodity getByIdAndIsDeleted(Long id, boolean b);

	Long countByMerchantIdAndBarcodeAndIsDeletedAndBarcodeIsNotNull(Long merchantId, String barcode, boolean b);

	Long countByMerchantIdAndBarcodeAndIsDeletedAndIdNotAndBarcodeIsNotNull(Long merchantId, String barcode, boolean b, Long id);

	List<Commodity> findByBarcodeAndMerchantIdAndIsDeleted(String key, Long merchantId, boolean b);

	Long countByMerchantIdAndTypeAndIsDeleted(Long merchantId, Long type, boolean flag);

	Long countByMerchantIdAndBarcodeAndTypeAndIsDeleted(Long merchantId, String barcode, Long type, boolean b);

	Page<Commodity> findByMerchantIdAndUpdateDateBetween(Long merchantId,Date updateTime,Date date, Pageable p);

	@Query("select c.name from Commodity c where id = :id and c.isDeleted = false")
	String getNameByIdAndIsDeleted(@Param("id") Long commodityId);

}
