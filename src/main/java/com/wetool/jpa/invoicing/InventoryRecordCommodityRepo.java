package com.wetool.jpa.invoicing;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.wetool.entity.invoicing.InventoryRecordCommodity;

/**
 * 描述：入库商品信息JPA仓库
 * @author lixin
 */
public interface InventoryRecordCommodityRepo
		extends JpaRepository<InventoryRecordCommodity, Long>, JpaSpecificationExecutor<InventoryRecordCommodity> {

	@Query(value = "select t from InventoryRecordCommodity t where t.commodityId = :id")
	List<InventoryRecordCommodity> findByCommodityId(@Param("id") Long commodityId);

	List<InventoryRecordCommodity> findByInventoryRecordIdIsNull();

}
