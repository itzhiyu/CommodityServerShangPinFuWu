package com.wetool.jpa.invoicing;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.wetool.entity.invoicing.Supplier;

/**
 * 描述：供应商JPA仓库
 * @author lixin
 */
@Repository
public interface SupplierRepo extends JpaRepository<Supplier, Long>, JpaSpecificationExecutor<Supplier> {
	
	@Query("select t.supplierName from Supplier t where t.id = :id ")
	public String getByIdToName(@Param("id") Long id);

	public List<Supplier> findByShopIdAndIsDeleted(Long shopId, boolean isDeleted);
}
