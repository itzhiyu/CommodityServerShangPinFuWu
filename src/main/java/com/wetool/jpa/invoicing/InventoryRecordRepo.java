package com.wetool.jpa.invoicing;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.wetool.entity.invoicing.InventoryRecord;
import com.wetool.entity.invoicing.InvoicingRecord;
import com.wetool.model.enums.OutOfStorage;
import com.wetool.model.enums.Status;
/**
 * 描述：入库信息JPA仓库
 * @author lixin
 */
@Repository
public interface InventoryRecordRepo extends JpaRepository<InventoryRecord, Long>, JpaSpecificationExecutor<InventoryRecord> {

	Long countBySupplierIdAndShopIdAndIsDeletedAndOutOfStorageAndTypeNotAndTypeNot( Long supplierId, Long shopId,boolean isDeleted,OutOfStorage s,Long type,Long types);

	@Query("select new com.wetool.entity.invoicing.InvoicingRecord(s.createDate,s.type,sc.commodityNumber,s.outOfStorage,s.totalPrice) from InventoryRecord s inner join s.commoditys sc "
			+ "where sc.commodityId = :id and s.shopId = :shopId and s.outOfStorage = :outOfStorage and s.isDeleted = false and s.type = :type and s.status = :status" )
	public List<InvoicingRecord> findByCommId(@Param("id") Long commodityId,@Param("shopId") Long shopId,@Param("outOfStorage")OutOfStorage outOfStorage,@Param("type") Long type, @Param("status")Status status);
	
	@Query("select new com.wetool.entity.invoicing.InvoicingRecord(s.createDate,s.type,sc.commodityNumber,s.outOfStorage,sc.totalPrice) from InventoryRecord s inner join s.commoditys sc "
			+ "where sc.commodityId = :id and s.shopId = :shopId and s.outOfStorage = :outOfStorage and s.isDeleted = false and (s.type = :type1 or s.type = :type2) and s.status = :status" )
	public List<InvoicingRecord> findByCommId(@Param("id") Long commodityId,@Param("shopId") Long shopId,@Param("outOfStorage")OutOfStorage outOfStorage,@Param("type1") Long type, @Param("type2") Long type2, @Param("status")Status status);

	
}