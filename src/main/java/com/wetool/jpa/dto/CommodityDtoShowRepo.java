package com.wetool.jpa.dto;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wetool.model.dto.CommodityDtoShow;

public interface CommodityDtoShowRepo extends JpaRepository<CommodityDtoShow, Long>{

}
