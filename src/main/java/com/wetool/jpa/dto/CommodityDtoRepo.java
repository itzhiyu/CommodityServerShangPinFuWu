package com.wetool.jpa.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.wetool.model.dto.CommodityDto;

public interface CommodityDtoRepo extends JpaRepository<CommodityDto, Long>,JpaSpecificationExecutor<CommodityDto>{

}
