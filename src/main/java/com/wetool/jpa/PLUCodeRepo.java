package com.wetool.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wetool.entity.PLUCode;

public interface PLUCodeRepo extends JpaRepository<PLUCode, Long>{

	PLUCode getByShopId(Long shopId);

}
