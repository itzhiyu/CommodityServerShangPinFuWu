package com.wetool.jpa;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.wetool.entity.PublicCommodity;
/**
 * 商品仓库（公共库）
 * @author lixin
 *
 */
@Repository
public interface PublicCommodityRepo
		extends JpaRepository<PublicCommodity, Long>, JpaSpecificationExecutor<PublicCommodity> {

	/**
	 * 统计商品目录下的商品数量
	 */
	@Query(value = "select count(*) from PublicCommodity c where  c.categoryId = :categoryId")
	public int countByCategoryId(@Param("categoryId") Long categoryId);

	public List<PublicCommodity> findByIdBetween(int start, int limit);

	public List<PublicCommodity> findByIdBetween(long l, long limit);

	@Query("select id from PublicCommodity where barcode = :barCode")
	public List<Long> getId(@Param("barCode") String barCode);

	@Query("SELECT max(id) FROM PublicCommodity")
	public long getMaxId();

	public List<PublicCommodity> findByBarcode(String key);
}