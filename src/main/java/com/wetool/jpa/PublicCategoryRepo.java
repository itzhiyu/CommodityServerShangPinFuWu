package com.wetool.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wetool.entity.PublicCategory;

/**
 * 商品分类仓库（公共库）
 * @author lixin
 *
 */
@Repository
public interface PublicCategoryRepo extends JpaRepository<PublicCategory, Long> {



}