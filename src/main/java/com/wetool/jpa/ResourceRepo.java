package com.wetool.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wetool.entity.Resource;

/**
 * 商品图片资源仓库
 * @author lixin
 *
 */
@Repository
public interface ResourceRepo extends JpaRepository<Resource, Long> {

}
