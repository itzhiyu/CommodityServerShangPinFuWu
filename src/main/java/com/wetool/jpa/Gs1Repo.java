package com.wetool.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wetool.entity.Gs1;

import feign.Param;

public interface Gs1Repo extends JpaRepository<Gs1, Integer> {

    Gs1 getByCommodityNameAndBrandName(String commodityName,
	    String companyName);

    Gs1 getByCommodityName(String commodityName);

    Gs1 getByCommodityNameLike(@Param("commodityName") String commodityName);

    Gs1 getByBarCode(String barcode);

    Gs1 getBarCodeById(@Param(value = "id") Long id);
}
