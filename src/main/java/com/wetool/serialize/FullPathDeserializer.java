package com.wetool.serialize;

import java.io.IOException;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.wetool.Constant;

@Component
@RefreshScope
public class FullPathDeserializer extends JsonDeserializer<String> {

	/**
     * set方法处理 调用upload服务，获取阿里OSS对象外网访问地址 resUrl中如果包含该地址，则用空字符串替换掉（只保留后面的文件相对路径）
     */

    @Override
    public String deserialize(JsonParser jp, DeserializationContext ctxt)
	    throws IOException, JsonProcessingException {
	// TODO Auto-generated method stub
	return jp.getText().replace(Constant.BASH_PATH, "");
    }
}