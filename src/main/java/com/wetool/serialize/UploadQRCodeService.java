package com.wetool.serialize;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Calendar;

/**
 * Created by zhanbin on 7/1/17.
 * 上传二维码服务
 */
@Service
public class UploadQRCodeService {

    /**
     * 验证二维码有效性
     *
     * @param timestamp 创建二维码的时间戳
     * @return 时间戳过期则返回ture，否则返回false
     */
    public boolean checkQRCodeValidity(String timestamp) {
        // 密文不能为空
        if (StringUtils.isEmpty(timestamp)) {
            // throw new ServiceException(ErrorType.errorValidationCode);
            return false;
        }

        // 校验密文时间是否操作15分钟， 超过则提示不能上传
        Calendar codeDate = Calendar.getInstance();
        codeDate.setTimeInMillis(Long.parseLong(timestamp));
        codeDate.add(Calendar.MINUTE, 15);
        return System.currentTimeMillis() > codeDate.getTimeInMillis();
    }

    /**
     * 检查Base64图片文件的格式有效性
     *
     * @param file
     * @return
     */
    public boolean checkFileValidity(String file) {
        return StringUtils.isEmpty(file) || !file.contains(":") || !file.contains("image") || !file.contains(";") || !file.contains("/") || !file.contains(",");
    }
}
