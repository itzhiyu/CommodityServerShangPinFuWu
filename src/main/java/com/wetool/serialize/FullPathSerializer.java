package com.wetool.serialize;

import java.io.IOException;

import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.wetool.Constant;

@Component
@RefreshScope
public class FullPathSerializer extends JsonSerializer<String> {

    /**
     * get方法处理 调用upload服务，获取阿里OSS对象外网访问地址，拼接在resUrl前面
     */
    @Override
    public void serialize(String url, JsonGenerator gen,
	    SerializerProvider provider)
	    throws IOException, JsonProcessingException {

	gen.writeString(Constant.BASH_PATH.concat(url));
    }
}