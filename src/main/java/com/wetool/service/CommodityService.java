package com.wetool.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

import com.wetool.entity.Commodity;
import com.wetool.entity.SplitCommodityRule;
import com.wetool.exception.BarcodeIndexOfException;
import com.wetool.exception.BarcodeRepeatException;
import com.wetool.exception.CategoryIsLeafException;
import com.wetool.exception.CategoryNotFindException;
import com.wetool.exception.CommodityAddException;
import com.wetool.exception.CommodityNotFindException;
import com.wetool.exception.CommodityUpdateException;
import com.wetool.exception.MerchantNotLoginException;
import com.wetool.jpa.CommodityEventHandler;
import com.wetool.jpa.CommodityRepo;
import com.wetool.jpa.CommoditySpec;
import com.wetool.model.BePerfectedCommodity;
import com.wetool.model.CommodityAdd;
import com.wetool.model.CommodityModify;
import com.wetool.model.CommodityQuery;
import com.wetool.model.CommodityReceive;
import com.wetool.model.SearchParam;
import com.wetool.model.SpiltCommodityModel;
import com.wetool.push.api.model.model.CommodityPushMessage;
import com.wetool.utils.Transformation;

/**
 * 商家商品业务处理
 * @author lixin
 */
@Component
public class CommodityService {
	
	private static final Logger logger = LogManager.getLogger(CommodityService.class);
	
	@Autowired
	private CommodityRepo commodityRepo;

	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private PushService pushService;
	
	@Autowired
	private CommodityEventHandler commodityEventHandler;

	@Autowired
	private SplitCommodityRuleService splitCommodityRuleService;
	
	@Value("${transformation.etl-url}")
	private String STATISTIC_URL;
	
	@Value("${transformation.db-host}")
	private String DB_HOST;  							//数据库host
	
	@Value("${transformation.report}")
	private String REPORT_USERNAME;  					//数据库用户名
	@Value("${transformation.password}")
	private String REPORT_PASSWORD;  					//数据库密码
	
	
	/**
	 * 商品查询
	 * @param params 查询参数
	 * @param p	分页参数
	 * @return 商品分页查询结果
	 * @throws MerchantNotLoginException 
	 * @throws ParseException 
	 * @throws CategoryNotFindException 
	 */
	public Page<Commodity> find(Long merchantId, SearchParam params, Pageable p) 
			throws MerchantNotLoginException, ParseException, CategoryNotFindException {
		if(merchantId == null ) {	// 判断商家ID是否为null
			logger.debug("商家商品业务处理类中（商品列表查询） -> 商家ID为空");
			throw new MerchantNotLoginException("商家商品业务处理类中（商品列表查询） -> 商家ID为空");	// 抛出商家未登录异常
		}
		params.setIsDeleted(false);
		Long[] categoryIds = categoryService.getIds(merchantId, params.getCategoryId());//获取商品分类和商品分类下打子节点
		CommoditySpec spec = new CommoditySpec(merchantId, params, categoryIds);
		Page<Commodity> page = commodityRepo.findAll(spec, p);
		for (Commodity commodity : page.getContent()) {
			if (!commodity.getResources().isEmpty()) {
				commodity.setResource(commodity.getResources().get(0));
			}
			commodity.setResources(null);
		}
		return page;
	}

	/**商品信息查询
	 * @param id 商品Id
	 * @return CommodityModify 查询商品信息和商品拆分信息
	 * @throws CommodityNotFindException
	 */
	public CommodityModify findOne(Long id) throws CommodityNotFindException {
		Commodity commodity = commodityRepo.findOne(id);
		if (commodity == null) {
			logger.debug("商家商品业务处理类中（商品信息查询） -> 商品信息不存在");
			throw new CommodityNotFindException("商家商品业务处理类中（商品信息查询） -> 商品信息不存在");//商品未找到异常
		}
		/* 商品信息逻辑删除，查询抛出异常 */
		if (commodity.getIsDeleted() != null && commodity.getIsDeleted() == true) {
			logger.debug("商家商品业务处理类中（商品信息查询） -> 商品信息被逻辑删除");
			throw new CommodityNotFindException("商家商品业务处理类中（商品信息查询） -> 商品信息被逻辑删除");//商品未找到异常
		}
		// 展示拆分商品数据 ，拆分数量 ，分类信息
		CommodityModify commodityModify = new CommodityModify();
		BeanUtils.copyProperties(commodity, commodityModify);
		commodityModify.setDetachable(commodity.getIsDetachable());
		commodityModify.setInventoryQuantity(commodity.getInventoryNumber());
		if (commodity.getResources() != null && commodity.getResources().size() > 0) {
			commodityModify.setResource(commodity.getResources().get(0));
		}
		if (commodity.getIsDetachable() == true) {// 拆分后商品详情
			commodityModify.setSplitRule(splitCommodityRuleService.getSpiltCommodityModel(commodity));
		}
		return commodityModify;
	}

	/**
	 * 提供美团服务查询商品信息
	 * @param id
	 * @return
	 */
	public Commodity getByCommodityId(Long id) {
		return commodityRepo.findOne(id);
	}
	
	/**
	 * 商品条码和商家ID查询商品信息
	 * @param key
	 * @param merchantId
	 * @return
	 * @throws MerchantNotLoginException 
	 */
	public List<CommodityQuery> findByBorcode(String key, Long merchantId) throws MerchantNotLoginException {
		if (merchantId == null) {
			logger.debug("商家商品业务处理类中(商品条码和商家ID查询商品信息) ->  商家ID为空");
			throw new MerchantNotLoginException("商家商品业务处理类中(商品条码和商家ID查询商品信息) ->  商家ID为空");
		}
		List<Commodity> co = commodityRepo.findByBarcodeAndMerchantIdAndIsDeleted(key,merchantId,false);
		List<CommodityQuery> commodityQuerys = new ArrayList<>();
		for (Commodity commodity : co) {
			CommodityQuery commodityQuery = new CommodityQuery();
			BeanUtils.copyProperties(commodity, commodityQuery);
			if (!commodity.getResources().isEmpty()) {
				commodityQuery.setResource(commodity.getResources().get(0));
			}
			commodityQuery.setType(commodity.getType().toString());
			commodityQuerys.add(commodityQuery);
		}
		return commodityQuerys;
	}

	
	/**
	 * 新增商品信息
	 * @param CommodityAdd 商品添加信息
	 * @return Commodity 商品信息
	 * @throws Exception 
	 */
	@Transactional
	public Commodity save(CommodityAdd c) throws Exception  {
		if (c.getMerchantId() == null) {
			logger.debug("商家商品业务处理类中(新增商品信息) ->  商家ID为空");
			throw new MerchantNotLoginException("商家商品业务处理类中(新增商品信息) ->  商家ID为空");
		}
		/* 商品条码重复判断 */
		Long count = commodityRepo.countByMerchantIdAndBarcodeAndIsDeleted(c.getMerchantId(), c.getBarcode(),false);
		if (count > 0) {
			logger.debug("商家商品业务处理类中(新增商品信息) ->  商品已存在商品档案或待完善商品中 . 参数商品添加条码【{}】",c.getBarcode());
			throw new BarcodeRepeatException("商家商品业务处理类中(新增商品信息) ->  商品已存在商品档案或待完善商品中");
		}
		if (c.getCategoryId() != null) {
			boolean flag = categoryService.getIsLeaf(c.getCategoryId());
			if (!flag) {
				logger.debug("商家商品业务处理类中(新增商品信息) ->  商品分类目录下能添加商品 . 参数商品分类id【{}】,是否为子节点【{}】",c.getCategoryId(),flag);
				throw new CategoryIsLeafException("商品分类目录下能添加商品 . 参数商品分类id【"+c.getCategoryId()+"】,是否为子节点【"+flag+"】");
			}
		}	
		if (c.getType() == 2 && c.getBarcode().length() > 4) {
			logger.debug("商家商品业务处理类中(新增商品信息) ->  称重商品条码超过４位 . 参数商品添加条码【{}】",c.getBarcode());
			throw new BarcodeIndexOfException("商家商品业务处理类中(新增商品信息) ->  称重商品条码超过４位");
		}
		Commodity commodity = new Commodity();	
		/* 对类进行copy */
		BeanUtils.copyProperties(c, commodity);
		commodity = commodityEventHandler.beforeCreate(c,commodity);//新增前处理参数处理
		commodity.setUpdateDate(new Timestamp(System.currentTimeMillis()));
		logger.debug("商家商品业务处理类中(新增商品信息) . 参数 商品数据处理结果 【{}】" ,commodity);
		commodity = commodityRepo.saveAndFlush(commodity);
		CommodityReceive cr = new CommodityReceive(c.getType());
		BeanUtils.copyProperties(commodity, cr);
		boolean flag = pushService.sendCommodityMassage(new CommodityPushMessage(c.getMerchantId(), cr.getCommodity()));
		logger.info("推送商品信息 . 参数 推送是否成功【{}】",flag);
		if (commodity == null || flag == false) {
			logger.debug("商家商品业务处理类中(新增商品信息) ->  商品信息添加失败");
			throw new CommodityAddException("商家商品业务处理类中(新增商品信息) ->  商品信息添加失败");
		}
		return commodity;
	}

	/**
	 * 修改商品信息
	 * @param CommodityModify 商品修改信息
	 * @return Commodity 商品信息
	 * @throws ClassNotFoundException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws CommodityNotFindException
	 * @throws CommodityUpdateException 
	 */
	public Commodity update(CommodityModify commodityModify) throws Exception{
		if (commodityModify.getMerchantId() == null) {
			logger.debug("商家商品业务处理类中(修改商品信息) ->  商家ID");
			throw new MerchantNotLoginException("商家商品业务处理类中(修改商品信息) ->  商家ID");
		}
		Commodity commodity = commodityRepo.findOne(commodityModify.getId());
		/* 查询是否有重复条码并且剔除本身 */
		Long count = commodityRepo.countByMerchantIdAndBarcodeAndIsDeletedAndIdNot
				(commodityModify.getMerchantId(), commodityModify.getBarcode(),false,commodity.getId());
		if (count > 0) {
			logger.debug("商家商品业务处理类中(修改商品信息) ->  商品已存在商品档案或待完善商品中 . 参数商品添加条码【{}】",commodityModify.getBarcode());
			throw new BarcodeRepeatException("商家商品业务处理类中(修改商品信息) ->  商品已存在商品档案或待完善商品中");
		}
		boolean flag = categoryService.getIsLeaf(commodityModify.getCategoryId());
		if (!flag) {
			logger.debug("商家商品业务处理类中(修改商品信息) ->  商品分类目录下能添加商品 . 参数商品分类id【{}】,是否为子节点【{}】",commodityModify.getCategoryId(),flag);
			throw new CategoryIsLeafException("商品分类目录下能添加商品 . 参数商品分类id【"+commodityModify.getCategoryId()+"】,是否为子节点【"+flag+"】");
		}
		if (commodityModify.getType() == 2 && commodityModify.getBarcode().length() > 4) {
			logger.debug("商家商品业务处理类中(新增商品信息) ->  称重商品条码超过４位 . 参数商品添加条码【{}】",commodityModify.getBarcode());
			throw new BarcodeIndexOfException("商家商品业务处理类中(新增商品信息) ->  称重商品条码超过４位");
		}
		/** 对类属性进行copy */
		BeanUtils.copyProperties(commodityModify, commodity);
		commodity.setUpdateDate(new Timestamp(System.currentTimeMillis()));
		commodity = commodityEventHandler.beforeSave(commodityModify , commodity);//修改前处理
		logger.debug("商家商品业务处理类中(修改商品信息) . 参数 商品数据处理结果 【{}】" ,commodity);
		commodity = commodityRepo.saveAndFlush(commodity); //查询信息后修改
		String i = commodityEventHandler.handleCommodity(commodity);//对库存商品信息数据修改
		CommodityReceive cr = new CommodityReceive(commodityModify.getType());
		BeanUtils.copyProperties(commodity, cr);
		boolean flags = pushService.sendCommodityMassage(new CommodityPushMessage(commodityModify.getMerchantId(), cr.getCommodity()));
		logger.info("推送商品信息 . 参数 推送是否成功【{}】",flags);
		if (commodity == null || !"0".equals(i) || flags == false) {
			logger.debug("商家商品业务处理类中(修改商品信息) ->  商家商品信息修改失败");
			throw new CommodityUpdateException("商家商品业务处理类中(修改商品信息) ->  商家商品信息修改失败");
		}
		return commodity;
	}

	/**
	 * 批量修改商品类型
	 * @param List<Long> ids 商品id集合 
	 * @param categoryId 商品需修改的分类ID
	 * @throws CommodityNotFindException
	 */
	public void upadteBatch(List<Long> ids, Long categoryId) throws CommodityNotFindException {
		List<Commodity> commoditys = new ArrayList<>();
		Commodity commodity = null;
		for (Long id : ids) {
			commodity = commodityRepo.findOne(id);
			if (commodity == null) {
				logger.debug("商家商品业务处理类中(批量修改商品类型) ->  商家商品信息不存在");
				throw new CommodityNotFindException("商家商品业务处理类中(批量修改商品类型) ->  商家商品信息不存在");
			}
			commodity.setCategoryId(categoryId);
			commoditys.add(commodity);
		}
		commodityRepo.save(commoditys);
	}

	/**
	 * 商品删除（逻辑删除）
	 * @param id 商品ID
	 * @throws Exception 
	 */
	@Transactional
	public Commodity delete(Long id) throws Exception {
		Commodity co = commodityRepo.findOne(id);
		if (co == null || co.getIsDeleted() == true) { //为空或已被逻辑删除
			logger.debug("商家商品业务处理类中(商品删除（逻辑删除）) ->  商家商品信息不存在或逻辑删除");
			throw new CommodityNotFindException("商家商品业务处理类中(商品删除（逻辑删除）) ->  商家商品信息不存在或逻辑删除");
		}
		Long splitRuleId = null;
		/* 删除商品时，移除拆分规则 */
		if (co.getIsDetachable() != null && co.getIsDetachable() == true){
			splitRuleId = co.getSplitRule().getId();//获取拆分规则ID
			splitCommodityRuleService.delete(splitRuleId);//DB删除拆分规则数据
			co.setIsDetachable(false);//商品信息设置为拆分为否
			co.setSplitRule(null);//移除拆分数据
		}
		/* 查询商品在拆分规则中对应信息 */
		List<SplitCommodityRule> scr = splitCommodityRuleService.findBySplitCommodity(id);
		if (scr != null && scr.size() > 0) {
			/* 删除被拆分商品时，将关联拆分后商品信息规则移除 */
			Long merchantId = co.getMerchantId();
			for (SplitCommodityRule splitCommodityRule : scr) {
				/* 获取被拆分规则 */
				List<Commodity> cos = commodityRepo.getByMerchantIdAndSplitRule(merchantId,splitCommodityRule);
				cos.stream().forEach(num -> num.setSplitRule(null));
				cos.stream().forEach(num -> num.setIsDetachable(false));
				commodityRepo.save(cos);
				splitCommodityRuleService.delete(splitCommodityRule.getId());
			}
		}
		co.setIsDeleted(true);//逻辑删除
		co.setUpdateDate(new Timestamp(System.currentTimeMillis()));
		co = commodityRepo.save(co);
		String i = commodityEventHandler.handleCommodity(co);//对库存商品信息数据修改
		CommodityReceive cr = new CommodityReceive(co.getType());
		BeanUtils.copyProperties(co, cr);
		boolean flag = pushService.sendCommodityMassage(new CommodityPushMessage(co.getMerchantId(), cr.getCommodity()));
		logger.info("推送商品信息 . 参数 推送是否成功【{}】",flag);
		if (co == null || !"0".equals(i) || flag == false) {
			logger.debug("商家商品业务处理类中(商品删除（逻辑删除）) ->  库存信息修改失败");
			throw new Exception("商家商品业务处理类中(商品删除（逻辑删除）) ->  库存信息修改失败");
		}
		return co;
	}

	/**
	 * 查找商品（商家库）
	 * @param merchantId 商家登录ID
	 * @param id 商品ID
	 * @return
	 */
	public Commodity findByMerchantIdAndId(Long merchantId, Long id) {
		return commodityRepo.findByMerchantIdAndId(merchantId, id);
	}

	/**
	 * 商品分类查找所属分下的商品总数
	 * @param categoryId
	 * @return
	 */
	public Long countByCategoryId(Long categoryId) {
		return commodityRepo.countByCategoryIdAndIsDeletedAndMerchantIdIsNotNullAndTypeNot(categoryId,false,3L);
	}

	/**
	 * 通过商品ID获取对应商品信息
	 * @param id 商品ID
	 * @return
	 */
	public CommodityModify getById(Long id) {
		Commodity co = commodityRepo.getByIdAndIsDeleted(id,false);
		CommodityModify com = null;
		if (co != null) {
			com = new CommodityModify();
			co.setResources(null);
			BeanUtils.copyProperties(co, com);//属性复制
			com.setInventoryQuantity(co.getInventoryNumber());
			com.setDetachable(co.getIsDetachable());
			SpiltCommodityModel scm = new SpiltCommodityModel();
			if (co.getSplitRule() != null) {
				BeanUtils.copyProperties(co.getSplitRule(), scm);
				com.setSplitRule(scm);
			}
		}
		return com;
	}

	/**
	 * 提供对其他服务对商家库商品信息修改
	 * @param commodity
	 * @return Commodity
	 */
	public Commodity saveOrUpdate(Commodity commodity) {
		return commodityRepo.save(commodity);
	}

	/**
	 * 店铺添加出入库对商家入库价格修改
	 * @param id
	 * @param inventoryNumber
	 * @return
	 * @throws CommodityNotFindException
	 */
	public Commodity updateInventoryNumber(Long id,BigDecimal buyingPrice) throws CommodityNotFindException {
		Commodity commodity = commodityRepo.findOne(id);
		if (commodity == null) {
			logger.debug("商家商品业务处理类中(店铺添加出入库对商家入库价格修改) ->  商品信息不存在");
			throw new CommodityNotFindException("商家商品业务处理类中(店铺添加出入库对商家入库价格修改) ->  商品信息不存在");
		}
		if (buyingPrice != null) {
			commodity.setBuyingPrice(buyingPrice);
		}
		return commodityRepo.save(commodity);
	}
	
	/**
	 * 拆分规则修改商品信息
	 * @param commodityModify
	 * @return
	 */
	public void getSplit(CommodityModify commodityModify,Commodity commodity){
		SplitCommodityRule splitCommodityRule = new SplitCommodityRule();
		if (commodityModify.isDetachable() == true && commodityModify.getSplitRule() != null) {//判断拆分模型是否为空
			/* 添加商品对象中拆分规则不为空则添加拆分属性值 */
			SpiltCommodityModel s = commodityModify.getSplitRule();
			if (commodity.getSplitRule() != null) {
				Long spId = commodity.getSplitRule().getId();
				s.setId(spId);
			}
			if (commodity.getSplitRule() != null) {
				splitCommodityRule = commodity.getSplitRule();
			}
			BeanUtils.copyProperties(s, splitCommodityRule);
			splitCommodityRule = splitCommodityRuleService.save(splitCommodityRule);
			commodity.setSplitRule(splitCommodityRule);
		}
		/* 商品选择不可拆分，原商品拆分信息删除 */
		if (commodityModify.isDetachable() == false && commodity.getSplitRule() != null) {
			splitCommodityRuleService.delete(commodity.getSplitRule());
			commodity.setSplitRule(null);
		}
	}

	/**
	 * 同步商品查询
	 * @param params 查询参数
	 * @param p	分页参数
	 * @return 商品分页查询结果
	 * @throws MerchantNotLoginException 
	 * @throws ParseException 
	 */
	public List<CommodityReceive> findByUpTime(Long merchantId, SearchParam params, Pageable p) throws MerchantNotLoginException, ParseException {
		if(merchantId == null ) {	// 判断商家ID是否为null
			logger.debug("商家商品业务处理类中（长链接商品同步列表） -> 商家ID为空");
			throw new MerchantNotLoginException("商家商品业务处理类中（长链接商品同步列表） -> 商家ID为空");	// 抛出商家未登录异常
		}
		Page<Commodity> page = null;
		if (params.getUpdateTime() != null) {
			String time = params.getUpdateTime();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date date = sdf.parse(time);
			page = commodityRepo.findByMerchantIdAndUpdateDateBetween(params.getMerchantId(),date,new Date(), p);//分页结果
		} else {
			page = commodityRepo.findByMerchantId(merchantId,p);
		}
		List<CommodityReceive> rs = new ArrayList<>();//对数据重新封装
		for (Commodity commodity : page.getContent()) {
			CommodityReceive cr = new CommodityReceive();
			BeanUtils.copyProperties(commodity, cr);//属性复制
			cr.setType(commodity.getType().toString());
			cr.setTotalPage(page.getTotalPages());//设置总页数
			rs.add(cr);
		}
		logger.info("商家商品业务处理类中（长链接商品同步列表） ->  . 结果 商品同步列表【{}】", rs);
		return rs;
	}

	/**
	 * 商家ID和商品类型查询数量
	 * @param merchantId
	 * @param type
	 * @return
	 */
	public Long countByMerchantIdAndType(Long merchantId, Long type,boolean flag) {
		return commodityRepo.countByMerchantIdAndTypeAndIsDeleted(merchantId,type,flag);
	}

	/**
	 * 快录商品查询条码是否重复
	 * @param merchantId
	 * @param barcode
	 * @param type
	 * @return
	 */
	public Long countByMerchantIdAndBarcodeAndType(Long merchantId, String barcode, Long type) {
		return commodityRepo.countByMerchantIdAndBarcodeAndTypeAndIsDeleted(merchantId,barcode,type,false);
	}

	/**
	 * 快录商品列表
	 * @param merchantId
	 * @param type
	 * @param p
	 * @return
	 */
	public Page<BePerfectedCommodity> findByMerchantIdAndType(Long merchantId, Long type, Pageable p) {
		return commodityRepo.findByMerchantIdAndType(merchantId,type,p);
	}
	
	/**
	 * 条码搜索快录商品列表
	 * @param merchantId
	 * @param type
	 * @param p
	 * @return
	 */
	public Page<BePerfectedCommodity> findByMerchantIdAndTypeAndKey(Long merchantId, Long type, Pageable p, String key) {
		return commodityRepo.findByMerchantIdAndType(merchantId,type,key,p);
	}
	
	/**
	 * 拆分规则对应商品信息
	 * @param id
	 * @return
	 */
	public List<CommodityModify> findByCommodity(Long id){
		List<Commodity> scr = commodityRepo.findByCommodity(id);
		List<CommodityModify> cms = new ArrayList<>();
		for (Commodity co : scr) {
			CommodityModify com = null;
			if (co != null) {
				com = new CommodityModify();
				co.setResources(null);
				BeanUtils.copyProperties(co, com);//属性复制
				com.setInventoryQuantity(co.getInventoryNumber());
				com.setDetachable(co.getIsDetachable());
				SpiltCommodityModel scm = new SpiltCommodityModel();
				if (co.getSplitRule() != null) {
					BeanUtils.copyProperties(co.getSplitRule(), scm);
					com.setSplitRule(scm);
				}
				cms.add(com);
			}
		}
		return cms;
	}

	/**
	 * 查询称重商品条码
	 * @param arrayString
	 * @param merchantId
	 * @param b
	 * @return
	 */
	public Long countByBarcodeAndMerchantIdAndIsDeleted(String arrayString, Long merchantId, boolean b) {
		return commodityRepo.countByBarcodeAndMerchantIdAndIsDeleted(arrayString,merchantId,b);
	}
	
	
	
	/**
	 * 按文件导入商品 leo
	 */
	public void importCommodityByFile(Long merchantId, Long shopId, MultipartFile excelFile) throws IOException {
		File f = File.createTempFile("temp", ".xlsx");  //会自动生成随机数
		String filePath = f.getAbsolutePath();
		try(InputStream is = excelFile.getInputStream();
			OutputStream os = new FileOutputStream(f)) {
			byte[] buf = new byte[8192];
			int length = 0;
			while((length = is.read(buf))!=-1){
				os.write(buf, 0, length);
			}
			os.flush();
			f.deleteOnExit();
		} catch (Exception e) {
			throw new MultipartException("file upload failed.", e);
		}
		String[] params = {DB_HOST, REPORT_USERNAME, REPORT_PASSWORD, merchantId+"", shopId+"", filePath}; // 传递参数
		Transformation.runJob(params, STATISTIC_URL);
		logger.info("商品导入完成!");
	}
	
	
}
