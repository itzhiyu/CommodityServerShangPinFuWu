package com.wetool.service;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import com.wetool.entity.PublicCategory;
import com.wetool.entity.PublicCommodity;
import com.wetool.entity.Resource;
import com.wetool.exception.CommodityNotFindException;
import com.wetool.jpa.PublicCommodityRepo;
import com.wetool.jpa.PublicCommoditySpec;
import com.wetool.model.CommodityQuery;
import com.wetool.model.PublicCommodityAdd;
import com.wetool.model.SearchParam;
import com.wetool.utils.GS1BarcodeUtil;

@Component
public class PublicCommodityService {
	private static final Logger logger = LogManager.getLogger(PublicCommodityService.class); 

	@Autowired
	private PublicCommodityRepo publicCommodityRepo;

	@Autowired
	private PublicCategoryService publicCategoryService;
	

	/**
	 * 商品查询
	 * 
	 * @param 多参+分页参数
	 * @return 商品对象集合
	 * @throws CommodityNotFindException
	 */
	public Page<PublicCommodity> find(SearchParam params, Pageable p) throws CommodityNotFindException {
		Long[] categoryIds = null;
		if (params.getCategoryId() != null) {
			categoryIds = publicCategoryService.getIds(params.getCategoryId());
		}
		Page<PublicCommodity> page = null ;
		PublicCommoditySpec spec = new PublicCommoditySpec(params, categoryIds);
		page = publicCommodityRepo.findAll(spec, p);
		return page;
	}

	/**
	 * 商品查询
	 * 
	 * @param key
	 *            商品KEY（主键）
	 * @return 角色对象
	 */
	public PublicCommodity findOne(Long id) {
		return publicCommodityRepo.findOne(id);
	}

	public PublicCommodity save(PublicCommodityAdd cpc) throws Exception {
		PublicCommodity commodity = new PublicCommodity();
		if (cpc == null) {
			throw new CommodityNotFindException();
		}
		BeanUtils.copyProperties(cpc, commodity);
		PublicCategory pc = publicCategoryService.findOne(commodity.getCategoryId());
		// 加入gs1地区或范围编号
		String counrt = GS1BarcodeUtil.getCountryCode(commodity.getBarcode());
		commodity.setCountry(counrt);
		// 判断是否是叶子节点
		if (pc.isLeaf()) {
			return publicCommodityRepo.saveAndFlush(commodity);
		}
		return null;
	}

	public void delete(Long id) {
		publicCommodityRepo.delete(id);
	}

	public Page<PublicCommodity> find(Pageable p) {
		// TODO Auto-generated method stub
		return publicCommodityRepo.findAll(p);
	}

	public List<PublicCommodity> findByIdBetween(Long start, Long end) {
		// TODO Auto-generated method stub
		return publicCommodityRepo.findByIdBetween(start, end);
	}

	public void save(PublicCommodity co) {
		// TODO Auto-generated method stub
		publicCommodityRepo.save(co);
	}

	/***
	 * post端根据条码查询商品信息
	 * @param key
	 * @return
	 */
	public List<CommodityQuery> findByBarcode(String key) {
		logger.debug("post端根据条码查询商品信息  . 参数 条码【{}】",key);
		List<PublicCommodity> PublicCommoditys = publicCommodityRepo.findByBarcode(key);
		List<CommodityQuery> commodityQuerys = new ArrayList<>();
		for (PublicCommodity publicCommodity : PublicCommoditys) {
			CommodityQuery commodityQuery = new CommodityQuery();
			BeanUtils.copyProperties(publicCommodity, commodityQuery);
			if (!publicCommodity.getResources().isEmpty()) {
				Resource rs = new Resource();
				BeanUtils.copyProperties(publicCommodity.getResources().get(0), rs);
				commodityQuery.setResource(rs);
			}
			commodityQuerys.add(commodityQuery);
		}
		return commodityQuerys;
	}

}
