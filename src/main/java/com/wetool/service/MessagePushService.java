package com.wetool.service;

import com.wetool.push.api.model.server.PushMessage;
import com.wetool.stream.Channels;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Service;

/**
 * Created by zhanbin on 7/1/17.
 * commodity-server -> push-server
 * 消息推送服务
 */
@Service
@EnableBinding(Channels.class)
public class MessagePushService {

    @Autowired
    private Channels channels;

    public void sendImageSyncMessage(PushMessage<?> message) {
        this.channels.outputChannel().send(MessageBuilder.withPayload(message).build());
    }
}
