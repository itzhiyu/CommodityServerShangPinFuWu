package com.wetool.service.commodity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wetool.common.commodity.CommodityResult;
import com.wetool.common.model.Message;
import com.wetool.entity.Commodity;
import com.wetool.entity.invoicing.InventoryRecord;
import com.wetool.jpa.CommodityRepo;
import com.wetool.model.enums.Status;
import com.wetool.model.invoicing.InventoryRecordAdd;
import com.wetool.model.invoicing.InventoryRecordModify;
import com.wetool.service.invoicing.InventoryService;

@Service
public class RuleCheckService {
	
	private static final Logger logger = LogManager.getLogger(RuleCheckService.class);
	
	@Autowired
	private CommodityRepo commodityRepo;
	
	@Autowired
	private InventoryService inventoryService;
	
	@Autowired
	private CategoryService categoryService;
	
	/**
	 * 参数校验
	 * @param c
	 * @return
	 */
	public Message<?> commodityCheck(Commodity c){
		if (c.getMerchantId() == null) {
			logger.debug("商家商品业务处理类中(新增商品信息) ->  商家ID为空");
			return new Message<>(CommodityResult.MERCHANTNOTLOGIN);
		}
		if ((c.getBarcode() == null || "".equals(c.getBarcode())) && (c.getSelfCoding() == null || "".equals(c.getSelfCoding()))) {
			return new Message<>(CommodityResult.BARCODENOTFIND);
		}
		/* 商品条码重复判断 */
		Long count = 0L; 
		if (c.getId() == null) {
			count = commodityRepo.countByMerchantIdAndBarcodeAndIsDeletedAndBarcodeIsNotNull(c.getMerchantId(), c.getBarcode(),false);
		}else {
			count = commodityRepo.countByMerchantIdAndBarcodeAndIsDeletedAndIdNotAndBarcodeIsNotNull(c.getMerchantId(), c.getBarcode(),false,c.getId());
		}
		if (count > 0) {
			logger.debug("商家商品业务处理类中(新增商品信息) ->  商品已存在商品档案或待完善商品中 . 参数商品添加条码【{}】",c.getBarcode());
			return new Message<>(CommodityResult.BARCODEREPEAT);
		}
		if (c.getCategoryId() != null) {//判断商品添加的分类是否为一级
			Boolean flag = categoryService.getIsLeaf(c.getCategoryId());
			if (flag == null || !flag) {
				logger.debug("商家商品业务处理类中(新增商品信息) ->  商品分类目录下能添加商品 . 参数商品分类id【{}】,是否为子节点【{}】",c.getCategoryId(),flag);
				return new Message<>(CommodityResult.CATEGORYISLEAF);
			}
		}	
		if (c.getCategoryId() == null) {//商品分类为空
			return new Message<>(CommodityResult.CATEGORYNOTFIND);
		}
		if (c.getType() == 2 && c.getBarcode().length() > 4) {//称重商品条码设置位数
			logger.debug("商家商品业务处理类中(新增商品信息) ->  称重商品条码超过４位 . 参数商品添加条码【{}】",c.getBarcode());
			return new Message<>(CommodityResult.BARCODEINDEXOF);
		}
		return new Message<>(CommodityResult.SUCCESS);
	}
	
	/**
	 * 出入库单据添加校验
	 * @param inventoryRecord
	 * @return
	 */
	public Message<?> inventoryRecordSaveCheck(InventoryRecordAdd inventoryRecordAdd){
		if (inventoryRecordAdd.getShopId() == null || inventoryRecordAdd.getShopId() == 0) {
			logger.debug("出库信息业务处理(新增出库记录) -> 店铺ID为空");
			return new Message<>(CommodityResult.SHGOPNOTLOGIN);
		}
		return new Message<>(CommodityResult.SUCCESS);
	}
	
	/**
	 * 出入库单据添加校验
	 * @param inventoryRecord
	 * @return
	 */
	public Message<?> inventoryRecordUpdateCheck(Long id, InventoryRecordModify inventoryRecordModify){
		if (inventoryRecordModify.getShopId() == null || inventoryRecordModify.getShopId() == 0) {
			logger.debug("出库信息业务处理(新增出库记录) -> 店铺ID为空");
			return new Message<>(CommodityResult.SHGOPNOTLOGIN);
		}
		InventoryRecord inventoryRecord = inventoryService.getById(id);
		if (inventoryRecord.getStatus() == Status.ORDERCOMMIT) {
			return new Message<>(CommodityResult.BILLCOMMOIT);
		}
		return new Message<>(CommodityResult.SUCCESS);
	}

	/**
	 * 出入库删除校验
	 * @param inventoryRecord出入库单据
	 * @return
	 */
	public Message<?> inventoryRecordDeletedCheck(InventoryRecord inventoryRecord) {
		if (inventoryRecord == null || inventoryRecord.getIsDeleted() == true) {
			logger.debug("出入库校验 . 出入库单据不存在或已删除");
			return new Message<>(CommodityResult.INVENTORYRECORDNOTFIND);
		}
		if (inventoryRecord.getStatus() == Status.ORDERCOMMIT) {
			logger.debug("出入库校验 . 出入库单据已提交");
			return new Message<>(CommodityResult.BILLCOMMOIT);
		}
		return new Message<>(CommodityResult.SUCCESS);
	}
}
