package com.wetool.service.commodity;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.http.ResponseEntity;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Service;

import com.wetool.feign.MacFeignClient;
import com.wetool.push.api.model.PushMsgType;
import com.wetool.push.api.model.model.CategoryPushMessage;
import com.wetool.push.api.model.model.CommodityPushMessage;
import com.wetool.push.api.model.server.PushMessage;
import com.wetool.stream.Channels;


@Service
@EnableBinding(Channels.class)
public class PushService {
	
	@Autowired
	private Channels channels;
	
	@Autowired
	private MacFeignClient macFeignClient;
	
	public Boolean sendCommodityMassage(CommodityPushMessage commodityPushMessage) {
		boolean flag = false;
		ResponseEntity<List<String>> res = macFeignClient.getByMerchantId(commodityPushMessage.getMerchantId());
		List<String> sns = res.getBody();
		for (String string : sns) {
			PushMessage<?> pushMessage = new PushMessage<>(PushMsgType.COMMODITY_PUSH, string, commodityPushMessage.getCommodity());
			flag = channels.outputChannel().send(MessageBuilder.withPayload(pushMessage).build());
			if (flag == false) {
				return false;
			}
		}
		return flag;
	}
	
	public Boolean sendCategoryMassage(CategoryPushMessage categoryPushMessage) {
		boolean flag = false;
		ResponseEntity<List<String>> res = macFeignClient.getByMerchantId(categoryPushMessage.getMerchantId());
		List<String> sns = res.getBody();
		for (String string : sns) {
			PushMessage<?> pushMessage = new PushMessage<>(PushMsgType.CATEGORY_PUSH, string, true);
			flag = channels.outputChannel().send(MessageBuilder.withPayload(pushMessage).build());
			if (flag == false) {
				return false;
			}
		}
		return flag;
	}
}
