package com.wetool.service.commodity;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wetool.entity.Commodity;
import com.wetool.entity.SplitCommodityRule;
import com.wetool.jpa.CommodityRepo;
import com.wetool.jpa.SplitCommodityRuleRepo;
import com.wetool.model.SpiltCommodityModel;

@Service
public class SplitCommodityRuleService {
	
	@Autowired
	private SplitCommodityRuleRepo splitCommodityRuleRepo;
	
	@Autowired
	private CommodityRepo commodityRepo;
	
	/**
	 * 删除拆分规则
	 * @param splitRule
	 */
	public void delete(Long commodityId){
		splitCommodityRuleRepo.delete(commodityId);
	}
	
	/**
	 * 商品ID对应的拆分规则
	 * @param id
	 * @return
	 */
	public List<SplitCommodityRule> findBySplitCommodity(Long id){
		List<SplitCommodityRule> scr = splitCommodityRuleRepo.findByCommodityId(id);
		return scr;
	}
	
	/**
	 * 保存拆分
	 * @param splitCommodityRule
	 * @return
	 */
	public SplitCommodityRule save(SplitCommodityRule splitCommodityRule) {
		return splitCommodityRuleRepo.saveAndFlush(splitCommodityRule);
	}
	
	/**
	 * 添加商品拆分数据
	 * @param commodity 商品信息
	 * @return
	 */
	public SpiltCommodityModel getSpiltCommodityModel(Commodity commodity){
		SpiltCommodityModel sc = new SpiltCommodityModel();
		/* 属性值复制 */
		BeanUtils.copyProperties(commodity.getSplitRule(),sc);
		/* DB保存数据处理展示对象 */
		Commodity co = commodityRepo.findOne(commodity.getSplitRule().getCommodityId());
		sc.setBarcode(co.getBarcode());
		sc.setCommodityId(co.getId());
		sc.setName(co.getName());
		return sc;
	}

	/**
	 * 删除拆分规则
	 * @param splitRule
	 */
	public void delete(SplitCommodityRule splitRule) {
		splitCommodityRuleRepo.delete(splitRule);
	}

	/**
	 *　被拆分商品查询
	 * @param commodityId
	 * @return
	 */
	public List<Commodity> findBySplit(Long commodityId) {
		List<Commodity> scr = splitCommodityRuleRepo.findBySplit(commodityId);
		return scr;
	}
}
