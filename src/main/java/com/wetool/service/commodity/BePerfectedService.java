package com.wetool.service.commodity;

import java.text.ParseException;
import javax.transaction.Transactional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import com.wetool.entity.Commodity;
import com.wetool.exception.BarcodeRepeatException;
import com.wetool.exception.BePerfectedNotFindException;
import com.wetool.exception.CommodityAddException;
import com.wetool.exception.CommodityNotFindException;
import com.wetool.exception.MerchantNotLoginException;
import com.wetool.model.BePerfectedCommodity;
import com.wetool.model.CommodityAdd;
import com.wetool.model.CommodityModify;
import com.wetool.model.CommodityPerfectedAdd;
import com.wetool.model.SpiltCommodityModel;
import com.wetool.service.CommodityService;

/**
 * 快录商品业务处理
 * @author lixin
 */
@Component
public class BePerfectedService {
	private static final Logger logger = LogManager.getLogger(BePerfectedService.class);
	
	@Autowired
	private CommodityService commodityService;

	/**
	 * 快录商品展示列表
	 * @param p
	 * @param merchantId 商家ID
	 * @return
	 * @throws MerchantNotLoginException 
	 * @throws ParseException 
	 */
	public Page<BePerfectedCommodity> findAll(Pageable p,Long merchantId,String key) throws MerchantNotLoginException {
		if (merchantId == null) {
			logger.debug("快录商品业务处理类中 -> 商家ID为空");
			throw new MerchantNotLoginException("快录商品业务处理类中 -> 商家ID为空");
		}
		Page<BePerfectedCommodity> page = null;
		if (key != null && !"".equals(key)) {
			page = commodityService.findByMerchantIdAndTypeAndKey(merchantId,3L, p, key);
		}else {
			page = commodityService.findByMerchantIdAndType(merchantId,3L, p);
		}
		return page;
	}
	
	/**
	 * 根据ID查询单个快录商品信息
	 * @param id 快录商品信息查询
	 * @return
	 * @throws BePerfectedNotFindException 
	 * @throws CommodityNotFindException 
	 */
	public CommodityAdd getById(Long id) throws BePerfectedNotFindException, CommodityNotFindException{
		CommodityAdd commodityAdd = new CommodityAdd();
		CommodityModify bpc = commodityService.findOne(id);
		if (bpc == null) {//判断快录商品是否存在
			logger.debug("快录商品业务处理类中 -> 根据快录商品ID查询不到快录商品信息 . 参数【{}】",id);
			throw new BePerfectedNotFindException("快录商品业务处理类中 -> 根据快录商品ID查询不到快录商品信息");
		}
		/* 属性值复制entity模型中 */
		BeanUtils.copyProperties(bpc, commodityAdd);
		return commodityAdd;
	}
	
	/**
	 * 统计商家快录商品数
	 * @param merchantId 商家ID
	 * @return
	 * @throws MerchantNotLoginException 
	 */
	public Long CountByMerchantId(Long merchantId) throws MerchantNotLoginException {
		if (merchantId == null) {
			logger.debug("快录商品业务处理类中 -> 商家ID为空");
			throw new MerchantNotLoginException("快录商品业务处理类中 -> 商家ID为空");
		}
		Long count = commodityService.countByMerchantIdAndType(merchantId , 3L, false);
		return count;
	}
	
	/**
	 * 添加快录商品
	 * @param commodityPerfectedAdd
	 * @return
	 * @throws Exception 
	 */
	@Transactional
	public BePerfectedCommodity create(CommodityPerfectedAdd commodityPerfectedAdd) throws Exception {
		if (commodityPerfectedAdd.getMerchantId() == null) {
			logger.debug("快录商品业务处理类中 -> 商家ID为空");
			throw new MerchantNotLoginException("快录商品业务处理类中 -> 商家ID为空");
		}
		CommodityAdd commodity = new CommodityAdd();
		commodity.setCategoryId(null);
		/* 查询条形码是否重复 */
		Long count = commodityService.countByMerchantIdAndBarcodeAndType(commodityPerfectedAdd.getMerchantId()
					,commodityPerfectedAdd.getBarcode(),3L);
		if (count > 0) {//重复抛出异常
			logger.debug("快录商品业务处理类中 -> 快录商品条码重复. 参数【{}】",commodityPerfectedAdd.getBarcode());
			throw new BarcodeRepeatException("快录商品业务处理类中 -> 快录商品条码重复"); 
		}
		BeanUtils.copyProperties(commodityPerfectedAdd, commodity);
		/* 添加时创建时间为默认系统时间 */
		commodity.setType(3L);//快录商品为3L
		Commodity co =  commodityService.save(commodity); 
		BePerfectedCommodity bc = new BePerfectedCommodity(); //对象封装展示
		BeanUtils.copyProperties(co, bc);
		return bc;
	}

	/**
	 * 快录商品转成正式商品
	 * @param commodityAdd
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public Commodity saveCommodity(Long id ,CommodityAdd commodityAdd) throws Exception {
		if (commodityAdd.getMerchantId() == null) {
			logger.debug("快录商品业务处理类中 -> 商家ID为空");
			throw new MerchantNotLoginException("快录商品业务处理类中 -> 商家ID为空");
		}
		commodityAdd.setId(id);
		Long type = commodityAdd.getType();
		if (type == 3) {
			commodityAdd.setType(1L);
		}
		CommodityModify cm = new CommodityModify();
		BeanUtils.copyProperties(commodityAdd, cm);
		SpiltCommodityModel scm = new SpiltCommodityModel();
		if (commodityAdd.getSplitRule() != null) {
			BeanUtils.copyProperties(commodityAdd.getSplitRule(), scm);
			cm.setSplitRule(scm);
		}
		Commodity co = commodityService.update(cm);
		if (co == null) {
			logger.debug("快录商品业务处理类中 -> 快录商品修改为商家商品失败. 参数【{}】",co);
			throw new CommodityAddException("快录商品业务处理类中 -> 快录商品修改为商家商品失败");
		}
		return co;
	}

	/**
	 * 快录商品删除
	 * @param id
	 * @throws Exception 
	 */
	public void delet(Long id) throws Exception {
		commodityService.delete(id);
	}
}
