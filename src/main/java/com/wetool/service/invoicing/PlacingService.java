package com.wetool.service.invoicing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wetool.Constant;
import com.wetool.common.model.Message;
import com.wetool.entity.Commodity;
import com.wetool.entity.invoicing.InventoryRecord;
import com.wetool.entity.invoicing.InventoryRecordCommodity;
import com.wetool.entity.invoicing.InvoicingCommodity;
import com.wetool.exception.CommodityNotFindException;
import com.wetool.model.enums.OutOfStorage;
import com.wetool.model.enums.Status;
import com.wetool.model.invoicing.InventoryRecordAdd;
import com.wetool.model.invoicing.InventoryRecordModify;
import com.wetool.service.commodity.RuleCheckService;
import com.wetool.utils.Arith;
import com.wetool.utils.BillNoUtils;

/**
 * 出库业务处理
 * @author li
 */
@Service
public class PlacingService {
	private static final Logger logger = LogManager.getLogger(PlacingService.class);
	
	@Autowired
	private BillNoUtils billNoUtils;
	
	@Autowired
	private RuleCheckService ruleCheckService;
	
	@Autowired
	private InventoryService inventoryService;
	
	@Autowired
	private InvoicingCommodityNewService invoicingCommodityNewService;

	/**
	 * 添加出库
	 * @param inventoryRecordAdd
	 * @return
	 * @throws CommodityNotFindException 
	 */
	public Message<?> save(InventoryRecordAdd inventoryRecordAdd) throws CommodityNotFindException{
		logger.info("出库信息添加 参数【{}】",inventoryRecordAdd);
		/* 生成出库单号 */
		String no = billNoUtils.getBillNo(Constant.WARE_OUT,inventoryRecordAdd.getShopId());
		/* 添加出库单号 */
		inventoryRecordAdd.setOrderNumber((inventoryRecordAdd.getOrderNumber() == null)?no:inventoryRecordAdd.getOrderNumber());
		inventoryRecordAdd.setOutOfStorage(OutOfStorage.PLACING);
		Message<?> message = ruleCheckService.inventoryRecordSaveCheck(inventoryRecordAdd);
		if (message.getCode() != 0) {
			logger.info("出库信息 参数校验错误【{}】",message.getMessage());
			return message;
		}
		message =  inventoryService.saveInventoryRecord(inventoryRecordAdd);
		return message;
	}
	
	/**
	 * 修改出库单
	 * @param inventoryRecordModify
	 * @return
	 * @throws CommodityNotFindException 
	 */
	public Message<?> update(Long id , InventoryRecordModify inventoryRecordModify) throws CommodityNotFindException{
		InventoryRecord inventoryRecord = inventoryService.getById(id);
		logger.info("出库信息 id查询后对象【{}】",inventoryRecord);
		Message<?> message = ruleCheckService.inventoryRecordUpdateCheck(id, inventoryRecordModify);
		if (message.getCode() != 0) {
			logger.info("出库信息 参数校验错误【{}】",message.getMessage());
			return message;
		}
		message =  inventoryService.updateInventoryRecord(inventoryRecord,inventoryRecordModify);
		return message;
	}

	/**
	 * 拆分添加出库记录
	 * @param commodity
	 * @param inventoryRecord
	 */
	public InventoryRecord addSplitPlacing(InvoicingCommodity ic, Long shopId, Double splitNumber) {
		Commodity commodity = ic.getCommodity();
		List<InventoryRecordCommodity> ircs = new ArrayList<>();
		logger.info("出库信息业务处理(商品信息生成出库记录（拆分出库）) . 参数出库商品信息【{}】, 拆分数量【{}】, 店铺id【{}】",commodity ,splitNumber, shopId);
		if (commodity.getBuyingPrice() == null) {commodity.setBuyingPrice(new BigDecimal(0));}
		BigDecimal totalPrice = commodity.getBuyingPrice().multiply(new BigDecimal(splitNumber));//计算出库总商品价格
		Double blance = Arith.sub(ic.getInventoryNumber(), splitNumber);//结存
		/* 拆分出库详情实体添加属性 */
		InventoryRecordCommodity irc = 
				new InventoryRecordCommodity(commodity.getSellingPrice(), splitNumber, commodity.getBarcode(),
						commodity.getName(), commodity.getSpecification(), commodity.getUnit(), commodity.getId(), blance, totalPrice);
		ircs.add(irc);
		/* 出库减少库存 */
		ic.setInventoryNumber(blance);
		invoicingCommodityNewService.save(ic);
		/* 生成出库单号 */
		String no = billNoUtils.getBillNo(Constant.WARE_OUT, shopId);
		/* 拆分出库实体添加详情 */
		InventoryRecord placingAdd = 
				new InventoryRecord(no , null, Constant.SPLIT_PLACING, ircs, null, shopId,
						Status.ORDERCOMMIT, null, OutOfStorage.PLACING, totalPrice);//创建出库信息entity模型
		logger.info("出库信息业务处理(商品信息生成出库记录（拆分出库）) . 参数出库记录【{}】",placingAdd);
		return inventoryService.save(placingAdd);
	}

}
