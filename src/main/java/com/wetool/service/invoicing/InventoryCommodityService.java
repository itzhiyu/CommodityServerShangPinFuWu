package com.wetool.service.invoicing;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wetool.common.model.Message;
import com.wetool.entity.Commodity;
import com.wetool.entity.invoicing.InventoryRecordCommodity;
import com.wetool.entity.invoicing.InvoicingCommodity;
import com.wetool.exception.CommodityNotFindException;
import com.wetool.jpa.invoicing.InventoryRecordCommodityRepo;
import com.wetool.model.enums.OutOfStorage;
import com.wetool.service.commodity.CommodityNewService;
import com.wetool.utils.Arith;
import com.wetool.utils.BeanUtils;

/**
 * 出入库详情业务处理
 * @author li
 *
 */
@Service
public class InventoryCommodityService {
	private static final Logger logger = LogManager.getLogger(InventoryCommodityService.class);

	@Autowired
	private CommodityNewService commodityNewService;
	
	@Autowired
	private InvoicingCommodityNewService invoicingCommodityNewService;
	
	@Autowired
	private InventoryRecordCommodityRepo inventoryRecordCommodityRepo;
	
	/**
	 * 保存出入库集合
	 * @param list
	 * @return
	 */
	public List<InventoryRecordCommodity> save(List<InventoryRecordCommodity> list) {
		return inventoryRecordCommodityRepo.save(list);
	}
	
	/**
	 * 保存出入库详情　
	 * @param ircs
	 * @param shopId
	 * @param supplierId
	 * @return
	 * @throws CommodityNotFindException 
	 */
	@Transactional
	public List<InventoryRecordCommodity> save(List<InventoryRecordCommodity> ircs, Long shopId, Long supplierId, OutOfStorage outOfStorage) throws CommodityNotFindException{
		for (InventoryRecordCommodity inventoryRecordCommodity : ircs) {
			logger.info("保存出入库详情 . 获取库存信息 店铺id【{}】, 商品id【{}】", shopId, inventoryRecordCommodity.getCommodityId());
			Commodity commodity = commodityNewService.getById(inventoryRecordCommodity.getCommodityId());
			if (commodity == null) {
				throw new CommodityNotFindException("商品信息不存在！商品id：" +  inventoryRecordCommodity.getCommodityId() + "");
			}
			Message<InvoicingCommodity> message = invoicingCommodityNewService.
					findOrAdd(commodity, shopId, supplierId, 0D);
			InvoicingCommodity invoicingCommodity = message.getData();
			logger.info("保存出入库详情 . 获取库存信息【{}】", invoicingCommodity);
			Double number = invoicingCommodity.getInventoryNumber();
			if(outOfStorage == OutOfStorage.PLACING) {//出库对库存修改
				number = Arith.sub(number, inventoryRecordCommodity.getCommodityNumber());
				logger.info("保存出入库详情 .　初始库存数【{}】， 出库库存修改后数量【{}】", inventoryRecordCommodity.getCommodityNumber(), number);
			}
			if (outOfStorage == OutOfStorage.STORAGE) {//入库对库存修改
				number = Arith.add(number, inventoryRecordCommodity.getCommodityNumber());
				logger.info("保存出入库详情 .　初始库存数【{}】， 入库库存修改后数量【{}】", inventoryRecordCommodity.getCommodityNumber(), number);
			}
			invoicingCommodity.setSupplierId(supplierId);//添加服务商
			invoicingCommodity.setInventoryNumber(number);//库存数量保存
			inventoryRecordCommodity.setBalance(number);//添加结存
			invoicingCommodityNewService.save(invoicingCommodity);//对库存数据保存
			logger.info("保存出入库详情 . 保存库存信息【{}】", invoicingCommodity);
		}
		return inventoryRecordCommodityRepo.save(ircs);
	}
	
	/**
	 * 属性值复制
	 * @param object
	 * @return
	 */
	@Transactional
	public List<InventoryRecordCommodity> copy(Object object) {
		List<?> list = (List<?>) object;//泛型强转
		logger.info("出入库商品详情属性复制 . 出入库详情DTO数据【{}】", object);
		List<InventoryRecordCommodity> ircs = new ArrayList<>();
		/* 遍历添加属性 */
		list.stream().forEach(obj -> {InventoryRecordCommodity irc = new InventoryRecordCommodity();
			BeanUtils.copyProperties(obj, irc);ircs.add(irc);});
		logger.info("出入库商品详情属性复制 . 出入库详情复制后实体数据【{}】", ircs);
		return ircs;
	}

	/**
	 * 删除修改后一对多冗余数据
	 */
	public void deletedByIdNull() {
		List<InventoryRecordCommodity> ircs = inventoryRecordCommodityRepo.findByInventoryRecordIdIsNull();
		inventoryRecordCommodityRepo.delete(ircs);
	}
	
}
