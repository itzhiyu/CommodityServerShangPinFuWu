package com.wetool.service.invoicing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wetool.Constant;
import com.wetool.common.commodity.CommodityResult;
import com.wetool.common.model.Message;
import com.wetool.entity.Commodity;
import com.wetool.entity.invoicing.InventoryRecord;
import com.wetool.entity.invoicing.InventoryRecordCommodity;
import com.wetool.entity.invoicing.InvoicingCommodity;
import com.wetool.exception.CommodityNotFindException;
import com.wetool.model.enums.OutOfStorage;
import com.wetool.model.enums.Status;
import com.wetool.model.invoicing.InventoryRecordAdd;
import com.wetool.model.invoicing.InventoryRecordModify;
import com.wetool.service.commodity.RuleCheckService;
import com.wetool.utils.Arith;
import com.wetool.utils.BillNoUtils;

/**
 * 入库业务处理
 * @author lixin
 */
@Service
public class StorageService {
	private static final Logger logger = LogManager.getLogger(StorageService.class); 
	
	@Autowired
	private BillNoUtils billNoUtils;
	
	@Autowired
	private RuleCheckService ruleCheckService;
	
	@Autowired
	private InvoicingCommodityNewService invoicingCommodityNewService;
	
	@Autowired
	private InventoryService inventoryService;
	
	/**
	 * 添加入库记录
	 * @param inventoryRecordAdd
	 * @return
	 * @throws CommodityNotFindException
	 */
	public Message<?> save(InventoryRecordAdd inventoryRecordAdd) throws CommodityNotFindException {
		logger.info("入库信息 参数【{}】",inventoryRecordAdd);
		String no = billNoUtils.getBillNo(Constant.WARE_IN,inventoryRecordAdd.getShopId());
		inventoryRecordAdd.setOrderNumber((inventoryRecordAdd.getOrderNumber() == null)?no:inventoryRecordAdd.getOrderNumber());
		inventoryRecordAdd.setOutOfStorage(OutOfStorage.STORAGE);
		Message<?> message = ruleCheckService.inventoryRecordSaveCheck(inventoryRecordAdd);
		if (message.getCode() != 0) {
			return message;
		}
		inventoryService.saveInventoryRecord(inventoryRecordAdd);
		return new Message<>(CommodityResult.SUCCESS);
	}
	
	/**
	 * 修改入库记录
	 * @param inventoryRecordAdd
	 * @return
	 * @throws CommodityNotFindException
	 */
	public Message<?> update(Long id, InventoryRecordModify inventoryRecordModify) throws CommodityNotFindException {
		InventoryRecord inventoryRecord = inventoryService.getById(id);
		Message<?> message = ruleCheckService.inventoryRecordUpdateCheck(id, inventoryRecordModify);
		if (message.getCode() != 0) {
			return message;
		}
		logger.info("入库信息 参数【{}】",inventoryRecordModify);
		inventoryService.updateInventoryRecord(inventoryRecord, inventoryRecordModify);
		return new Message<>(CommodityResult.SUCCESS);
	}

	/**
	 * 添加拆分入库信息
	 * @param ic
	 * @param sunNumber
	 * @param shopId
	 */
	public synchronized InventoryRecord addSplitStorage(InvoicingCommodity ic, Double sunNumber, Long shopId) {
		Commodity commodity = ic.getCommodity();
		logger.info("出库信息业务处理(商品信息生成出库记录（拆分入库）) . 参数出库商品信息【{}】, 拆分入库数量【{}】, 店铺id【{}】",commodity ,sunNumber, shopId);
		List<InventoryRecordCommodity> ircs = new ArrayList<>();
		if (commodity.getBuyingPrice() == null) {commodity.setBuyingPrice(new BigDecimal(0));}
		/* 计算入库总商品价格 */
		BigDecimal totalPrice = commodity.getBuyingPrice().multiply(new BigDecimal(sunNumber));
		/* 结存 */
		Double blance = Arith.add(ic.getInventoryNumber(), sunNumber);
		/* 拆分入库详情实体添加属性 */
		InventoryRecordCommodity irc = 
				new InventoryRecordCommodity(commodity.getSellingPrice(), sunNumber, commodity.getBarcode(),
						commodity.getName(), commodity.getSpecification(), commodity.getUnit(), commodity.getId(), blance, totalPrice);
		/* 添加库存 */
		ic.setInventoryNumber(blance);
		invoicingCommodityNewService.save(ic);
		ircs.add(irc);
		/* 生成入库单号 */
		String no = billNoUtils.getBillNo(Constant.WARE_IN, shopId);
		/* 拆分入库实体添加详情 */
		InventoryRecord inventoryRecord = 
				new InventoryRecord(no , null, Constant.SPLIT_STORAGE, ircs, null, shopId,
						Status.ORDERCOMMIT, null, OutOfStorage.STORAGE, totalPrice);//创建出库信息entity模型
		logger.info("出库信息业务处理(商品信息生成出库记录（拆分出库）) . 参数出库记录【{}】",inventoryRecord);
		return inventoryService.save(inventoryRecord);
	}
	
}
