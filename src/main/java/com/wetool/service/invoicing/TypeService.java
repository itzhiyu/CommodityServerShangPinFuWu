package com.wetool.service.invoicing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.wetool.model.invoicing.PlacingType;
import com.wetool.model.invoicing.StorageType;

import lombok.Data;

/**
 * 描述：出库类型service
 * @author lixin
 */
@Component
@Data
@ConfigurationProperties(prefix = "type")
public class TypeService {
	
	private Long[] array1;
	private Long[] array2;
	private List<Map<String, String>> listProp1 = new ArrayList<>(); // 接收prop1里面的属性值
	private Map<Long, String> mapNames = new HashMap<>(); // 接收prop1里面的属性值
	private Map<Long, Long> parentProps = new HashMap<>();

	/**
	 * 读取配置文件信息展示
	 * 所有出库类型id 和 名称
	 * @return
	 */
	public List<StorageType> StorageTypelist(){
		List<StorageType> storageTypes = new ArrayList<>();
		for (Long long1 : array1) {
			StorageType storageType = new StorageType();
			storageType.setId(long1);
			String name = getTypeName(long1);
			storageType.setTypeName(name);
			storageTypes.add(storageType);
		}
		return storageTypes;
	}
	
	/**
	 * 读取配置文件信息展示
	 * 所有出库类型id 和 名称
	 * @return
	 */
	public List<PlacingType> PlacingTypelist(){
		List<PlacingType> placingTypes = new ArrayList<>();
		for (Long long1 : array2) {
			PlacingType placingType = new PlacingType();
			placingType.setId(long1);
			String name = getTypeName(long1);
			placingType.setTypeName(name);
			placingTypes.add(placingType);
		}
		return placingTypes;
	}

	/** 根据配置文件中mapNames 的ID显示出库类型 */
	public String getTypeName(Long idString) {
		return this.mapNames.get(idString);
	}
}
