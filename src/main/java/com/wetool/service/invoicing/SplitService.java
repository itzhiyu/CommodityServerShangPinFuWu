package com.wetool.service.invoicing;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wetool.common.model.Message;
import com.wetool.entity.Commodity;
import com.wetool.entity.invoicing.InvoicingCommodity;
import com.wetool.exception.CommodityNotFindException;
import com.wetool.service.commodity.CommodityNewService;
import com.wetool.service.commodity.SplitCommodityRuleService;
import com.wetool.utils.Arith;

/**
 * 拆分业务处理
 * @author lixin
 */
@Service
public class SplitService {
	private static final Logger logger = LogManager.getLogger(SplitService.class);
	
	@Autowired
	private PlacingService placingService;
	
	@Autowired
	private StorageService storageService;
	
	@Autowired
	private CommodityNewService commodityNewService;
	
	@Autowired
	private SplitCommodityRuleService splitCommodityRuleService;

	@Autowired
	private InvoicingCommodityNewService invoicingCommodityNewService;
	
	/**
	 * 拆分处理
	 * @param commodityId 商品Id
	 * @param shopId　店铺id
	 * @param number 销售出库数量
	 * @param count 拆分次数
	 * @throws CommodityNotFindException
	 */
	@Transactional
	public void slpit(Long commodityId, Long shopId, Double number, Integer count) throws CommodityNotFindException {
		Commodity co = commodityNewService.getById(commodityId);
		if (co == null ) {
			logger.debug("商品不存在！"); 
			throw new CommodityNotFindException("商品不存在！"); 
		}
		Message<InvoicingCommodity> message = invoicingCommodityNewService.findOrAdd(co, shopId, null, 0D);
		InvoicingCommodity ic = message.getData();
		/* 被拆分商品规则 */
		List<Commodity> scrs = splitCommodityRuleService.findBySplit(commodityId);
		for (Commodity scr : scrs) {
			InvoicingCommodity inCommodity = invoicingCommodityNewService.findOrAdd(scr, shopId, null, 0D).getData();
			Double inventoryNumber = inCommodity.getInventoryNumber() == null ? 0D : inCommodity.getInventoryNumber();
			Double splitNumber = this.getSplitNumber(scr.getSplitRule().getSplitNumber(), number);//拆分数量
			if (splitNumber > inventoryNumber && count < 2) {//判断是否需要二次拆分
				/* 库存商品进行二次拆分 */
				logger.info("商品二级库存拆分 . 商品库存所需拆分数【{}】,商品拆分次数【{}】", splitNumber,count);
				slpit(scr.getId(), shopId, number, ++count);
			}
			placingService.addSplitPlacing(inCommodity, shopId, splitNumber);
			Double sunNumber = Arith.mul(scr.getSplitRule().getSplitNumber(), splitNumber);//入库数量
			storageService.addSplitStorage(ic, sunNumber, shopId);
			return ;
		}
	}
	
	/**
	 * 获取商品拆分商品数
	 * @param splitNumber 拆分数量
	 * @param SunNumber 商品总数(商品销售数或出库数)
	 * @return Double 需要拆分件数
	 */
	public Double getSplitNumber(Double splitNumber ,Double sunNumber){
		/* 库存余数和拆分数量为负数转化为正数 */
		splitNumber = (splitNumber < 0)?Arith.mul(splitNumber, -1):splitNumber;
		sunNumber = (sunNumber < 0)?Arith.mul(sunNumber , -1):sunNumber;
		/* 计算拆分数 */
		Double splitSunNumber = Arith.div(sunNumber, splitNumber, 1);
		/* 有小数累加１ */
		splitSunNumber = Math.ceil(Math.abs(sunNumber) / splitNumber);
		return splitSunNumber;
	}
}
