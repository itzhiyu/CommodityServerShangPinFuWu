package com.wetool.service.invoicing;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wetool.common.commodity.CommodityResult;
import com.wetool.common.model.Message;
import com.wetool.entity.Commodity;
import com.wetool.entity.invoicing.InvoicingCommodity;
import com.wetool.entity.invoicing.Supplier;
import com.wetool.jpa.invoicing.InvoicingCommodityRepo;
import com.wetool.utils.BeanUtils;

@Service
public class InvoicingCommodityNewService {
	private static final Logger logger = LogManager.getLogger(InvoicingCommodityNewService.class);
	
	@Autowired
	private InvoicingCommodityRepo invoicingCommodityRepo;
	
	/**
	 * 查询商品库存对应供应商
	 * @param id
	 * @param shopId
	 * @return
	 */
	public Supplier getSupplierByCommodityIdAndShopId(Long id, Long shopId) {
		logger.info("查询商品库存对应供应商 . 参数　商品id【{}】, 店铺id 【{}】", id, shopId);
		Supplier supplier = invoicingCommodityRepo.getSupplierByCommodityAndShopId(id, shopId);
		logger.info("商品最近供应商 . 返回　供应商信息【{}】", supplier);
		return supplier;
	}

	/**
	 * 商品id和店铺id查询库存信息
	 * @param commodityId
	 * @param shopId
	 * @return
	 */
	public InvoicingCommodity getByCommodityIdAndShopId(Long commodityId, Long shopId) {
		logger.info("查询库存信息 . 参数　商品id【{}】, 店铺id 【{}】", commodityId, shopId);
		InvoicingCommodity ic = invoicingCommodityRepo.getCommodityAndShopId(commodityId, shopId);
		return ic;
	}
	
	/**
	 * 实体保存方法
	 * @param invoicingCommodity
	 */
	public void save(InvoicingCommodity invoicingCommodity) {
		invoicingCommodityRepo.saveAndFlush(invoicingCommodity);
	}
	
	/**
	 * 查询或添加商品库存信息
	 * @param commodity
	 * @param shopId
	 * @param supplierId
	 * @return
	 */
	public Message<InvoicingCommodity> findOrAdd(Commodity commodity,Long shopId,Long supplierId, Double number) {
		logger.info("添加商品库存信息 . 参数 商品信息【{}】, 店铺id【{}】, 供应商id【{}】", commodity, shopId, supplierId);
		InvoicingCommodity invoicingCommodity = invoicingCommodityRepo.findOneByCommodityAndShopIdAndIsDeleted(commodity.getId(), shopId, false);
		if (invoicingCommodity != null) {
			return new Message<>(CommodityResult.SUCCESS, invoicingCommodity);
		}
		invoicingCommodity = new InvoicingCommodity(); 
		BeanUtils.copyProperties(commodity, invoicingCommodity);
		invoicingCommodity.setId(null);
		invoicingCommodity.setInventoryNumber(number);
		invoicingCommodity.setCommodity(commodity);
		invoicingCommodity.setSupplierId(supplierId);
		invoicingCommodity.setShopId(shopId);
		logger.info("添加商品库存信息 . 参数 库存信息【{}】", invoicingCommodity);
		invoicingCommodityRepo.saveAndFlush(invoicingCommodity);
		return new Message<>(CommodityResult.SUCCESS, invoicingCommodity);
	}

	/**
	 * 修改商品同步修改库存信息
	 * @param commodity
	 * @return
	 */
	public Message<?> update(Commodity commodity) {
		logger.info("修改商品同步修改库存信息 . 参数　商品信息【{}】", commodity);
		List<InvoicingCommodity> ics = invoicingCommodityRepo.getByCommodity(commodity);
		if (ics == null || ics.isEmpty()) {
			logger.info("修改商品同步修改库存信息 . 库存信息为空【{}】", ics);
			return new Message<>(CommodityResult.SUCCESS);
		}
		for (InvoicingCommodity invoicingCommodity : ics) {
			Long icId = invoicingCommodity.getId();
			BeanUtils.copyProperties(commodity, invoicingCommodity);//属性值复制
			invoicingCommodity.setId(icId);
			invoicingCommodity.setCommodity(commodity);
			invoicingCommodityRepo.saveAndFlush(invoicingCommodity);
		}
		logger.info("修改商品同步修改库存信息 . 返回修改库存信息【{}】", ics);
		return new Message<>(CommodityResult.SUCCESS);
	}
	
}
