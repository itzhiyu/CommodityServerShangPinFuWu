package com.wetool.service.invoicing;

import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wetool.Constant;
import com.wetool.entity.Commodity;
import com.wetool.entity.invoicing.InventoryRecord;
import com.wetool.entity.invoicing.InventoryRecordCommodity;
import com.wetool.entity.invoicing.InvoicingCommodity;
import com.wetool.exception.CommodityNotFindException;
import com.wetool.exception.invoicing.ShopNotLoginException;
import com.wetool.model.enums.Status;
import com.wetool.model.invoicing.InventoryRecordAdd;
import com.wetool.service.commodity.CommodityNewService;
import com.wetool.utils.Arith;
import com.wetool.utils.BillNoUtils;

/**
 * 销售出库业务处理
 * @author lixin
 */
@Service
public class SalePlacingService {
	private static final Logger logger = LogManager.getLogger(SalePlacingService.class); 

	@Autowired
	private BillNoUtils billNoUtils;
	
	@Autowired
	private SplitService splitService;
	
	@Autowired
	private InventoryService inventoryService;
	
	@Autowired
	private CommodityNewService commodityNewService;
	
	@Autowired
	private InventoryCommodityService inventoryCommodityService;
	
	@Autowired
	private InvoicingCommodityNewService invoicingCommodityNewService;
	
	/**
	 * 销售出库业务处理
	 * @param inventoryRecordAdd
	 * @throws Exception
	 */
	public void saveSalePlacing(InventoryRecordAdd inventoryRecordAdd) throws Exception {
		if (inventoryRecordAdd.getShopId() == null) {
			logger.debug("销售出库业务处理(添加线上销售出库) 店铺ID为空");
			throw new ShopNotLoginException("销售出库业务处理(添加线上销售出库) 店铺ID为空");
		}
		/* 生成入库单号 */
		String no = billNoUtils.getBillNo(Constant.WARE_OUT,inventoryRecordAdd.getShopId());
		inventoryRecordAdd.setOrderNumber(no);//赋值入库单号
		inventoryRecordAdd.setStatus(Status.GENERATEORDER);//修改状态
		/* 保存销售出库记录 */
		InventoryRecord inventoryRecord = inventoryService.saveInventoryRecord(inventoryRecordAdd).getData();
		inventoryRecord.setStatus(Status.ORDERCOMMIT);//状态值改为已提交
		logger.info("销售出库业务处理　销售出库记录【{}】", inventoryRecord);
		if (inventoryRecord.getStatus() == Status.ORDERCOMMIT) {
			this.addSale(inventoryRecord.getCommoditys(),inventoryRecord.getType(),inventoryRecord.getShopId());//订单提交对商品库存修改
		}
		inventoryService.save(inventoryRecord);//DB中保存出库数据
	}
	
	/**
	 * 线上销售订单提交对商品库存修改
	 * @param pcs
	 * @return
	 * @throws Exception 
	 */
	private List<InventoryRecordCommodity> addSale(List<InventoryRecordCommodity> ircs,Long type,Long shopId) 
		throws Exception {
		for (InventoryRecordCommodity irc : ircs) {
			Long id = irc.getCommodityId();// 获取商品库存中ID
			Commodity commodity = commodityNewService.getById(id);
			if (commodity == null || commodity.getIsDeleted()) {
				logger.debug("销售出库业务处理(销售订单提交对商品库存修改) 商品不存在");
				throw new CommodityNotFindException("销售出库业务处理(销售订单提交对商品库存修改) 商品不存在或逻辑删除");//商品信息
			}
			InvoicingCommodity ic = invoicingCommodityNewService.findOrAdd(commodity, shopId, null, 0D).getData();// 获取商品库存信息
			Double invoicingNum = Arith.sub(ic.getInventoryNumber(), irc.getCommodityNumber());
			logger.info("销售出库业务处理(销售订单提交对商品库存修改) . 商品库存扣除数量【{}】", invoicingNum);
			if (ic.getInventoryNumber() <= 0 || invoicingNum <= 0) {// 对商品库存做判断
				splitService.slpit(irc.getCommodityId(), shopId, invoicingNum, 1);//商品拆分处理
			}
			/* 拆分后库存数重新扣除 */
			invoicingNum = Arith.sub(ic.getInventoryNumber(), irc.getCommodityNumber());
			irc.setBalance(invoicingNum);// 添加结存
			ic.setInventoryNumber(invoicingNum);// 库存数
			ic.setUpdateDate(new Date());//修改时间
			invoicingCommodityNewService.save(ic);
		}
		inventoryCommodityService.save(ircs);//保存出库商品信息
		return ircs;
	}
}
