package com.wetool.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wetool.entity.Commodity;
import com.wetool.entity.Resource;
import com.wetool.jpa.ResourceRepo;

@Service
public class ResourceService {
	
	@Autowired
	private ResourceRepo resourceRepo; 
	
	public void resourceHandle(Resource resource ,Commodity commodity) {
			String url = resource.getResUrl();
			List<Resource> resources = commodity.getResources();
			if (url!= null && !"".equals(url)){
				if (resources.isEmpty()) {
					List<Resource> list = new ArrayList<>();
					list.add(resource);
					commodity.setResources(list);
				}
				for (Resource rs : resources) {//商家库商品同步图片资源为单张(为以后扩展)
					if (rs.getId() != null) {
						resource.setId(rs.getId());
					}
					BeanUtils.copyProperties(resource, rs);
					continue;
				}
			}
			if ("".equals(url) && resource.getId() == null && !resources.isEmpty()) {
				resources.clear();
				resourceRepo.delete(resources);
			}
	}

	public List<Resource> save(List<Resource> rs) {
		return resourceRepo.save(rs);
	}
}
