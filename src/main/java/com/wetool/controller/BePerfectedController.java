package com.wetool.controller;

import java.security.Principal;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.wetool.exception.ParamCheckException;
import com.wetool.model.BePerfectedCommodity;
import com.wetool.model.CommodityAdd;
import com.wetool.model.CommodityPerfectedAdd;
import com.wetool.model.Message;
import com.wetool.model.Message.Result;
import com.wetool.service.commodity.BePerfectedService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/bePerfected")
@RefreshScope
@Api(description = "快录商品管理")
public class BePerfectedController {
	private static final Logger logger = LogManager.getLogger(BePerfectedController.class);

	@Autowired
	private BePerfectedService bePerfectedService;
	
	@ApiOperation(value="查询快录信息列表", notes="已登录的商家查询快录信息列表", response = Message.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name = "merchantId", value = "商家ID", required = true, dataType = "Long", paramType = "query", defaultValue = "1"),
			@ApiImplicitParam(name = "key", value = "商品条码", required = true, dataType = "String", paramType = "query", defaultValue = "63902888111222"),
	    	@ApiImplicitParam(name = "page", value = "第几页", required = false, dataType = "int", paramType = "query", defaultValue = "0"),
	    	@ApiImplicitParam(name = "size", value = "每页行数", required = false, dataType = "int", paramType = "query", defaultValue = "20"),
	    	@ApiImplicitParam(name = "sort", value = "排序规则", required = false, dataType = "String", paramType = "query", defaultValue = "createDate,asc")
	    })
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 19,message = "商家未登录!")
	})
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> list(Principal principal, Long merchantId,Pageable p,String key, 
			final PagedResourcesAssembler<BePerfectedCommodity> assembler)throws Exception {
		logger.info("进入接口>查询快录信息列表 . 参数：商家ID【{}】",merchantId);
		Page<BePerfectedCommodity> page = bePerfectedService.findAll(p,merchantId,key);
		logger.info("结束接口>查询快录信息列表 ");
		return new ResponseEntity<Object>(new Message<>(Result.SUCCESS,assembler.toResource(page)), HttpStatus.OK); 
	}
	
	@ApiOperation(value="查询单个快录信息", notes="已登录的商家查询单个快录信息", response = Message.class)
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "快录商品ID", required = true, paramType = "path", dataType = "Long",defaultValue = "1"),
	})
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 17,message = "未找到快录商品信息！")
	})
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(Principal principal,@PathVariable Long id)
	  throws Exception{
		logger.info("进入接口>查询单个快录信息 . 参数：快录商品Id【{}】",id);
		CommodityAdd ca = bePerfectedService.getById(id);
		logger.info("结束接口>查询单个快录信息 ");
		return new ResponseEntity<Object>(new Message<>(Result.SUCCESS,ca), HttpStatus.OK); 
	}
	
	@ApiOperation(value="查询商家快录商品数", notes="已登录的商家查询快录商品数", response = Message.class)
	@ApiImplicitParam(name = "merchantId", value = "商家ID", required = true, dataType = "Long", paramType = "query", defaultValue = "1")
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 19,message = "商家未登录!")
	})
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public ResponseEntity<?> count(Principal principal, Long merchantId) throws Exception{
		logger.info("进入接口>查询商家快录商品数 . 参数：商家ID【{}】",merchantId);
		Long count = bePerfectedService.CountByMerchantId(merchantId);
		logger.info("结束接口>查询商家快录商品数 ");
		return new ResponseEntity<Object>(new Message<>(Result.SUCCESS,count), HttpStatus.OK); 
	}
	
	@ApiOperation(value="添加快录商品", notes="已登录的商家添加快录商品", response = Message.class)
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 6, message = "参数格式错误!"),
			@ApiResponse(code = 16,message = "商品条形码重复！"),
			@ApiResponse(code = 19,message = "商家未登录!")
	})
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> create(Principal principal,@Valid@RequestBody CommodityPerfectedAdd commodityPerfectedAdd,
			BindingResult bindingResult) throws Exception{
		logger.info("进入接口>添加快录商品 . 参数：商家ID【{}】",commodityPerfectedAdd);
		if (bindingResult.hasErrors()) {
			throw new ParamCheckException(bindingResult);
		}
		BePerfectedCommodity co = bePerfectedService.create(commodityPerfectedAdd);
		logger.info("结束接口>添加快录商品 ");
		return new ResponseEntity<Object>(new Message<>(Result.SUCCESS,co), HttpStatus.OK); 
	}

	@ApiOperation(value="快录商品添加成正式商品", notes="已登录的商家把快录商品添加成正式商品", response = Message.class)
	@ApiImplicitParam(name = "id", value = "快录商品ID", required = true, paramType = "path", dataType = "Long",defaultValue = "1")
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 8, message = "参数不正确!"),
			@ApiResponse(code = 14,message = "商品添加失败！")
			})
	@RequestMapping(value = "/saveCommodity/{id}", method = RequestMethod.PATCH)
	public ResponseEntity<?> saveCommodity(Principal principal,@PathVariable Long id,@RequestBody CommodityAdd commodityAdd)
	  throws Exception{
		logger.info("进入接口>快录商品添加成正式商品 . 参数：商品信息【{}】,",commodityAdd);
		bePerfectedService.saveCommodity(id,commodityAdd);
		logger.info("结束接口>快录商品添加成正式商品 ");
		return new ResponseEntity<Object>(new Message<>(Result.SUCCESS), HttpStatus.OK); 
	}

	@ApiOperation(value="删除快录商品", notes="已登录的商家删除快录商品", response = Message.class)
	@ApiImplicitParam(name = "id", value = "快录商品ID", required = true, paramType = "path", dataType = "Long",defaultValue = "1")
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！")
	})
	@RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(Principal principal, @PathVariable Long id) throws Exception{
		logger.info("进入接口>删除快录商品 . 参数：快录商品id【{}】,",id);
		bePerfectedService.delet(id);
		logger.info("结束接口>删除快录商品 ");
		return new ResponseEntity<Object>(new Message<>(Result.SUCCESS), HttpStatus.OK); 
	}
}
