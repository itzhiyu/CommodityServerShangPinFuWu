package com.wetool.controller;

import com.wetool.model.Message;
import com.wetool.model.Message.Result;
import com.wetool.service.commodity.PLUService;
import com.wetool.model.PLUModelModify;
import com.wetool.model.PluModel;

import io.swagger.annotations.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/plu")
@RefreshScope
@Api(description = "PLU码控制器")
public class PLUController {
	
	private static final Logger logger = LogManager.getLogger(PLUController.class);
	
	@Autowired
	private PLUService pLUService;
	
	@ApiOperation(value="PLU码查询", notes="已登录商家查询PLU码", response = Message.class)
	@ApiImplicitParam(name = "shopId", value = "店铺ID", required = true,paramType = "path", dataType = "Long")
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 19,message = "店铺未登录!")
	})
	@RequestMapping(value = "/{shopId}/{merchantId}", method = RequestMethod.GET)
	public ResponseEntity<?> getPLU(@PathVariable Long shopId,@PathVariable Long merchantId) throws Exception{
		logger.info("进入接口>PLU码查询 . 参数：店铺ID【{}】",shopId);
		String arrayString = pLUService.getMinPLUCode(shopId,merchantId);
		logger.info("结束接口>PLU码查询 ");
		return new ResponseEntity<Object>(new Message<>(Result.SUCCESS,arrayString), HttpStatus.CREATED);
	}
	
	@ApiOperation(value="PLU传秤列表", notes="已登录店铺查询PLU传秤", response = Message.class)
	@ApiImplicitParams({
		@ApiImplicitParam(name = "shopId", value = "店铺ID", required = true,paramType = "path", dataType = "Long"),
		@ApiImplicitParam(name = "size", value = "查询条数", required = true,paramType = "query", dataType = "Long")
	})
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 19,message = "店铺未登录!")
	})
	@RequestMapping(value = "/list/{shopId}", method = RequestMethod.GET)
	public ResponseEntity<?> list(@PathVariable Long shopId,Long size) throws Exception{
		logger.info("进入接口>PLU传秤 . 参数：店铺ID【{}】",shopId);
		List<PluModel> pluModels = pLUService.list(shopId,size);
		logger.info("结束接口>PLU传秤 ");
		return new ResponseEntity<Object>(new Message<>(Result.SUCCESS,pluModels), HttpStatus.CREATED);
	}
	
	
	@ApiOperation(value="PLU传秤修改对应商品ID", notes="PLU传秤修改对应商品ID", response = Message.class)
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 19,message = "店铺未登录!")
	})
	@RequestMapping(value = "/update", method = RequestMethod.PATCH)
	public ResponseEntity<?> updatePlu(@RequestBody PLUModelModify pLUModelModify) throws Exception{
		logger.info("进入接口>PLU传秤 . 参数：店铺ID【{}】,修改参数【{}】",pLUModelModify.getShopId(),pLUModelModify);
		 pLUService.add(pLUModelModify.getShopId(),pLUModelModify);
		logger.info("结束接口>PLU传秤 ");
		return new ResponseEntity<Object>(new Message<>(Result.SUCCESS), HttpStatus.CREATED);
	}
}
