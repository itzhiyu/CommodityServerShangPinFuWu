package com.wetool.controller;

import com.wetool.common.commodity.CommodityResult;
import com.wetool.common.model.Message;
import com.wetool.entity.Commodity;
import com.wetool.exception.CategoryNotFindException;
import com.wetool.exception.CommodityNotFindException;
import com.wetool.exception.MerchantNotLoginException;
import com.wetool.exception.ParamCheckException;
import com.wetool.model.CommodityAdd;
import com.wetool.model.CommodityModify;
import com.wetool.model.CommodityQuery;
import com.wetool.model.CommodityReceive;
import com.wetool.model.SearchParam;
import com.wetool.model.dto.CommodityAdds;
import com.wetool.model.dto.CommodityDto;
import com.wetool.model.dto.CommodityDtoShow;
import com.wetool.model.dto.CommodityModifys;
import com.wetool.service.CommodityService;
import com.wetool.service.commodity.CommodityNewService;
import com.wetool.service.invoicing.SplitService;

import io.swagger.annotations.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.security.Principal;
import java.text.ParseException;
import java.util.List;

/**
 * 商品管理控制器(商家库)
 * @author lixin
 */
@RestController
@RequestMapping("/commodity")
@RefreshScope
@Api(description = "商品管理(商家库)")
public class CommodityController {
	
	private static final Logger logger = LogManager.getLogger(CommodityController.class); 

	@Autowired
	private CommodityService commodityService;
	
	@Autowired
	private SplitService splitService;
	
	@Autowired
	private CommodityNewService commodityNewService;
	

	@ApiOperation(value="商品信息列表", notes="已登录商家查询商品列表", response = Message.class)
    @ApiImplicitParams({
    	@ApiImplicitParam(name = "merchantId", value = "商家ID", required = true,paramType = "query", dataType = "Long"),
    	@ApiImplicitParam(name = "page", value = "第几页", required = false, dataType = "int", paramType = "query", defaultValue = "0"),
    	@ApiImplicitParam(name = "size", value = "每页行数", required = false, dataType = "int", paramType = "query", defaultValue = "20"),
    	@ApiImplicitParam(name = "sort", value = "排序规则", required = false, dataType = "String", paramType = "query", defaultValue = "createDate,asc")
    })
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 19,message = "商家未登录!"),
			@ApiResponse(code = 18,message ="时间格式错误!")
	})
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseEntity<?> list(Principal principal,Long merchantId,SearchParam searchParam, Pageable p, final PagedResourcesAssembler<Commodity> assembler) 
		throws MerchantNotLoginException, ParseException, CategoryNotFindException {
		logger.info("进入接口>商品信息列表(商家库) . 参数：商家ID【{}】",merchantId);
		searchParam.setMerchantId(merchantId);
		Page<Commodity> adverts = commodityService.find(searchParam.getMerchantId(), searchParam, p);
		PagedResources<Resource<Commodity>>  sc = assembler.toResource(adverts);
		logger.info("结束接口>商品信息列表(商家库) ");
		return new ResponseEntity<>(new Message<>(CommodityResult.SUCCESS, sc), HttpStatus.OK);
	}
	
	@ApiOperation(value="根据商品编号查询商品信息", notes="已登录商家查询编号商品信息",response = Message.class)
	@ApiImplicitParams({
		@ApiImplicitParam(name = "merchantId", value = "商家ID", required = true,paramType = "query", dataType = "Long"),
		@ApiImplicitParam(name = "id", value = "商品ID", required = true,paramType = "path", dataType = "Long",defaultValue = "1084")
	})
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 5,message = "商品信息不存在!"),
			@ApiResponse(code = 8,message = "参数不正确！")
	})
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> findOne(Principal principal,Long merchantId,@PathVariable Long id) 
			throws CommodityNotFindException, MerchantNotLoginException {
		logger.info("进入接口>商品信息列表(商家库) . 参数：商家ID【{}】",merchantId);
		if (merchantId == null) {
			logger.debug("根据商品编号查询商品信息 商家ID为空");
			throw new MerchantNotLoginException();
		}
		CommodityModify commodity = commodityService.findOne(id);
		logger.info("结束接口>商品信息列表(商家库) ");
		return new ResponseEntity<Object>(new Message<>(CommodityResult.SUCCESS, commodity), HttpStatus.OK);
	}
	
	@ApiOperation(value="添加商品", notes="已登录商家添加商品信息",response = Message.class)
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 14, message = "商品添加失败！"),
			@ApiResponse(code = 16,message = "商品条形码重复！"),
			@ApiResponse(code = 8, message = "参数不正确!")
	})
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> create(Principal principal, @Valid @RequestBody CommodityAdd commodityAdd,
			BindingResult bindingResult) throws Exception  {
		logger.info("进入接口>添加商品信息(商家库) . 参数：商品添加信息【{}】",commodityAdd);
		if (bindingResult.hasErrors()) {
			logger.debug("添加商品信息(商家库) 参数格式不正确");
			throw new ParamCheckException(bindingResult);
		}
		commodityService.save(commodityAdd);
		logger.info("结束接口>添加商品信息(商家库) ");
		return new ResponseEntity<Object>(new Message<>(CommodityResult.SUCCESS), HttpStatus.OK);
	}

	@ApiOperation(value="修改商品信息", notes="修改单个商品信息",response = Message.class)
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "商品ID", required = true,paramType = "path", dataType = "Long",defaultValue = "1533"),
	})
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 5, message = "商品信息不存在!"),
			@ApiResponse(code = 8, message = "参数不正确!"),
			@ApiResponse(code = 16,message = "商品条形码重复！"),
			@ApiResponse(code = 15,message = "商品修改信息失败！")
	})
	@RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
	public ResponseEntity<?> update(Principal principal, @PathVariable Long id,@Valid @RequestBody CommodityModify commodityModify,
			BindingResult bindingResult) throws Exception {
		logger.info("进入接口>修改商品信息(商家库) . 参数：商品修改信息【{}】",commodityModify);
		if (bindingResult.hasErrors()) {
			logger.debug("修改商品信息(商家库) 参数格式不正确");
			throw new ParamCheckException(bindingResult);
		}
		commodityModify.setId(id);
		commodityService.update(commodityModify);
		logger.info("结束接口>修改商品信息(商家库)");
		return new ResponseEntity<Object>(new Message<>(CommodityResult.SUCCESS), HttpStatus.OK);
	}
	
	@ApiOperation(value="批量修改商品分类信息", notes="批量商品分类信息修改",response = Message.class)
	@ApiImplicitParam(name = "categoryId", value = "商品分类ID", required = true,paramType = "path", dataType = "Long",defaultValue = "24")
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 5,message = "商品信息不存在!"),
	})
	@RequestMapping(value = "/updateBatch/{categoryId}", method = RequestMethod.PATCH)
	public ResponseEntity<?> upadteBatch(Principal principal, @PathVariable Long categoryId,
			@RequestBody List<Long> commodityIds) throws CommodityNotFindException {
		logger.info("进入接口>批量修改商品分类信息(商家库) . 参数：商品分类ID【{}】，修改分类商品id集合【{}】",categoryId,commodityIds);
		commodityService.upadteBatch(commodityIds, categoryId);
		logger.info("结束接口>批量修改商品分类信息(商家库)");
		return new ResponseEntity<Object>(new Message<>(CommodityResult.SUCCESS), HttpStatus.OK);
	}
	
	/////////////////////////POS端接口//////////////////////////////////////////////////
	@ApiOperation(value="提供扫码查询商品信息", notes="提供扫码查询商品信息",response = Message.class)
	@ApiImplicitParams({
		@ApiImplicitParam(name = "key", value = "商品条码", required = true,paramType = "path", dataType = "String",defaultValue = "6901294130036"),
		@ApiImplicitParam(name = "merchantId", value = "商家ID", required = true,paramType = "query", dataType = "Long")
	})
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 5,message = "商品信息不存在!"),
			@ApiResponse(code = 8,message = "参数不正确！")
	})
	@RequestMapping(value = "/findByBorcode/{key}", method = RequestMethod.GET)
	public ResponseEntity<?> findByBorcode(Principal principal,Long merchantId,@PathVariable String key) throws Exception {
		logger.info("进入接口>提供扫码查询商品信息 . 参数：商家ID【{}】，条码【{}】",merchantId,key);
		List<CommodityQuery> commoditys = commodityService.findByBorcode(key,merchantId);
		logger.info("结束接口>提供扫码查询商品信息 ");
		return new ResponseEntity<Object>(new Message<>(CommodityResult.SUCCESS, commoditys), HttpStatus.OK);
	}

	@ApiOperation(value="删除商品信息", notes="删除商品信息",response = Message.class)
	@ApiImplicitParam(name = "id", value = "商品id", required = true,paramType = "path", dataType = "Long",defaultValue = "84")
	@ApiResponses(value = {
			@ApiResponse(code = 0,message = "操作成功！"),
			@ApiResponse(code = 1,message = "操作失败！"),
			@ApiResponse(code = 5,message = "商品信息不存在!"),
			@ApiResponse(code = 12,message = "商品仍有库存!"),
			@ApiResponse(code = 13,message = "商品存在拆分商品!")
	})
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(Principal principal,@PathVariable Long id) throws Exception {
		logger.info("进入接口>删除商品信息 . 参数：商品ID【{}】",id);
		commodityService.delete(id);
		logger.info("结束接口>删除商品信息 ");
		return new ResponseEntity<Object>(new Message<>(CommodityResult.SUCCESS), HttpStatus.CREATED);
	}
	
    ////////////////////提供其他服务接口//////////////	
	
	@ApiOperation(value="提供库存查询商品信息", notes="提供库存查询商品信息",response = Message.class)
	@ApiImplicitParam(name = "id", value = "商品ID", required = true,paramType = "path", dataType = "Long",defaultValue = "1162")
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 5, message = "商品信息不存在!"),
			@ApiResponse(code = 12,message = "商品仍有库存!")
	})
	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET)
	public CommodityModify getById(@PathVariable Long id) {
		logger.info("进入接口>提供库存查询商品信息 . 参数：商品ID【{}】",id);
		CommodityModify commodity = commodityService.getById(id);
		logger.info("结束接口>提供库存查询商品信息 . 参数：商品信息【{}】",commodity);
		return commodity;
	}
	
	@ApiOperation(value="提供库存查询拆分信息", notes="提供库存查询拆分信息",response = Message.class)
	@ApiImplicitParam(name = "id", value = "商品ID", required = true,paramType = "path", dataType = "Long",defaultValue = "1162")
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 5, message = "商品信息不存在!"),
			@ApiResponse(code = 12,message = "商品仍有库存!")
	})
	@RequestMapping(value = "/findBySplit/{id}", method = RequestMethod.GET)
	public List<CommodityModify> findBySplit(@PathVariable Long id) {
		logger.info("进入接口>提供库存查询拆分信息 . 参数：商品信息ID【{}】",id);
		List<CommodityModify> scr = commodityService.findByCommodity(id);
		logger.info("结束接口>提供库存查询拆分信息 . 参数：拆分信息集合【{}】",scr);
		return scr;
	}
	
	@ApiOperation(value="提供库存保存商品信息", notes="提供库存修改商品商家(库存信息)",response = Message.class)
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "商品ID", required = true,paramType = "path", dataType = "Long",defaultValue = "1084"),
		@ApiImplicitParam(name = "inventoryNumber", value = "商家库商品增加后库存数", required = true,paramType = "path", dataType = "Long",defaultValue = "10")
	})
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 5, message = "商品信息不存在!")
	})
	@RequestMapping(value = "/updateInventoryNumber/{id}/{buyingPrice}", method = RequestMethod.PATCH)
	public ResponseEntity<?> updateInventoryNumber(@PathVariable("id") Long id,@PathVariable("buyingPrice") BigDecimal buyingPrice
		) throws CommodityNotFindException {
		logger.info("进入接口>提供库存保存商品信息 . 参数：商品ID【{}】,进货价格【{}】",id,buyingPrice);
		commodityService.updateInventoryNumber(id,buyingPrice);
		logger.info("结束接口>提供库存保存商品信息 ");
		return new ResponseEntity<Object>(new Message<>(CommodityResult.SUCCESS), HttpStatus.CREATED);
	}
	
	///////////////////////商品同步查询分页集合///////////////////////////////

	@ApiOperation(value="商品同步查询分页集合", notes="已登录商家查询商品列表", response = Message.class)
    @ApiImplicitParams({
    	@ApiImplicitParam(name = "merchantId", value = "商家ID", required = true,paramType = "query", dataType = "Long"),
    	@ApiImplicitParam(name = "page", value = "第几页", required = false, dataType = "int", paramType = "query", defaultValue = "0"),
    	@ApiImplicitParam(name = "size", value = "每页行数", required = false, dataType = "int", paramType = "query", defaultValue = "20"),
    	@ApiImplicitParam(name = "sort", value = "排序规则", required = false, dataType = "String", paramType = "query", defaultValue = "createDate,asc")
    })
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 19,message = "商家未登录!"),
			@ApiResponse(code = 18,message ="时间格式错误!")
	})
	@RequestMapping(value = "/findByUpTime", method = RequestMethod.GET)
	public ResponseEntity<?> findByUpTime(Principal principal,Long merchantId,SearchParam searchParam, Pageable p, final PagedResourcesAssembler<Commodity> assembler) 
		throws MerchantNotLoginException, ParseException {
		logger.info("进入接口>商品同步查询分页集合 . 参数：商家ID【{}】",merchantId);
		searchParam.setMerchantId(merchantId);
		List<CommodityReceive> adverts = commodityService.findByUpTime(searchParam.getMerchantId(), searchParam, p);
		logger.info("结束接口>商品同步查询分页集合 ");
		return new ResponseEntity<>(new Message<>(CommodityResult.SUCCESS, adverts), HttpStatus.OK);
	}
	
	////////////////////pos端新版本接口////////////////////////
	
    @ApiOperation(value = "按文件导入商品", notes = "按文件导入商品", response = Message.class, httpMethod = "POST")
    @ApiResponses({
        @ApiResponse(code = 0, message = "操作成功", response = Message.class),
        @ApiResponse(code = 1, message = "操作失败"),
    })
    @ResponseBody
    @PostMapping(value = "/importCommodity", consumes = "multipart/form-data")
    public ResponseEntity<Message<?>> importCommodityByFile(@RequestParam("merchantId") Long merchantId, @RequestParam("shopId")Long shopId, 
    		@RequestParam("excelFile") MultipartFile excelFile) throws Exception {
    	logger.info("进入接口>按文件导入商品 . 参数：商家ID【{}】, 店铺ID【{}】",merchantId, shopId);
    	this.commodityService.importCommodityByFile(merchantId, shopId, excelFile);
    	logger.info("结束接口>按文件导入商品 ");
        return new ResponseEntity<>(new Message<>(CommodityResult.SUCCESS), HttpStatus.OK);
    }
	
	@ApiOperation(value="商品信息列表(新)", notes="已登录商家查询商品列表(新)", response = Message.class)
    @ApiImplicitParams({
    	@ApiImplicitParam(name = "merchantId", value = "商家ID", required = true,paramType = "query", dataType = "Long"),
    	@ApiImplicitParam(name = "shopId", value = "店铺ID", required = true,paramType = "query", dataType = "Long"),
    	@ApiImplicitParam(name = "page", value = "第几页", required = false, dataType = "int", paramType = "query", defaultValue = "0"),
    	@ApiImplicitParam(name = "size", value = "每页行数", required = false, dataType = "int", paramType = "query", defaultValue = "20"),
    	@ApiImplicitParam(name = "sort", value = "排序规则", required = false, dataType = "String", paramType = "query", defaultValue = "createDate,asc")
    })
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 19,message = "商家未登录!"),
			@ApiResponse(code = 18,message ="时间格式错误!")
	})
	@RequestMapping(value = "/v2/list", method = RequestMethod.GET)
	public ResponseEntity<?> lists(Principal principal,Long merchantId,SearchParam searchParam, Pageable p, final PagedResourcesAssembler<CommodityDto> assembler) 
		throws MerchantNotLoginException, ParseException, CategoryNotFindException {
		logger.info("进入接口>商品信息列表(商家库) . 参数：商家ID【{}】",merchantId);
		searchParam.setMerchantId(merchantId);
		Page<CommodityDto> adverts = commodityNewService.find(searchParam.getMerchantId(), searchParam, p);
		PagedResources<Resource<CommodityDto>>  sc = assembler.toResource(adverts);
		logger.info("结束接口>商品信息列表(商家库) ");
		return new ResponseEntity<>(new Message<>(CommodityResult.SUCCESS, sc), HttpStatus.OK);
	}
	
	@ApiOperation(value="根据商品编号查询商品信息", notes="已登录商家查询编号商品信息",response = Message.class)
	@ApiImplicitParams({
		@ApiImplicitParam(name = "merchantId", value = "商家ID", required = true,paramType = "query", dataType = "Long"),
		@ApiImplicitParam(name = "id", value = "商品ID", required = true,paramType = "path", dataType = "Long",defaultValue = "1084")
	})
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 5,message = "商品信息不存在!"),
			@ApiResponse(code = 8,message = "参数不正确！")
	})
	@RequestMapping(value = "/v2/getById/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getCommodityById(Principal principal,Long merchantId,Long shopId,@PathVariable Long id){
		logger.info("进入接口>根据商品编号查询商品信息(商家库) . 参数：商家ID【{}】, 店铺ID【{}】, 商品ID【{}】", merchantId, shopId, id);
		Message<CommodityDtoShow> message = commodityNewService.findOne(merchantId, id, shopId);
		logger.info("结束接口>根据商品编号查询商品信息(商家库) . 返回 商品详情【{}】", message.getData());
		return ResponseEntity.ok(message);
	}
	
	@ApiOperation(value="添加商品信息(商家库)", notes="添加商品信息(商家库)",response = Message.class)
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 5,message = "商品信息不存在!"),
			@ApiResponse(code = 8,message = "参数不正确！")
	})
	@RequestMapping(value = "/v2/add", method = RequestMethod.POST)
	public ResponseEntity<?> add(@Valid @RequestBody CommodityAdds commodityAdds,BindingResult bindingResult) throws ParamCheckException, IllegalArgumentException, ClassNotFoundException, IllegalAccessException {
		logger.info("进入接口>添加商品信息(商家库) . 参数：商家添加商品信息【{}】", commodityAdds);
		if (bindingResult.hasErrors()) {
			logger.debug("添加商品信息(商家库) 参数格式不正确");
			throw new ParamCheckException(bindingResult);
		}
		Message<?> message = commodityNewService.add(commodityAdds);
		logger.info("结束接口>添加商品信息(商家库) . 返回 商品详情【{}】");
		return ResponseEntity.ok(message);
	}
	
	@ApiOperation(value="修改商品信息(商家库)", notes="修改商品信息(商家库)",response = Message.class)
	@ApiImplicitParam(name = "id", value = "商品ID", required = true,paramType = "path", dataType = "Long",defaultValue = "1084")
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 5,message = "商品信息不存在!"),
			@ApiResponse(code = 8,message = "参数不正确！")
	})
	@RequestMapping(value = "/v2/update/{id}", method = RequestMethod.PATCH)
	public ResponseEntity<?> add(@PathVariable Long id, @Valid @RequestBody CommodityModifys commodityModifys,BindingResult bindingResult) throws ParamCheckException, IllegalArgumentException, ClassNotFoundException, IllegalAccessException {
		logger.info("进入接口>修改商品信息(商家库) . 参数：商品id【{}】,　商家修改商品信息【{}】", id, commodityModifys);
		if (bindingResult.hasErrors()) {
			logger.debug("修改商品信息(商家库) 参数格式不正确");
			throw new ParamCheckException(bindingResult);
		}
		Message<?> message = commodityNewService.update(id, commodityModifys);
		logger.info("结束接口>修改商品信息(商家库) . 返回 商品详情【{}】");
		return ResponseEntity.ok(message);
	}
	
	@ApiOperation(value="删除商品信息", notes="删除商品信息",response = Message.class)
	@ApiResponses(value = {
			@ApiResponse(code = 0,message = "操作成功！"),
			@ApiResponse(code = 1,message = "操作失败！"),
			@ApiResponse(code = 5,message = "商品信息不存在!"),
			@ApiResponse(code = 12,message = "商品仍有库存!"),
			@ApiResponse(code = 13,message = "商品存在拆分商品!")
	})
	@RequestMapping(value = "/v2", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@RequestBody List<Long> ids) throws Exception {
		logger.info("进入接口>删除商品信息 . 参数：商品ID集合【{}】",ids);
		Message<?> message = commodityNewService.delete(ids);
		logger.info("结束接口>删除商品信息 ");
		return ResponseEntity.ok(message);
	}
	
	@ApiOperation(value="拆分测试", notes="拆分测试",response = Message.class)
	@GetMapping(value = "/v2/sst")
	public void sss() throws CommodityNotFindException {
		splitService.slpit(640439L, 182L, 13D, 1);
	}
	
	@ApiOperation(value="商品编号查询商品名称", notes="商品编号查询商品名称",response = Message.class)
	@ApiResponses(value = {
			@ApiResponse(code = 0,message = "操作成功！"),
			@ApiResponse(code = 1,message = "操作失败！")
	})
	@RequestMapping(value = "/v2/getName", method = RequestMethod.GET)
	public ResponseEntity<?> getName(Long id) throws Exception {
		logger.info("进入接口>商品编号查询商品名称 . 参数：商品ID集合【{}】", id);
		Message<?> message = commodityNewService.getCommodityName(id);
		logger.info("结束接口>商品编号查询商品名称 . 返回　商品名称【{}】", message.getData());
		return ResponseEntity.ok(message);
	}
}