package com.wetool.controller;

import java.security.Principal;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.hateoas.EntityLinks;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.wetool.entity.PublicCategory;
import com.wetool.exception.DataRelationException;
import com.wetool.exception.NodeRelationException;
import com.wetool.exception.ParamCheckException;
import com.wetool.model.CategoryTree;
import com.wetool.model.Message;
import com.wetool.model.Message.Result;
import com.wetool.service.commodity.PublicCategoryService;
import com.wetool.model.PublicCategoryAdd;
import com.wetool.model.PublicCategoryModify;

/**
 * 商品分类（公共库）管理控制器
 * 
 * @author zhangjie
 */
@RestController
@RequestMapping("/public/category/")
@RefreshScope
public class PublicCategoryController {

	@Autowired
	private PublicCategoryService publicCategoryService;
	
	@Autowired
	private EntityLinks entityLinks;

	@GetMapping("/tree")
	public ResponseEntity<?> getTree(Principal principal,Long categoryId) {
		if (categoryId == null) {
			categoryId=1L;
		}
		List<CategoryTree> tree = publicCategoryService.getTree(categoryId);
		return new ResponseEntity<Object>(new Message<>(Result.SUCCESS, tree), HttpStatus.CREATED); 
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> create(Principal principal, @Valid @RequestBody PublicCategoryAdd publicCategory,
			BindingResult bindingResult) throws Exception {
		if (bindingResult == null) {
			throw new ParamCheckException(bindingResult);
		}
		// User user =
		// userService.findByUsername(principal.getName()).orElseThrow(AccessDeniedException::new);
		publicCategoryService.add(publicCategory);
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setLocation(entityLinks.linkForSingleResource(PublicCategory.class, publicCategory).toUri());
		return new ResponseEntity<Object>(new Message<>(Result.SUCCESS), responseHeaders, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{categoryId}", method = RequestMethod.PATCH)
	public ResponseEntity<?> update(@PathVariable Long categoryId, @RequestBody PublicCategoryModify publicCategory)
			throws Exception {

		publicCategoryService.update(categoryId, publicCategory);
		return new ResponseEntity<Object>(new Message<>(Result.SUCCESS), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable Long id) throws NodeRelationException, DataRelationException {
		publicCategoryService.delete(id);
		return new ResponseEntity<Object>(new Message<>(Result.SUCCESS), HttpStatus.OK);
	}
}