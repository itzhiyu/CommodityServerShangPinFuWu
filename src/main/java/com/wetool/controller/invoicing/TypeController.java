package com.wetool.controller.invoicing;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wetool.model.Message;
import com.wetool.model.Message.Result;
import com.wetool.model.invoicing.PlacingType;
import com.wetool.model.invoicing.StorageType;
import com.wetool.service.invoicing.TypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 描述：出库信息类型控制器
 * @author lixin
 */
@RestController
@RefreshScope
@RequestMapping("/types")
@Api(description = "出入库类型管理")
public class TypeController {
	private static final Logger logger = LogManager.getLogger(TypeController.class);

	@Autowired
	private TypeService placingTypeService;

	@ApiOperation(value="出库类型列表", notes="查询出库类型列表", response = Message.class)
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！")
	})
	@RequestMapping(value = "/placingType", method = RequestMethod.GET)
	public ResponseEntity<?> getPlacingType() {
		logger.info("进入接口>出库类型列表");
		List<PlacingType> placingTypes = placingTypeService.PlacingTypelist();
		logger.info("结束接口>出库类型列表. 参数：出库类型集合【{}】",placingTypes);
		return new ResponseEntity<>(new Message(Result.SUCCESS, placingTypes), HttpStatus.OK);
	}

	@ApiOperation(value="入库类型列表", notes="查询出库类型列表", response = Message.class)
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！")
	})
	@RequestMapping(value = "/storageType", method = RequestMethod.GET)
	public ResponseEntity<?> getStorageType() {
		logger.info("进入接口>入库类型列表");
		List<StorageType> storageTypes = placingTypeService.StorageTypelist();
		logger.info("进入接口>入库类型列表. 参数：入库类型集合【{}】",storageTypes);
		return new ResponseEntity<>(new Message(Result.SUCCESS, storageTypes), HttpStatus.OK);
	}
}
