package com.wetool.controller.invoicing;

import java.security.Principal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.wetool.entity.invoicing.InvoicingCommodity;
import com.wetool.exception.invoicing.ShopNotLoginException;
import com.wetool.model.Message;
import com.wetool.model.Message.Result;
import com.wetool.model.invoicing.InformationStatistics;
import com.wetool.model.invoicing.SearchParam;
import com.wetool.service.InvoicingCommodityService;
import com.wetool.service.invoicing.InvoicingCommodityNewService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 描述：商品控制器
 * @author lixin
 */
@RestController
@RequestMapping(value = "/invoicing/commodity")
@RefreshScope
@Api(description = "库存商品信息管理")
public class InvoicingCommodityController {
	private static final Logger logger = LogManager.getLogger(InvoicingCommodityController.class);
	
	@Autowired
	private InvoicingCommodityService invoicingCommodityService;
	
	@Autowired
	private InvoicingCommodityNewService invoicingCommodityNewService;
	
	@ApiOperation(value="商品库存信息列表", notes="查询已登录店铺的商品档案信息列表", response = Message.class)
	@ApiImplicitParam(name = "shopId", value = "店铺id", required = true, dataType = "Long", paramType = "query", defaultValue = "1")
    @ApiImplicitParams({
    	@ApiImplicitParam(name = "page", value = "第几页", required = false, dataType = "int", paramType = "query", defaultValue = "0"),
    	@ApiImplicitParam(name = "size", value = "每页行数", required = false, dataType = "int", paramType = "query", defaultValue = "20"),
    	@ApiImplicitParam(name = "sort", value = "排序规则", required = false, dataType = "String", paramType = "query", defaultValue = "createDate,asc")
    })
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 5, message = "商品不存在！"),
			@ApiResponse(code = 14,message =  "店家未登录！")
	})
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseEntity<?> find(Principal principal, SearchParam searchParam, Pageable p,Long shopId,
			final PagedResourcesAssembler<InvoicingCommodity> assembler) throws Exception {
		logger.info("进入接口>商品库存信息列表 . 参数：店铺ID【{}】,查询参数【{}】",shopId,searchParam);
		searchParam.setShopId(shopId);//登录店铺ID
		Page<InvoicingCommodity> page = invoicingCommodityService.find(searchParam, p);
		logger.info("结束接口>商品库存信息列表");
		return new ResponseEntity<>(new Message(Result.SUCCESS, assembler.toResource(page)), HttpStatus.OK);
	}

	@ApiOperation(value="商品库存汇总信息", notes="查询已登录店铺的商品库存汇总信息", response = Message.class)
	@ApiImplicitParam(name = "shopId", value = "店铺id", required = true, dataType = "Long", paramType = "query", defaultValue = "1")
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 5, message = "商品不存在！"),
			@ApiResponse(code = 14,message =  "店家未登录！")
	})
	@RequestMapping(value = "/getSummary", method = RequestMethod.GET)
	public ResponseEntity<?> getInformationStatistics(Long shopId){
		logger.info("进入接口>商品库存汇总信息 . 参数：店铺ID【{}】",shopId);
		InformationStatistics is = invoicingCommodityService.getInformationStatistics(shopId);
		logger.info("结束接口>商品库存汇总信息 ");
		return new ResponseEntity<>(new Message(Result.SUCCESS,is), HttpStatus.OK);
	}

	@ApiOperation(value="删除出库信息", notes="已登录店铺的删除出库信息", response = Message.class)
	@ApiImplicitParams({
		@ApiImplicitParam(name = "shopId", value = "店铺id", required = true, dataType = "Long", paramType = "query", defaultValue = "1"),
		@ApiImplicitParam(name = "id", value = "商品id", required = true, dataType = "Long", paramType = "path", defaultValue = "1")
		})
	@ApiResponses(value = {
			@ApiResponse(code = 0, message = "操作成功！"),
			@ApiResponse(code = 1, message = "操作失败！"),
			@ApiResponse(code = 5, message = "商品不存在！"),
			//@ApiResponse(code = 15, message = "商品仍有库存!"),
			//@ApiResponse(code = 16, message = "商品下有拆分商品！")
	})
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(Principal principal,@PathVariable("id") Long id,Long shopId) throws Exception {
		logger.info("进入接口>删除出库信息. 参数：店铺ID【{}】，库存商品ID【{}】",shopId,id);
		if (shopId == null) {
			throw new ShopNotLoginException();
		}
		invoicingCommodityService.delete(id);
		logger.info("结束接口>删除出库信息");
		return new ResponseEntity<>(new Message(Result.SUCCESS), HttpStatus.OK);
	}
	
	
}
