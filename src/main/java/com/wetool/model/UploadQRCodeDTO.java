package com.wetool.model;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by zhanbin on 6/30/17.
 *
 */
@Data()
public class UploadQRCodeDTO implements Serializable {

    private static final long serialVersionUID = 2290260362115038903L;
    private String timestamp;
    private String uploadKey;
    private String SN;
    private String modType;
    private String file;

}
