package com.wetool.model;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "PLU热键修改对象", description = "")
public class PLUModelModify implements Serializable {
	private static final long serialVersionUID = 8927885829682551347L;
	
	@ApiModelProperty(value = "热键",required = true, example = "1", position = 0)
	private String hotKey;
	
	@ApiModelProperty(value = "商品ID",required = true, example = "1639", position = 0)
	private Long commodityId;
	
	@ApiModelProperty(value = "店铺ID",required = true, example = "1", position = 0)
	private Long shopId;
}
