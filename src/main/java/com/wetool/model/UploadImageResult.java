package com.wetool.model;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by zhanbin on 6/30/17.
 * 二维码扫码上传结果DTO
 */
@Data()
public class UploadImageResult implements Serializable {

    private static final long serialVersionUID = 2286786532638561031L;
    private String timestamp;

    private String SN;

    /**
     * 文件名
     */
    private String uploadKey;

    private String modType;

}
