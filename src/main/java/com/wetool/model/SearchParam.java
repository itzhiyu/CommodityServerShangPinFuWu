package com.wetool.model;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 商品查询条件模型
 * @author lixin
 */
@Data
@ApiModel(value = "查询参数对象", description = "")
public class SearchParam {

	@ApiModelProperty(value = "查询关键字（商品名称 / 商品条码/拼音/拼音首字母)", required = false, example = "690",position = 0)  
	private String key;

	@ApiModelProperty(value = "商品分类ID ", required = false, example = "1",position = 1)  
	private Long categoryId; // 商品分类ID
	
	@ApiModelProperty(value = "商品类型 1条码商品，2称重商品 3快录商品 ", required = false, example = "1",position = 2) 
	private Long type;

	@ApiModelProperty(hidden=true)
	private Date from; // 起始时间

	@ApiModelProperty(hidden=true)
	private Date to; // 结尾时间
	
	@ApiModelProperty(hidden=true)
	private Boolean isDeleted;
	
	@ApiModelProperty(value = "商品类型 1条码商品，2称重商品 3快录商品 ", required = false, example = "1",position = 3)
	private Long shopId;
	
	//@ApiModelProperty(hidden=true)
	private String updateTime;//长链接同步查询条件
	
	@ApiModelProperty(hidden=true)
	private Long merchantId;	//商家ID
}