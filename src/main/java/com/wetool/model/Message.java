/* 
 * Copyright (c) 2017, S.F. Express Inc. All rights reserved.
 */
package com.wetool.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;

/**
 * 描述：
 * 
 * @author zhangjie
 * @since 1.0
 */
@Data
@ApiModel(description = "响应对象")
public class Message<T> implements Serializable {
	private static final long serialVersionUID = 2953946557905707053L;

	@ApiModelProperty(value = "返回码", required = true)
	private int code;
	
	@ApiModelProperty(value = "返回消息", required = true)
	private String message;
	
	@ApiModelProperty(value = "返回数据", required = true)
	@JsonInclude(Include.NON_NULL)
	private T data;

	public Message(Result result) {
		code = result.getCode();
		message = result.getMessage();
	}

	public Message(Result result, String msg) {
		code = result.getCode();
		message = msg;
	}

	public Message(Result result, T data) {
		code = result.getCode();
		message = result.getMessage();
		this.data = data;
	}

	@Getter
	public enum Result {
		SUCCESS(0, "操作成功！"), 
		ERROR(1, "操作失败！"), 
		ERR_DATA_RELATION(2, "删除失败，该目录下存在商品数据！"), 
		ERR_NODE_RELATION(3, "删除失败，该目录存在子节点！"), 
		ERR_CHANGE_ISLEAF(4, "禁止修改目录类型！"), 
		ERR_NOTFIND_COMMODITY(160103, "商品信息不存在!"), 
		ERR_PARAMCHECK(6, "参数格式错误!"), 
		ERR_CLASS_NOTFIND(7, "类找不到！"), 
		ERR_ILLEGAL_ARGUMENT(8, "参数不正确!"), 
		ERR_ILLEGAL_ACCESS(9, "无法访问!"), 
		ERR_FORMAT(10, "上传图片格式错误!"),
		ERR_INVENTORY_SURPLUS(11,"商品仍有库存"),
		ERR_MERCHANT_NOT_LOGIN(160108, "商家未登录!"),
		ERR_SPLITRULE_EXISTED(13, "商品存在拆分商品!"),
		ERR_ADD_COMMODITY(14,"商品添加失败！"),
		ERR_COMMODITY_UPDATE(15,"商品修改信息失败！"),
		ERR_BARCODE_REPEAT(16,"商品已存在商品档案或待完善商品中！"),
		ERR_BEPERFECTED_NOT_FIND(17,"未找到快录商品信息！"),
		ERR_PARSE(18,"时间格式错误！"),
		ERR_CATEGORY_NOTFIND(20,"商品分类找不到异常"),
		ERR_PLU_SAVE(21,"PLU获取失败"),
		ERR_CATEGORY_NAME_REPETITION(22,"商品分类名称重复"),
		CATEGORY_IS_LEAF(23,"商品分类目录下，不能添加商品！"),
		ERR_BARCODE_INDEX_OF(24,"称重商品条码长度超过４位"),
		ERR_INSUFFICIENT_STOCK(25, "库存不足！"),
		ERR_ADDFAILED_STORAGE(26,"添加入口单失败!"), 
		ERR_ADDFAILED_PLACING(27,"添加出库单失败！"),
		ERR_NOTFIND_PLACING(28, "未找到出库记录!"), 
		ERR_NOTFIND_STORAGE(29,"未找到入库记录!"), 
		ERR_ADD_PLACING(30, "添加出库信息失败!"), 
		ERR_ADD_STORAGE(31,"添加入库信息失败!"), 
		ERR_ADD_SUPPLIER(32, "添加供应商失败!"), 
		ERR_PARAM_CHECK(33, "请求参数格式错误！"),
		ERR_BILL(34, "单据已提交！"),
		ERR_UPDATE_STORAGE(35, "修改入库信息失败！"),
		ERR_SHOP_NOT_LOGIN(36, "店家未登录！"),
		ER_SPLIT_COMMODITY(37,"商品下有拆分商品！"),
		ERR_UPDATE_PLACING(38,"出库信息修改失败！"),
		ERR_SUPPLIER_NOT_FIND(39, "未找到供应商信息!"),
		ERR_DOCUMENTS_NOT_FIND(40,"无出入库信息!"),
		ERR_DELETE_FAILED(41,"删除失败！");

		private final int code;

		private final String message;

		Result(int code, String message) {
			this.code = code;
			this.message = message;
		}
	}
}