package com.wetool.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;
import com.wetool.entity.Resource;
import com.wetool.entity.SplitCommodityRule;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 商品实体对象（商家库）
 * 
 * @author lixin
 */
@Data
@ApiModel(value = "新增商品（商家库）参数对象", description = "")
public class CommodityAdd implements java.io.Serializable {
	private static final long serialVersionUID = -198546246502271343L;
	
	@ApiModelProperty(hidden =true)
	private Long id;
	
	// @NotEmpty(message = "{commodity.merchant.null}")
	@ApiModelProperty(value = "商家ID", required = true, example = "1", position = 0)
	private Long merchantId; // 商家ID
	
	@ApiModelProperty(hidden = true)
	private Long shopId; //店铺Id
	
	@NotEmpty(message = "{commodity.name.null}")
	@Size(min = 2, max = 20, message = "{commodity.name.min}")
	@ApiModelProperty(value = "商品名称(必填)", required = true, example = "测试商品-01", position = 2)
	private String name; // 商品名称

	@ApiModelProperty(value = "商品分类标识(必填)", required = true, example = "21", position = 3)
	private Long categoryId; // 商品分类标识

	@NotEmpty(message = "{commodity.barcode.null}")
	@ApiModelProperty(value = "商品条形码(必填)", required = true, example = "6900804677719", position = 4)
	private String barcode; // 商品条形码

	// @NotEmpty(message = "{commodity.type.null}")
	@ApiModelProperty(value = "商品类型 1:正规商品 1:自编码; 2:称重商品(必填)", required = true, example = "1", position = 5)
	private Long type; // 商品类型 0:正规商品 1:自编码; 3:称重商品

	// @NotEmpty(message = "{commodity.specification.null}")
	@ApiModelProperty(value = "商品规格(可为空)", required = false, example = "105g", position = 6)
	private String specification; // 商品规格

	// @NotEmpty(message = "{commodity.unit.null}")
	@ApiModelProperty(value = "商品单位(可为空)", required = false, example = "克", position = 7)
	private String unit; // 商品单位

	@ApiModelProperty(value = "是否可拆分(必填)", required = true, example = "true", position = 8)
	private boolean isDetachable; // 是否可拆分

	@ApiModelProperty(value = "描述(可为空)", required = false, example = "这是一个测试商品", position = 9)
	private String description; // 描述

	@Range(min= 0, max = 999999,message = "{commodity.sellPrice.ltZero}")
	@ApiModelProperty(value = "销售价格(可为空)", required = false, example = "1.0",position = 10)
	private BigDecimal sellingPrice; // 销售价格

	@ApiModelProperty(value = "进货价格(可为空)", required = false, example = "1.5",position = 11)
	@Range(min= 0, max = 999999,message = "{commodity.sellPrice.ltZero}") 
	private BigDecimal buyingPrice; // 进货价格

	@ApiModelProperty(hidden = true)
	// @NotEmpty(message = "{commodity.createTime.null}")
	private Date createTime = new Date(); // 创建时间
	
	@ApiModelProperty(hidden = true)
	// @NotEmpty(message = "{commodity.createTime.null}")
	private Date updateTime = new Date(); // 修改时间

	@ApiModelProperty(value = "资源列表(可为空)", required = false, position = 12)
	private Resource resource;// 资源列表

	@ApiModelProperty(value = "拆分规则(可为空)", required = false, position = 13)
	private SplitCommodityRule splitRule; // 拆分规则

	@ApiModelProperty(value = "商品预警数", required = false, position = 14)
	private Double warningNumber; // 商品预警数
	
	@ApiModelProperty(value = "商家商品库存数", required = false, position = 15)
	private Double inventoryQuantity; //商家商品库存数

	
	public void setSellingPrice(BigDecimal sellingPrice) {
		if (sellingPrice != null) {
			this.sellingPrice = sellingPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
		}
	}
	
	public void setBuyingPrice(BigDecimal buyingPrice) {
		if (buyingPrice != null) {
			this.buyingPrice = buyingPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
		}
	}
}