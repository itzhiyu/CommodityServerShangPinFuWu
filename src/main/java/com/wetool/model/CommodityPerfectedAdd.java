package com.wetool.model;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "快录商品添加参数对象")
public class CommodityPerfectedAdd {

	@ApiModelProperty(value = "商品条码", required = true, example = "6901231231232", position = 0)
	private String barcode; //商品条码
	
	@ApiModelProperty(value = "商品名称", required = true, example = "测试商品-01", position = 1)
	private String name; //商品名称
	
	@ApiModelProperty(value = "商品售价", required = true, example = "11", position = 2)
	private BigDecimal sellingPrice;//商品售价
	
	@ApiModelProperty(value = "商家ID", required = true, example = "1", position = 3)
	private Long merchantId; // 商家ID
	
}
