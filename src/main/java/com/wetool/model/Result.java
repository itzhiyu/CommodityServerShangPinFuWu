package com.wetool.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Result implements Serializable {
    private static final long serialVersionUID = -3423739241260562187L;
    private final int code;
    private final String message;
    private String uploadId;
    private String uploadKey;
    private String url;
}