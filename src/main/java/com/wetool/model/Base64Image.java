package com.wetool.model;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by zhanbin on 7/1/17.
 * Base64Image DTO
 */
@Data
public class Base64Image implements Serializable {

    private static final long serialVersionUID = 126171733862610862L;

    /**
     * Base64 file content
     */
    private String file;

    private String modType;

    private String uploadKey;

    public Base64Image() {

    }

    public Base64Image(String file, String modType, String uploadKey) {
        this.file = file;
        this.modType = modType;
        this.uploadKey = uploadKey;
    }
}