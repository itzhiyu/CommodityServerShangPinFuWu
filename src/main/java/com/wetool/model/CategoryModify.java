package com.wetool.model;

import org.hibernate.validator.constraints.NotEmpty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 商品类别
 * 
 * @author lixin
 */
@Data
@ApiModel(value = "修改商品类别（商家库）参数对象", description = "")
public class CategoryModify{

	@ApiModelProperty(value = "分类ID",required = true, example = "1", position = 0)
	private Long id;
	
	@ApiModelProperty(value = "商家ID", required = true, example = "1", position = 1)
	private Long merchantId; // 商家ID
	
	@NotEmpty(message = "{category.name.null}")
	@ApiModelProperty(value = "目录名称(必填)", required = true, example = "test-02", position = 2)
	private String categoryName; // 目录名称

	@ApiModelProperty(value = "级别(必填)", required = true, example = "2", position = 3)
	private Integer categoryLevel; // 级别

	@NotEmpty(message = "{category.orderNum.null}")
	@ApiModelProperty(value = "排序编号(必填)", required = false, example = "5", position = 4)
	private String orderNum; // 排序编号

	@ApiModelProperty(value = "是否为叶子节点(必填)", required = true, example = "true", position = 5)
	private boolean isLeaf; // 是否为叶子节点
	
	@ApiModelProperty(value = "类别的父目录(为NULL是父目录)", required = false, example = "11", position = 6)
	private Long parentId;

}