package com.wetool.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.wetool.entity.Resource;
import com.wetool.entity.SplitCommodityRule;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 商品实体对象（公共库）
 * 
 * @author lixin
 */
@Data
@ApiModel(value = "新增商品（公共库）参数对象", description = "")
public class PublicCommodityAdd implements java.io.Serializable {
	private static final long serialVersionUID = 4744891820482977460L;

	private Long id;

	@NotEmpty(message = "{commodity.name.null}")
	@Size(min = 2, max = 20, message = "{commodity.name.min}")
	private String name; // 商品名称

	@NotEmpty(message = "{commodity.category.null}")
	private Integer categoryId; // 商品分类标识

	@NotEmpty(message = "{commodity.barcode.null}")
	private String barcode; // 商品条形码

	@NotEmpty(message = "{commodity.specification.null}")
	private String specification; // 商品规格

	@NotEmpty(message = "{commodity.unit.null}")
	private String unit; // 商品单位

	@NotEmpty(message = "{commodity.price.null}")
	private BigDecimal commodityPrice;// 商品价格

	private Long companyId;// 企业Id

	private String brandName; // 品牌名称

	private boolean addIsDetachable; // 是否可拆分(is开头后台无法接收jsonboolean)

	private String description; // 描述

	private Boolean isGs1; // 是否为正规条码商品

	private String country; // 商品所属国家

	private Timestamp createTime; // 创建时间

	private List<Resource> resources;// 资源列表

	private SplitCommodityRule splitRule; // 拆分规则
}