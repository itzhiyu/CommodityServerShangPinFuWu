package com.wetool.model;

import lombok.Data;

@Data
public class PluModel {
	
	private String PLU;
	
	private String name;
	
	private Long commodityId;
	
	private Integer hotKey;
	
	private String price;
}
