package com.wetool.model;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 快录商品
 * @author lixin
 */
@Data
public class BePerfectedCommodity {
	
	private Long id;
	
	private Long merchantId; // 商家ID
	
	private String barcode; //商品条码
	
	private String name; //商品名称
	
	private BigDecimal sellingPrice;//商品售价
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd") 
	private Date createDate;//创建时间

	public BePerfectedCommodity(){}
	public BePerfectedCommodity(Long id, Long merchantId, String barcode, String name, BigDecimal sellingPrice,
			Date createDate) {
		this.id = id;
		this.merchantId = merchantId;
		this.barcode = barcode;
		this.name = name;
		this.sellingPrice = sellingPrice;
		this.createDate = createDate;
	}
	
}
