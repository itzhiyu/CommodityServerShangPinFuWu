package com.wetool.model;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 商品类别
 * 
 * @author lixin
 */
@Data
@ApiModel(value = "新增商品类别（公共库）参数对象", description = "")
public class PublicCategoryAdd implements java.io.Serializable {
	private static final long serialVersionUID = -1679572259923023796L;

	@NotEmpty(message = "{category.name.null}")
	@Size(min = 2, max = 20, message = "{category.name.min}")
	private String categoryName; // 目录名称

	@NotEmpty(message = "{category.level.null}")
	private int categoryLevel; // 级别

	@NotEmpty(message = "{category.orderNum.null}")
	private String orderNum; // 排序编号

	@NotEmpty(message = "{category.isLeaf.null}")
	private boolean isLeaf; // 是否为叶子节点

	@NotEmpty(message = "{category.parent.null}")
	private PublicCategoryAdd parent;

}