package com.wetool.model.invoicing;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.wetool.Constant;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 描述：供应商修改参数对象model
 * @author lixin
 */
@Data
@ApiModel(value = "供应商修改参数对象")
public class SupplierModify implements Serializable{
	private static final long serialVersionUID = -1227441399515884145L;

	@ApiModelProperty(value = "供应商Id",required = true, example = "1", position = 4)
	private Long id;
	
	/** 店铺id */
	@ApiModelProperty(value = "店铺id", required = true, example = "1",position = 0)
	private Long shopId; 

	/** 供应商名称 */
	@NotNull(message = "{supplier.name.null}")
	@Size(min = 2, max = 20, message = "{placing.name.min}")
	@ApiModelProperty(value = "供应商名称(必填项)", required = true, example = "text-01",position = 1)
	private String supplierName;

	/** 供应商联系人 */
	@NotNull(message = "{supplier.supplierContacts.null}")
	@ApiModelProperty(value = "供应商联系人(必填项)", required = true, example = "text",position = 2)
	private String supplierContacts;

	/** 供应商联系电话 */
	@NotNull(message = "{supplier.supplierTel.null}")
	@Pattern(regexp = Constant.REG_MOBILE, message = "{mobile.format}")
	@ApiModelProperty(value = "供应商联系电话(必填项)", required = true, example = "13000001111",position = 3)
	private String supplierTel;

	/** 供应商地址 */
	@NotNull(message = "{supplier.address.null}")
	@ApiModelProperty(value = "供应商地址(必填项)", required = true, example = "广东省深圳市宝安区航空路靠近威士顿花园酒店(新机场航站楼店)",position = 4)
	private String supplierAddress;
	
	@ApiModelProperty(hidden = true)
	private Integer count;

	/** 供应商备注 */
	@ApiModelProperty(value = "供应商备注", required = false, example = "xxxx",position = 5)
	private String remark;
}