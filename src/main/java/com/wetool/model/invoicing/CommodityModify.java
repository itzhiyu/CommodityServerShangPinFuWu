package com.wetool.model.invoicing;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class CommodityModify implements java.io.Serializable {
	private static final long serialVersionUID = -198546246502271343L;

	private Long id;

	private Long merchantId; // 商家ID

	private Long shopId; // 店铺Id

	private String name; // 商品名称

	private Long categoryId; // 商品分类标识

	private String barcode; // 商品条形码

	private Long type; // 商品类型 0:正规商品 1:自编码; 3:称重商品

	private String specification; // 商品规格

	private String unit; // 商品单位

	private boolean isDetachable; // 是否可拆分

	private String description; // 描述

	private BigDecimal sellingPrice; // 销售价格

	private BigDecimal buyingPrice; // 进货价格

	private Date updateDate; // 修改时间

	private SpiltCommodityModel splitRule; // 拆分展示

	private Double warningNumber; // 商品预警数

	private Double inventoryQuantity; // 商家商品库存数

}
