package com.wetool.model.invoicing;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 描述：修改入库商品参数对象module
 * @author lixin
 */
@Data
@ApiModel(value = "修改入库商品参数对象")
public class InventoryRecordCommodityModify implements Serializable{
	private static final long serialVersionUID = -2049540510210826441L;

	@ApiModelProperty(value = "修改入库商品参数对象ID" ,required = true , example = "7" , position = 0)
	private Long id;
	
    /** 采购单价 */
    @NotNull(message = "{storage.price.null}")
    @ApiModelProperty(value = "采购单价(必填项)", required = true, example = "5.0", position = 1)
    private BigDecimal price;
    
    /** 采购金额 */
    @NotNull(message = "{storage.price.null}")
    @ApiModelProperty(value = "采购金额(必填项)", required = true, example = "10.0", position = 2)
    private BigDecimal totalPrice;

    /** 商品数量 */
    @NotNull(message = "{storage.commodityNumber.null}")
    @ApiModelProperty(value = "商品数量(必填项)", required = true, example = "2.0", position = 3)
    private Double commodityNumber;

    /** 商品条码 */
    @NotNull(message = "{storage.barcode.null}")
    @ApiModelProperty(value = "商品条码", required = true, example = "6900077001211", position = 4)
    private String barcode;
    
    /** 商品名称 */
    @NotNull(message = "{storage.barcode.null}")
    @ApiModelProperty(value = "商品名称", required = true, example = "隆力奇蛇油膏", position = 5)
    private String name;
 
    /** 商品规格*/
    @ApiModelProperty(value = "商品规格", required = false, example = "80g", position = 6)
	private String specification; 

    /**商品单位*/
    @ApiModelProperty(value = "商品名称", required = false, example = "瓶", position = 7)
	private String unit;
    
    /** 多件销售商品 */
    @NotNull(message = "{storage.commodityId.null}")
    @ApiModelProperty(value = "商品ID(商家库或店铺)", required = false, example = "1162", position = 8)
    private Long commodityId;
    
    @ApiModelProperty(value = "备注",required = false ,example = "1111", position = 8)
	private String remark;
}
