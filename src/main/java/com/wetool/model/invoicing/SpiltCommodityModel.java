package com.wetool.model.invoicing;

import java.io.Serializable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 描述：拆分信息展示
 * @author lixin
 *
 */
@Data
public class SpiltCommodityModel implements Serializable{
	private static final long serialVersionUID = 6862709765542274085L;

	@ApiModelProperty(value = "拆分id(新增拆分为空，修改拆分填入ID)", required = false, example =  "3" ,position = 0)
	private Long id;//拆分id
	
	@ApiModelProperty(value = "商品Id", required = true, example = "1533", position = 1)
	private Long commodityId;
	
	@ApiModelProperty(value = "商品名称", required = false, example = "text-02", position = 2)
	private String name;
	
	@ApiModelProperty(value = "商品条形码", required = false, example = "6900804677719", position = 3)
	private String barcode;

	@ApiModelProperty(value = "拆分后商品数量", required = true, example =  "10" ,position = 4)
	private Double splitNumber;// 拆分数
}
