package com.wetool.model.invoicing;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;

/**
 * 描述：异常、信息返回
 * @author lixin
 */
@Data
@ApiModel(description = "响应对象")
public class Message implements Serializable{
	private static final long serialVersionUID = 5019072351312256682L;

	/** 返回码 */
	@ApiModelProperty(value = "返回码", required = true)
	private int code;
	
	/** 放回消息 */
	@ApiModelProperty(value = "返回消息", required = true)
	private String message;

	@ApiModelProperty(value = "返回数据", required = true)
	@JsonInclude(Include.NON_NULL)
	private Object data;

	public Message() {
	}

	public Message(Result result) {
		code = result.getCode();
		message = result.getMessage();
	}

	public Message(Result result, String msg) {
		code = result.getCode();
		message = msg;
	}

	public Message(Result result, Object data) {
		code = result.getCode();
		message = result.getMessage();
		this.data = data;
	}

	@Getter
	public enum Result {
		SUCCESS(0, "操作成功！"), 
		ERROR(1, "操作失败！"), 
		ERR_INSUFFICIENT_STOCK(2, "库存不足！"),
		ERR_ADDFAILED_STORAGE(3,"添加入口单失败!"), 
		ERR_NOTFIND_COMMODITY(5, "未找到商品信息！"),
		ERR_ADDFAILED_PLACING(4,"添加出库单失败！"),
		ERR_NOTFIND_PLACING(6, "未找到出库记录!"), 
		ERR_NOTFIND_STORAGE(7,"未找到入库记录!"), 
		ERR_ADD_PLACING(8, "添加出库信息失败!"), 
		ERR_ADD_STORAGE(9,"添加入库信息失败!"), 
		ERR_ADD_SUPPLIER(10, "添加供应商失败!"), 
		ERR_PARAM_CHECK(11, "请求参数格式错误！"),
		ERR_BILL(12, "单据已提交！"),
		ERR_UPDATE_STORAGE(13, "修改入库信息失败！"),
		ERR_SHOP_NOT_LOGIN(14, "店家未登录！"),
		ERR_INVENTORY_SURPLUS(15,"商品仍有库存!"),
		ER_SPLIT_COMMODITY(16,"商品下有拆分商品！"),
		ERR_UPDATE_PLACING(17,"出库信息修改失败！"),
		ERR_SUPPLIER_NOT_FIND(18, "未找到供应商信息!"),
		ERR_DOCUMENTS_NOT_FIND(19,"无出入库信息!"),
		ERR_DELETE_FAILED(20,"删除失败！");

		private final int code;

		private final String message;

		Result(int code, String message) {
			this.code = code;
			this.message = message;
		}
	}
}
