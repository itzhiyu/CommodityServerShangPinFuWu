package com.wetool.model.invoicing;

import lombok.Data;

/**
 * 描述：入库类型model
 * @author lixin
 */
@Data
public class StorageType {

    /** 对应编号 */
    private Long id;

    /** 入库类型 */
    private String typeName;

    /** 父节点 */
    private StorageType parent;

}
