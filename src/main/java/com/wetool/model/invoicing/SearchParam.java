package com.wetool.model.invoicing;

import java.io.Serializable;
import java.util.Date;

import com.wetool.model.enums.OutOfStorage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 描述：查询条件模型
 * @author lixin
 */
@Data
@ApiModel(value = "查询参数对象")
public class SearchParam implements Serializable{
	private static final long serialVersionUID = 5229674041605063494L;

	@ApiModelProperty(value = "查询关键字（商品名称 /商品条码)/出入库单号", required = false, example = "",position = 0)
    private String key; // 商品名称

	@ApiModelProperty(value = "商品分类类型", required = false, example = "84",position = 1)
    private Long categoryId; // 商品分类类型

	@ApiModelProperty(hidden = true)
    private Long shopId;// 店铺ID

	@ApiModelProperty(hidden =true)
    private Long parentId; // 类型父Id

	@ApiModelProperty(value = "(出入库)类型Id/(库存商品)商品分类类型Id", required = false, example = "3",position = 2)
    private Long typeId; // 类型Id

	@ApiModelProperty(hidden = true)
    private Date formDate; // 起始时间

	@ApiModelProperty(hidden = true)
    private Date toDate;// 结束时间

	@ApiModelProperty(hidden=true)
    private Integer status; // 出入库单状态

	@ApiModelProperty(value = "供应商ID", required = false, example = "1",position = 4)
    private Long supplierId; // 供应商ID
	
	/** 出入库 1 为出库 2为入库 */
	@ApiModelProperty(hidden=true) 
	private OutOfStorage outOfStorage;

	@ApiModelProperty(hidden=true)
	private Long commodityId;//商品ID
	
	@ApiModelProperty(hidden=true)
	private Long splitCommodityId; //拆分商品ID
	
	@ApiModelProperty(value = "是否展示快录商品", required = false, example = "false",position = 5)
	private Boolean flag;
}