package com.wetool.model.invoicing;

import lombok.Data;

/**
 * 描述：出库类型模型 
 * @author lixin
 * */
@Data
public class PlacingType {

    /** 主键 */
    private Long id;

    /** 出库类型名 */
    private String typeName;

//    /** 父级模块 */
//    private PlacingType parent;
}
