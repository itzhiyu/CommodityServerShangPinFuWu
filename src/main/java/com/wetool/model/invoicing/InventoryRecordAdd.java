package com.wetool.model.invoicing;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;

import com.wetool.model.enums.OutOfStorage;
import com.wetool.model.enums.Status;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 描述：添加入库参数对象model
 * @author lixin
 */
@Data
@ApiModel(value = "添加入库参数对象")
public class InventoryRecordAdd implements Serializable{
	private static final long serialVersionUID = 6169665928756132620L;

	/** 订单号 */
	//@NotNull(message = "{storage.order.null}")
	//@Size(min = 14, message = "{placing.order.min}")
	@ApiModelProperty(value = "入库订单号(必填项)", required = true, example = "UKF12322113312",position = 0)
	private String orderNumber;
	
	/** 初始订单号 */
	@ApiModelProperty(value = "初始订单号", required = false, example = "12312312313123",position = 1)
	private String initialOrderNumber;

	/** 入库类型 */
	//@NotNull(message = "{storage.type.null}")
	@ApiModelProperty(value = "采购入库 默认4",required = true, example = "4", position = 2)
	private Long type; 

	/** 多商品 */
	@NotNull(message = "{storage.commodity.null}")
	@ApiModelProperty(value = "入库商品(必填项)", required = true, position = 3)
	private List<InventoryRecordCommodityAdd> commoditys;

	/** 供应商 */
	@NotNull(message = "{storage.supplierId.null}")
	@ApiModelProperty(value = "供应商Id", required = true, example = "1", position = 4)
	private Long supplierId;

	/** 店铺ID */
	//@NotNull(message = "{storage.shopId.null}")
	@ApiModelProperty(value = "店铺Id",required = true, example = "1", position = 5)
	private Long shopId;

	/** 入库日期 */
	//@NotNull(message = "{storage.createDate.null}")
	@ApiModelProperty(value = "创建时间", required = true, example = "2017-06-12", position = 6)
	private Date createDate; // 创建时间
	
	@ApiModelProperty(value = "出入库类型 1 为出库 2为入库", required = true, example = "1", position = 6)
	private OutOfStorage outOfStorage; // 出入库 1 为出库 2为入库 
	
	/** status 0:单据未提交 1：单据已提交 */
	@ApiModelProperty(value = "入库单状态(0:单据未提交 1：单据已提交)(必填项)", required = true, example = "1", position = 7)
	private Status status;

	/** 调拨出库库商店 */
	@ApiModelProperty(hidden = true)
	private Long placingShop;

	/** 订单总价 */
	@ApiModelProperty(value = "订单总价(必填项)", required = true, example = "10.0", position = 8)
	private BigDecimal totalPrice;

	@ApiModelProperty(value = "备注",required = false ,example = "1111", position = 9)
	private String remark;
	
	public void setCreateDate(String createDate) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		this.createDate = sdf.parse(createDate);
	}
	
	public void setCreateDate(Date createDate) throws ParseException{
		this.createDate = createDate;
	}

	public InventoryRecordAdd() {}
	
	public InventoryRecordAdd(String orderNumber, String initialOrderNumber, Long type,
			List<InventoryRecordCommodityAdd> commoditys, Long supplierId, Long shopId, 
			OutOfStorage outOfStorage, Status status, Long placingShop, BigDecimal totalPrice, String remark) {
		super();
		this.orderNumber = orderNumber;
		this.initialOrderNumber = initialOrderNumber;
		this.type = type;
		this.commoditys = commoditys;
		this.supplierId = supplierId;
		this.shopId = shopId;
		this.outOfStorage = outOfStorage;
		this.status = status;
		this.placingShop = placingShop;
		this.totalPrice = totalPrice;
		this.remark = remark;
	}
	
	
	
}
