package com.wetool.model.invoicing;

import java.math.BigDecimal;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import lombok.Data;

@Data
public class InformationStatistics {
	
	private Double SUK;
	
	private Double inventoryQuantity;
	
	@NumberFormat(style=Style.CURRENCY)
	private String inventoryWeight;
	
	private String inventoryCost;
	
	private String advanceCost;
	
	public InformationStatistics(Double SUK,Double inventoryQuantity,BigDecimal inventoryWeight,BigDecimal inventoryCost,BigDecimal advanceCost) {
		this.advanceCost = advanceCost.setScale(2, BigDecimal.ROUND_HALF_UP).toString();
		this.inventoryCost = inventoryCost.setScale(2, BigDecimal.ROUND_HALF_UP).toString();
		this.inventoryWeight = inventoryWeight.setScale(2, BigDecimal.ROUND_HALF_UP).toString();
		this.inventoryQuantity=inventoryQuantity;
		this.SUK = SUK;
	}
}
