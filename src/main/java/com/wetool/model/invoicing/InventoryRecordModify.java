package com.wetool.model.invoicing;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;

import com.wetool.model.enums.Status;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 描述：修改入库参数对象model
 * @author lixin
 */
@Data
@ApiModel(value = "修改入库参数对象")
public class InventoryRecordModify implements Serializable{
	private static final long serialVersionUID = -2216201476621725753L;

	/** 订单号 */
	//@NotNull(message = "{storage.order.null}")
	//@Size(min = 14, message = "{placing.order.min}")
	@ApiModelProperty(value = "入库订单号(必填项)", required = true, example = "UKF12322113312",position = 1)
	private String orderNumber;
	
	/** 初始订单号 */
	@ApiModelProperty(value = "初始订单号", required = true, example = "12312312313123",position = 2)
	private String initialOrderNumber;

	/** 入库类型 */
	//@NotNull(message = "{storage.type.null}")
	@ApiModelProperty(value = "采购入库 默认4",required = true, example = "4", position = 4)
	private Long type;

	/** 多商品 */
	@NotNull(message = "{storage.commodity.null}")
	@ApiModelProperty(value = "入库商品(必填项)", required = true, position = 5)
	private List<InventoryRecordCommodityModify> commoditys;

	/** 供应商 */
	@NotNull(message = "{storage.supplierId.null}")
	@ApiModelProperty(value = "供应商Id(必填项)", required = true, example = "1", position = 6)
	private Long supplierId;

	/** 店铺ID */
	@NotNull(message = "{storage.shopId.null}")
	@ApiModelProperty(value = "店铺ID", required = true, example = "1", position = 7)
	private Long shopId;

	/** 入库日期 */
	//@NotNull(message = "{storage.createDate.null}")
	@ApiModelProperty(value = "创建时间", required = true, example = "2017-06-12", position = 8)
	private Date createDate; // 创建时间

	/** status 0:单据未提交 1：单据已提交 */
	@ApiModelProperty(value = "入库单状态(0:单据未提交 1：单据已提交)(必填项)", required = true, example = "1", position = 9)
	private Status status;

	/** 调拨出库库商店 */
	@ApiModelProperty(hidden = true)
	private Long placingShop;

	/** 订单总价 */
	@ApiModelProperty(value = "订单总价(必填项)", required = true, example = "10.0", position = 10)
	private BigDecimal totalPrice;

	@ApiModelProperty(hidden = true)
	private String typeName; //出入库类型名称
	
	@ApiModelProperty(value = "备注",required = false ,example = "1111", position = 11)
	private String remark;
	
	public void setCreateDate(String createDate) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		this.createDate = sdf.parse(createDate);
	}
	
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}

}
