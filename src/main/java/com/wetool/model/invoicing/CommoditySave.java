package com.wetool.model.invoicing;

import java.math.BigDecimal;
import java.util.Date;
import com.wetool.entity.SplitCommodityRule;

import lombok.Data;

@Data
public class CommoditySave {

	/** 主键 */
	private Long id;

	private Long type;//商品类型 1编码商品 2称重商品
	
	private Long commId;//商家库商品ID
	
	private String name; // 商品名称

	private Long categoryId; // 商品分类标识

	private String barcode; // 商品条形码

	private BigDecimal sellingPrice; // 销售价格

	private BigDecimal buyingPrice; // 进货价格
	
	private Integer status; //商品状态 0：下架 1：上架 (入库单添加商品时默认下架)
	
	private String specification; // 商品规格

	private String unit; // 商品单位

	private Date createDate; // 创建时间
	
	private Date updateDate; // 修改时间

	private Boolean isDeleted = false; // 删除标识

	private Boolean isDetachable; // 是否可拆分(is开头后台无法接收jsonboolean)

	private SplitCommodityRule splitRule; // 拆分规则
	
	private Double inventoryWarningQuantity; // 商品预警数

	private Double inventoryQuantity; //店铺商品库存数
}
