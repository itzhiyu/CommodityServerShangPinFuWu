package com.wetool.model.invoicing;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 描述：商品出入库信息展示
 * @author Lixin
 */
@Data
public class InvoicingRecord  implements Serializable{
	private static final long serialVersionUID = 6448730983957525502L;

	private Long type; // 出入类型
	
	private String typeName;// 出入类型名称

	private Double commodityNumber; // 出入库数量

	private Date createDate; // 创建时间

	private Integer incoming; // 0:入库 1:出库

	private Double balances; // 结存
	
}
