package com.wetool.model.invoicing;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 描述：供应商添加参数对象model
 * @author lixin
 */
@Data
@ApiModel(value = "供应商添加参数对象")
public class SupplierAdd implements Serializable{
	private static final long serialVersionUID = 7617985255928976510L;

	/** 店铺id */
	@ApiModelProperty(value = "店铺Id",required = true, example = "1", position = 0)
	private Long shopId;

	/** 供应商名称 */
	@NotNull(message = "{supplier.name.null}")
	@Size(min = 2, max = 20, message = "{placing.name.min}")
	@ApiModelProperty(value = "供应商名称(必填项)", required = true, example = "text-01",position = 1)
	private String supplierName;

	/** 供应商联系人 */
	@ApiModelProperty(value = "供应商联系人", required = false, example = "text",position = 2)
	private String supplierContacts;

	/** 供应商联系电话 */
	@ApiModelProperty(value = "供应商联系电话(必填项)", required = true, example = "13000001111",position = 3)
	private String supplierTel;

	/** 供应商地址 */
	@ApiModelProperty(value = "供应商地址", required = false, example = "广东省深圳市宝安区航空路靠近威士顿花园酒店(新机场航站楼店)",position = 4)
	private String supplierAddress;

	/** 供应商备注 */
	@ApiModelProperty(value = "供应商备注", required = false, example = "xxxx",position = 5)
	private String remark;
}