package com.wetool.model.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "添加商品（商家库）拆分规则参数对象", description = "")
public class SplitCommodityRuleAdd implements Serializable{

	private static final long serialVersionUID = 2325885231464908325L;

	@ApiModelProperty(value = "拆分后商品编号", required = false, example =  "1084" ,position = 0)
	private Long commodityId;// 拆分后商品编号

	@ApiModelProperty(value = "拆分数量", required = false, example =  "0.5" ,position = 0)
	private Double splitNumber;// 拆分数量
	
}
