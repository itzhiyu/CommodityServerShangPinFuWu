package com.wetool.model.dto;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import com.wetool.entity.Resource;
import com.wetool.entity.SplitCommodityRule;

import lombok.Data;

@Entity(name = "commodity")
@Data
public class CommodityDtoShow {

	@Id
	@GeneratedValue
	private Long id;
	
	private String name;
	
	private Long type;
	
	private String barcode;
	
	private String selfCoding;
	
	private Long categoryId;
	
	@Transient
	private String categoryName;//分类名称
	
	private String specification;
	
	private String unit;
	
	private BigDecimal sellingPrice;
	
	private BigDecimal buyingPrice; 
	
	private Boolean isDeleted;
	
	private Boolean isDetachable; 
	
	@Transient
	private Double inventoryNumber; //商家商品库存数
	
	@OneToOne(cascade = { CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name = "split_rule_id")
	private SplitCommodityRule splitRule; // 拆分规则
	
	@OneToMany(cascade = { CascadeType.MERGE ,CascadeType.PERSIST})
	@JoinColumn(name = "commodity_id")
	private List<Resource> resources;// 资源列表
	
	@Transient
	private Resource resource;// 展示资源列表
	
	private Double warningNumber; // 商品预警数
	
	@Transient
	private Double sellingCost;//销售成本
	
	@Transient
	private Double buyingCost;//库存成本
	
	@Transient
	private String supplierName;//供应商名称
	
	@Transient
	private Long supplierId;//供应商名称
	
	public Resource resource() {
		if (this.resources == null || this.resources.isEmpty()) {
			this.setResources(null);
			return null;
		}
		this.resource = this.resources.get(0);
		this.setResources(null);
		return this.resource;
	}

}
