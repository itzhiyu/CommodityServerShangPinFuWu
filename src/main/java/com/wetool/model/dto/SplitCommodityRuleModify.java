package com.wetool.model.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "修改商品（商家库）拆分规则参数对象", description = "")
public class SplitCommodityRuleModify implements Serializable{

	private static final long serialVersionUID = -1086413994731469047L;

	@ApiModelProperty(value = "拆分后商品编号", required = false, example =  "1084" ,position = 1)
	private Long commodityId;// 拆分后商品编号

	@ApiModelProperty(value = "拆分数量", required = false, example =  "0.5" ,position = 2)
	private Double splitNumber;// 拆分数量
}
