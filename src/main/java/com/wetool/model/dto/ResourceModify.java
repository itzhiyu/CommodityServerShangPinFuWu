package com.wetool.model.dto;

import java.io.Serializable;

import javax.persistence.Column;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wetool.serialize.FullPathDeserializer;
import com.wetool.serialize.FullPathSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "修改商品（商家库）资源参数对象", description = "")
public class ResourceModify implements Serializable{

	private static final long serialVersionUID = 2891553404776633555L;

	@ApiModelProperty(value = "资源路径", required = false, example =  "http://wetool.oss-cn-beijing.aliyuncs.com/ms.test.pro/ba85cb80-09e8-4a35-95f0-f1de79069877_w640_h394.jpeg" ,position = 0)
	@Column(name = "res_url", nullable = false, length = 255)
	private String resUrl;

	@JsonDeserialize(using = FullPathDeserializer.class)
	public void setResUrl(String resUrl) {
		this.resUrl = resUrl;
	}
	
	@JsonSerialize(using = FullPathSerializer.class)
	public String getResUrl() {
		return this.resUrl;
	}
}
