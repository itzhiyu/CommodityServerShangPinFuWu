package com.wetool.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.wetool.entity.Resource;
import com.wetool.entity.invoicing.InvoicingCommodity;
import com.wetool.utils.Arith;
import lombok.Data;

@Data
@Entity(name = "commodity")
public class CommodityDto {

	@Id
	@GeneratedValue
	private Long id;
	
	private String name;
	
	private Long merchantId;
	
	private Long categoryId; // 商品分类标识
	
	private String pinyin; // 商品名称拼音

	private String pinyinShorthand; // 商品名称拼音首字母简写
	
	private Boolean isDeleted;
	
	private Long type;
	
	private String barcode; // 商品条形码
	
	private String specification; // 商品规格
	
	private Date createDate;
	
	private String unit; // 商品单位
	
	private BigDecimal sellingPrice; // 销售价格

	private BigDecimal buyingPrice; // 进货价格
	
	@Transient
	private Double number;
	
	@OneToMany(cascade = { CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name = "commodity_id")
	private List<InvoicingCommodity> invoicingCommodity; // 库存详情

	@OneToMany(fetch = FetchType.EAGER,cascade = { CascadeType.MERGE ,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name = "commodity_id")
	private List<Resource> resources;// 资源列表
	
	@Transient
	private String picPath;// 展示资源列表
	
	/**
	 * 获取库存数量
	 * @return
	 */
	public Double number(Long shopId) {
		Double count = 0D;
		if (this.invoicingCommodity == null) {
			return count;
		}
		for (InvoicingCommodity ic : invoicingCommodity) {
			if (!shopId.equals(ic.getShopId())) {
				continue;
			}
			double countNumber =  (ic.getInventoryNumber() == null) ? 0D:ic.getInventoryNumber(); 
			count = Arith.add(countNumber,count);
			count = Arith.round(count, 3);
		}
		return count;
	}
	
	/**
	 * 获取图片资源路径
	 * @return
	 */
	public String path() {
		if (this.resources == null || this.resources.isEmpty()) {
			return null;
		}
		String url = this.resources.get(0).getResUrl();
		return "http://wetool.oss-cn-beijing.aliyuncs.com/"+url;
	}
}

