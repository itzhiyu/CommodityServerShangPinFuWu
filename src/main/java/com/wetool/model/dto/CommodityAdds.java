package com.wetool.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "新增商品（商家库）参数对象", description = "")
public class CommodityAdds implements Serializable {
	private static final long serialVersionUID = -6991390826836331066L;

	@NotNull(message = "{commodity.merchant.null}")
	@ApiModelProperty(value = "商家ID", required = true, example = "1", position = 0)
	private Long merchantId; // 商家ID

	@NotNull(message = "{commodity.shopId.null}")
	@ApiModelProperty()
	private Long shopId; // 店铺Id

	@NotEmpty(message = "{commodity.name.null}")
	@Size(min = 1, max = 20, message = "{commodity.name.min}")
	@ApiModelProperty(value = "商品名称(必填)", required = true, example = "测试商品-01", position = 2)
	private String name; // 商品名称

	@NotNull(message = "{commodity.category.null}")
	@ApiModelProperty(value = "商品分类标识(必填)", required = true, example = "21", position = 3)
	private Long categoryId; // 商品分类标识

	//@NotEmpty(message = "{commodity.barcode.null}")
	@ApiModelProperty(value = "商品条形码(必填)", required = true, example = "6900804677719", position = 4)
	private String barcode; // 商品条形码

	@ApiModelProperty(value = "自编码", required = true, example = "6900804677719", position = 5)
	private String selfCoding;
	
	@NotNull(message = "{commodity.type.null}")
	@ApiModelProperty(value = "商品类型 1:正规商品 1:自编码; 2:称重商品(必填)", required = true, example = "1", position = 6)
	private Long type; // 商品类型 0:正规商品 1:自编码; 3:称重商品

	@ApiModelProperty(value = "商品规格(可为空)", required = false, example = "105g", position = 7)
	private String specification; // 商品规格

	@ApiModelProperty(value = "商品单位(可为空)", required = false, example = "克", position = 8)
	private String unit; // 商品单位

	@ApiModelProperty(value = "是否可拆分(必填)", required = true, example = "true", position = 9)
	private Boolean isDetachable; // 是否可拆分

	@ApiModelProperty(value = "描述(可为空)", required = false, example = "这是一个测试商品", position = 10)
	private String description; // 描述

	@Range(min = 0, max = 999999, message = "{commodity.sellPrice.ltZero}")
	@ApiModelProperty(value = "销售价格(可为空)", required = false, example = "1.0", position = 11)
	private BigDecimal sellingPrice; // 销售价格

	@ApiModelProperty(value = "进货价格(可为空)", required = false, example = "1.5", position = 12)
	@Range(min = 0, max = 999999, message = "{commodity.sellPrice.ltZero}")
	private BigDecimal buyingPrice; // 进货价格

	@ApiModelProperty(value = "资源列表(可为空)", required = false, position = 13)
	private ResourceAdd resource;// 资源列表

	@ApiModelProperty(value = "拆分规则(可为空)", required = false, position = 14)
	private SplitCommodityRuleAdd splitRule; // 拆分规则

	@ApiModelProperty(value = "商品预警数", required = false, position = 15)
	private Double warningNumber; // 商品预警数

	@ApiModelProperty(value = "商家商品库存数", required = false, position = 16)
	private Double inventoryQuantity; // 商家商品库存数
	
	@ApiModelProperty(value = "供应商Id", required = false, position = 17)
	private Long supplierId;

	public void setSellingPrice(BigDecimal sellingPrice) {
		if (sellingPrice != null) {
			this.sellingPrice = sellingPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
		}
	}

	public void setBuyingPrice(BigDecimal buyingPrice) {
		if (buyingPrice != null) {
			this.buyingPrice = buyingPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
		}
	}

}
