package com.wetool.model.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Status {

	GENERATEORDER(0,"生成订单"),
	ORDERCOMMIT(1,"提交订单"),;
	
	private final Integer code;
	private final String name;
	
	@JsonValue
	private Integer getCode() {
		return this.code;
	}
	
	public String getName() {
		return name;
	}

	Status(Integer code,String name) {
		this.code = code;
		this.name = name;
	}
}
