package com.wetool.model.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum OutOfStorage {
	NULL(0,"无"),
	PLACING(1,"出库订单"),
	STORAGE(2,"入库订单");
	
	private final Integer code;
	private final String name;
	
	@JsonValue
	private Integer getCode() {
		return this.code;
	}
	
	public String getName() {
		return name;
	}

	OutOfStorage(Integer code,String name) {
		this.code = code;
		this.name = name;
	}
}
