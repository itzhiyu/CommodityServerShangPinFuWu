package com.wetool.stream.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

/**
 * 出入库多商品modlule
 */
@Data
public class PlacingCommodity implements Serializable {
	private static final long serialVersionUID = -190497089031780225L;

	/** 售价 */
    private BigDecimal price;

    /** 出库数量 */
    private Double commodityNumber;

    /** 多件销售商品 */
    private Long commodityId;


}
