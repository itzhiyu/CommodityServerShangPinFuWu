package com.wetool.stream.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class Placing implements Serializable {

    private static final long serialVersionUID = -6005157418594955555L;

    /** 订单号 */
   private String placingOrderNumber;

    /** 出库类型     5:  商品退换货入库   7:  线上销售出库 8:  线下销售出库*/
   private byte type;

    /** 店铺Id */
    private Long shopId;

    /** 订单日期 */
   private Date createDate;

    /** 多商品 */
   private List<PlacingCommodity> commoditys;

    /**
     * 出库状态
 	 *	1:提交订单
     */
    private byte status;

    /** 实收总金额 */
    private BigDecimal totalPrice;

}
