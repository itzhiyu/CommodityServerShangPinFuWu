package com.wetool.stream;

import com.wetool.Constant;
import com.wetool.entity.Commodity;
import com.wetool.model.enums.OutOfStorage;
import com.wetool.model.enums.Status;
import com.wetool.model.invoicing.InventoryRecordAdd;
import com.wetool.model.invoicing.InventoryRecordCommodityAdd;
import com.wetool.service.commodity.CommodityNewService;
import com.wetool.service.invoicing.SalePlacingService;
import com.wetool.service.invoicing.StorageService;
import com.wetool.stream.model.Placing;
import com.wetool.stream.model.PlacingCommodity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * RabbitMq 接收订单信息
 */
@EnableBinding(Channels.class)
public class RabbitReceiver {
	private static final Logger logger = LogManager.getLogger(RabbitReceiver.class);

//	@Autowired
//	private PlacingService placingService;
	
	@Autowired
	private StorageService storageService;
	
	@Autowired
	private SalePlacingService salePlacingService;
	
	@Autowired
	private CommodityNewService commodityNewService;

	/**
	 * 接收订单 rabbitmq 消息并消费添加出入库信息
	 * @param message接收集合消息
	 * @throws Exception
	 */
	@StreamListener(Channels.INPUT_CHANNEL)
	public void receiver(Placing placing) throws Exception {
		logger.info("接收订单 rabbitmq 消息并消费添加出入库信息.参数消费信息【{}】", placing);
		List<InventoryRecordCommodityAdd> pcadd = getPlacingCommoditys(placing.getCommoditys(), placing.getShopId());
		InventoryRecordAdd inventoryRecordAdd = new InventoryRecordAdd(placing.getPlacingOrderNumber(),null,(long)placing.getType(),
				pcadd,null,placing.getShopId(),null,Status.ORDERCOMMIT,null,placing.getTotalPrice(),null);
		logger.info("接收消息 . 封装出入库实体对象【{}】", inventoryRecordAdd);
		Long type = inventoryRecordAdd.getType();
		switch (type.intValue()) {
		case Constant.SALES_PLACING:
			inventoryRecordAdd.setOutOfStorage(OutOfStorage.PLACING);
			logger.info("接收订单 消费消息添加销售出库信息 . 出库参数【{}】", inventoryRecordAdd);
			salePlacingService.saveSalePlacing(inventoryRecordAdd);
			break;
		case Constant.ONLINE_SALES_PLACING:
			inventoryRecordAdd.setOutOfStorage(OutOfStorage.PLACING);
			logger.info("接收订单 消费消息添加线上销售出库信息 . 出库参数【{}】", inventoryRecordAdd);
			salePlacingService.saveSalePlacing(inventoryRecordAdd);
			break;
		case Constant.REFUND_STORAGE:
			inventoryRecordAdd.setOutOfStorage(OutOfStorage.STORAGE);
			logger.info("接收订单 消费消息 商品退货入库库信息 . 入库参数【{}】", inventoryRecordAdd);
			storageService.save(inventoryRecordAdd);
			break;
		default: break;
		}
		logger.info("接收订单完成 消费结束");	
	}

	/**
	 * 对销售出库处理
	 * @param placings
	 * @param shopId
	 * @return
	 */
	public List<InventoryRecordCommodityAdd> getPlacingCommoditys(List<PlacingCommodity> placings, Long shopId) {
		List<InventoryRecordCommodityAdd> ircs = new ArrayList<>();
		for (PlacingCommodity pc : placings) {
			Commodity commodity = commodityNewService.getById(pc.getCommodityId());
			logger.debug("接收订单 消费消息 出入库库存信息 . 参数出入库商品信息【{}】", commodity);
			if (commodity != null && commodity.getIsDeleted() != true) {
				/* 计算单件商品的总价 */
				Double num = pc.getCommodityNumber();
				BigDecimal price = pc.getPrice() == null? new BigDecimal(0D):pc.getPrice();
				BigDecimal totalPrice = price.multiply(new BigDecimal(num));
				logger.debug("接收订单 消费消息 出入库库存信息 . 参数商品出入库数量【{}】,商品出入库价格【{}】,总金额【{}】", num, price, totalPrice);
				/* 补充商品信息 */
				InventoryRecordCommodityAdd irc = new InventoryRecordCommodityAdd(price, totalPrice, pc.getCommodityNumber(), 
						commodity.getBarcode(), commodity.getName(), commodity.getSpecification(), commodity.getUnit(), commodity.getId(),null);
				/* 商品信息补全 */
				ircs.add(irc);
			}
		}
		logger.info("接收消息 . 出入库详情补全 . 返回出入库详情实体【{}】", ircs);
		return ircs;
	}
}
