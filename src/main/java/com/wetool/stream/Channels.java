package com.wetool.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * Created by zhanbin on 7/1/17.
 */

public interface Channels {
    String OUTPUT_CHANNEL_NAME="push_output_channel";
    
    @Output(Channels.OUTPUT_CHANNEL_NAME)
    MessageChannel outputChannel();
    
    String INPUT_CHANNEL = "invoicing_input_channel"; // position3
    //String OUTPUT_CHANNEL = "invoicing_output_channel";

    //@Output(Channels.OUTPUT_CHANNEL)
    //SubscribableChannel invoicingOutput();

    @Input(Channels.INPUT_CHANNEL)
    MessageChannel invoicingInPut();
}
